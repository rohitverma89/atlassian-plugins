package com.atlassian.plugin.validation;


import com.atlassian.fugue.Either;
import com.atlassian.plugin.Application;
import com.atlassian.plugin.InstallationMode;
import com.atlassian.plugin.parsers.ModuleReader;
import com.atlassian.plugin.parsers.PluginDescriptorReader;
import com.google.common.annotations.VisibleForTesting;
import com.google.common.base.Function;
import com.google.common.base.Predicate;
import com.google.common.collect.Iterables;
import com.google.common.collect.Sets;
import com.google.common.io.InputSupplier;

import java.io.InputStream;
import java.io.Reader;
import java.util.Map;
import java.util.Set;

import static com.atlassian.plugin.validation.Dom4jUtils.readDocument;
import static com.google.common.base.Preconditions.checkNotNull;
import static com.google.common.collect.ImmutableSet.copyOf;
import static com.google.common.collect.Iterables.concat;
import static com.google.common.collect.Iterables.filter;
import static com.google.common.collect.Iterables.transform;

/**
 * A simple validator that given a descriptor and a schema will check that the permissions set in the plugin are valid
 * and all required have been asked.
 *
 * @since 3.0.0
 */
public final class DescriptorValidator {
    private static final String REMOTE_PLUGIN_CONTAINER_MODULE_NAME = "remote-plugin-container";

    private final PluginDescriptorReader descriptorReader;
    private final SchemaReader schemaReader;

    /**
     * @deprecated Please use {@link com.atlassian.plugin.validation.DescriptorValidator#DescriptorValidator(java.io.InputStream, java.io.InputStream, java.util.Set)}. Since v4.0
     */
    @Deprecated
    public DescriptorValidator(InputSupplier<? extends Reader> descriptor, InputSupplier<? extends Reader> schema, Set<Application> applications) {
        descriptorReader = new PluginDescriptorReader(readDocument(descriptor), copyOf(checkNotNull(applications)));
        schemaReader = new SchemaReader(readDocument(schema));
    }

    public DescriptorValidator(InputStream descriptor, InputStream schema, Set<Application> applications) {
        descriptorReader = new PluginDescriptorReader(readDocument(descriptor), copyOf(checkNotNull(applications)));
        schemaReader = new SchemaReader(readDocument(schema));
    }

    public Either<ValidationError, ValidationSuccess> validate(InstallationMode installationMode) {
        final Set<String> allowedPermissions = schemaReader.getAllowedPermissions();
        final Set<String> askedPermissions = descriptorReader.getPluginInformationReader().getPermissions(installationMode);

        final Sets.SetView<String> invalidPermissions = Sets.difference(askedPermissions, allowedPermissions);

        final Set<String> requiredPermissions = getRequiredPermissions(installationMode);
        final Sets.SetView<String> notAskedPermissions = Sets.difference(requiredPermissions, askedPermissions);

        if (!invalidPermissions.isEmpty() || (!descriptorReader.getPluginInformationReader().hasAllPermissions() && !notAskedPermissions.isEmpty())) {
            return Either.left(new ValidationError(invalidPermissions, notAskedPermissions));
        } else {
            return Either.right(new ValidationSuccess(isRemotable(installationMode)));
        }
    }

    private boolean isRemotable(InstallationMode installationMode) {
        final ModuleReader remotePluginContainerModuleReader = Iterables.find(descriptorReader.getModuleReaders(installationMode), new Predicate<ModuleReader>() {
            @Override
            public boolean apply(ModuleReader moduleReader) {
                return moduleReader.getType().equals(REMOTE_PLUGIN_CONTAINER_MODULE_NAME);
            }
        }, null);

        return remotePluginContainerModuleReader != null;
    }

    @VisibleForTesting
    Set<String> getRequiredPermissions(InstallationMode installationMode) {
        final Set<String> moduleKeys = getModuleKeys(installationMode);
        final Map<String, Set<String>> modulesRequiredPermissions = schemaReader.getModulesRequiredPermissions();

        return copyOf(concat(transform(filter(
                        modulesRequiredPermissions.entrySet(),
                        new Predicate<Map.Entry<String, Set<String>>>() {
                            @Override
                            public boolean apply(Map.Entry<String, Set<String>> entry) {
                                return moduleKeys.contains(entry.getKey());
                            }
                        }),
                GetEntryValue.<String, Set<String>>newGetEntryValue())));
    }

    private Set<String> getModuleKeys(InstallationMode installationMode) {
        return copyOf(transform(descriptorReader.getModuleReaders(installationMode), new Function<ModuleReader, String>() {
            @Override
            public String apply(ModuleReader moduleReader) {
                return moduleReader.getType();
            }
        }));
    }

    public static final class ValidationError {
        private final Set<String> nonValidPermissions;
        private final Set<String> notAskedPermissions;

        private ValidationError(Sets.SetView<String> nonValidPermissions, Sets.SetView<String> notAskedPermissions) {
            this.nonValidPermissions = nonValidPermissions.immutableCopy();
            this.notAskedPermissions = notAskedPermissions.immutableCopy();
        }

        public Set<String> getNonValidPermissions() {
            return nonValidPermissions;
        }

        public Set<String> getNotAskedPermissions() {
            return notAskedPermissions;
        }
    }

    public static final class ValidationSuccess {
        private final boolean remotable;

        private ValidationSuccess(boolean remotable) {
            this.remotable = remotable;
        }

        public boolean isRemotable() {
            return remotable;
        }
    }

    private static final class GetEntryValue<K, V> implements Function<Map.Entry<K, V>, V> {
        private GetEntryValue() {
        }

        static <K, V> GetEntryValue<K, V> newGetEntryValue() {
            return new GetEntryValue<K, V>();
        }

        @Override
        public V apply(Map.Entry<K, V> entry) {
            return entry.getValue();
        }
    }
}
