package com.atlassian.plugins.servlet.scope.implicit;

import com.atlassian.plugins.servlet.scope.DummyServlet;

public class ImplicitScopeServlet extends DummyServlet {
    @Override
    protected String name() {
        return "implicit";
    }
}
