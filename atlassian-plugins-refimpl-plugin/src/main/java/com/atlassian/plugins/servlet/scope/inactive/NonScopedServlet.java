package com.atlassian.plugins.servlet.scope.inactive;

import com.atlassian.plugins.servlet.scope.DummyServlet;

public class NonScopedServlet extends DummyServlet{
    @Override
    public String name() {
        return "non-scoped";
    }
}
