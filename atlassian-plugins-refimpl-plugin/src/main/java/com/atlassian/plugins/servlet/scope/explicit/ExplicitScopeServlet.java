package com.atlassian.plugins.servlet.scope.explicit;

import com.atlassian.plugins.servlet.scope.DummyServlet;

public class ExplicitScopeServlet extends DummyServlet {
    @Override
    protected String name() {
        return "explicit";
    }
}
