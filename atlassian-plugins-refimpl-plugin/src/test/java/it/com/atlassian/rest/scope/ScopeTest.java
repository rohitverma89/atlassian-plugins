package it.com.atlassian.rest.scope;

import com.atlassian.rest.jersey.client.WebResourceFactory;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
import org.junit.Test;

import javax.ws.rs.core.Cookie;
import java.util.Optional;

import static java.util.Optional.empty;
import static java.util.Optional.of;
import static javax.ws.rs.core.Response.Status.NOT_FOUND;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;

public class ScopeTest {

    final Cookie dummyScope = new Cookie("atlassian.scope.dummy", "true");
    final Cookie refappScope = new Cookie("atlassian.scope.refapp", "true");

    @Test
    public void testExplicitScopeNoScope() {
        final ClientResponse explicit = get("implicit", empty());

        assertEquals(NOT_FOUND.getStatusCode(), explicit.getStatus());
        assertFalse(explicit.getHeaders().containsKey("dummy-filtered"));
    }

    @Test
    public void testExplicitScopeDummyScope() {
        final ClientResponse explicit = get("explicit", of(dummyScope));
        assertEquals(NOT_FOUND.getStatusCode(), explicit.getStatus());
        assertFalse(explicit.getHeaders().containsKey("dummy-filtered"));
    }

    @Test
    public void testExplicitScope() {
        final ClientResponse explicit = get("implicit", of(refappScope));
        assertEquals("implicit", explicit.getEntity(String.class));
        assertEquals("true", explicit.getHeaders().getFirst("dummy-filtered"));
    }

    @Test
    public void testImplicitScopeNoScope() {
        final ClientResponse implicit = get("implicit", empty());

        assertFalse(implicit.getHeaders().containsKey("dummy-filtered"));
        assertEquals(NOT_FOUND.getStatusCode(), implicit.getStatus());
    }

    @Test
    public void testImplicitScopeDummyScope() {
        final ClientResponse implicit = get("implicit", of(dummyScope));

        assertFalse(implicit.getHeaders().containsKey("dummy-filtered"));
        assertEquals(NOT_FOUND.getStatusCode(), implicit.getStatus());
    }

    @Test
    public void testImplicitScope() {
        final ClientResponse implicit = get("implicit", of(refappScope));

        assertEquals("implicit", implicit.getEntity(String.class));
        assertEquals("true", implicit.getHeaders().getFirst("dummy-filtered"));
    }

    @Test
    public void testNoScoped() {
        assertEquals("non-scoped", get("non-scoped", empty()).getEntity(String.class));
        assertFalse(get("non-scoped", empty()).getHeaders().containsKey("dummy-filtered"));

        assertEquals("non-scoped", get("non-scoped", of(dummyScope)).getEntity(String.class));
        assertFalse(get("non-scoped", of(dummyScope)).getHeaders().containsKey("dummy-filtered"));


        assertEquals("non-scoped", get("non-scoped", of(refappScope)).getEntity(String.class));
        assertEquals("true", get("non-scoped", of(refappScope)).getHeaders().getFirst("dummy-filtered"));
    }

    private ClientResponse get(String path, Optional<Cookie> scope) {
        final WebResource ws = WebResourceFactory.anonymous(
                WebResourceFactory.getUriBuilder()
                        .path("plugins/servlet")
                        .path(path)
                        .build());

        return scope.isPresent()
                ? ws.cookie(scope.get()).get(ClientResponse.class)
                : ws.get(ClientResponse.class);
    }
}
