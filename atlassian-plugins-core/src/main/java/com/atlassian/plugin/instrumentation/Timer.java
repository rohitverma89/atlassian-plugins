package com.atlassian.plugin.instrumentation;

import com.atlassian.instrumentation.operations.OpTimer;

import javax.annotation.Nonnull;
import java.io.Closeable;
import java.util.Optional;

import static com.google.common.base.Preconditions.checkNotNull;

/**
 * Wrapper around an {@link OpTimer} that may be safely used even if that class is not present in the class loader.
 * <p>
 * This wrapper implements {@link Closeable}
 * <p>
 * Note to maintainers: extreme care must be taken to ensure that <code>OpTimer</code> not accessed at runtime if it is
 * not present.
 *
 * @see PluginSystemInstrumentation
 * @since 4.1
 */
public class Timer implements Closeable {
    private final Optional<OpTimer> opTimer;

    Timer(@Nonnull Optional<OpTimer> opTimer) {
        this.opTimer = checkNotNull(opTimer);
    }

    /**
     * Retrieve the timer, if instrumentation is present and enabled.
     */
    public Optional<OpTimer> getOpTimer() {
        return opTimer;
    }

    /**
     * End the timer, if instrumentation is present and enabled.
     */
    @Override
    public void close() {
        if (opTimer.isPresent()) {
            opTimer.get().end();
        }
    }
}
