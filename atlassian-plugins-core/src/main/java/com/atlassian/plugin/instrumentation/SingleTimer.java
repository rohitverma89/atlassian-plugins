package com.atlassian.plugin.instrumentation;

import com.atlassian.instrumentation.operations.OpSnapshot;
import com.atlassian.instrumentation.operations.OpTimer;
import org.apache.log4j.MDC;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Nonnull;
import java.util.Optional;
import java.util.concurrent.TimeUnit;

/**
 * Wrapper around an {@link com.atlassian.instrumentation.operations.OpTimer} that may be safely used even if that class is not present in the class loader.
 * <p>
 * This wrapper implements {@link java.io.Closeable}. On close it logs elapsed time and elapsed cpu time
 * <p>
 * Note to maintainers: extreme care must be taken to ensure that <code>OpTimer</code> not accessed at runtime if it is
 * not present.
 *
 * @see PluginSystemInstrumentation
 * @since 4.1
 */
public class SingleTimer extends Timer {
    private static final Logger log = LoggerFactory.getLogger(SingleTimer.class);
    // We cannot rely on OpTimer name for logging as it contains timestamps
    private String name;

    SingleTimer(@Nonnull Optional<OpTimer> opTimer, String name) {
        super(opTimer);
        this.name = name;
    }

    public String getName() {
        return name;
    }
    
    /**
     * End the timer, if instrumentation is present and enabled log clock time and cpu time
     */
    @Override
    public void close() {
        Optional<OpTimer> timerOption = this.getOpTimer();
        if (timerOption.isPresent()) {
            OpSnapshot snapshot = timerOption.get().snapshot();
            long cpuTime = snapshot.getCpuTotalTime(TimeUnit.MILLISECONDS);
            long elapsedTime = snapshot.getElapsedTotalTime(TimeUnit.MILLISECONDS);
            String logKey = "com.atlassian.plugin." + name;
            String cpuKey = logKey + ".cpu-ms";
            String clockKey = logKey + ".clock-ms";

            MDC.put(cpuKey, cpuTime);
            MDC.put(clockKey, elapsedTime);
            // Kibana does not register DEBUG messages. This is safe as instrumentation won't be enabled in PROD
            log.info("Timer {} took {}ms ({} cpu ns)", snapshot.getName(), elapsedTime, cpuTime);
            MDC.remove(clockKey);
            MDC.remove(cpuKey);
        }
        super.close();
    }
}
