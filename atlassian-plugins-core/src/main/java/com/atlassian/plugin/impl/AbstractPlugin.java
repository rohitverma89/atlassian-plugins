package com.atlassian.plugin.impl;

import com.atlassian.annotations.ExperimentalApi;
import com.atlassian.annotations.Internal;
import com.atlassian.fugue.Option;
import com.atlassian.plugin.InstallationMode;
import com.atlassian.plugin.ModuleDescriptor;
import com.atlassian.plugin.Permissions;
import com.atlassian.plugin.Plugin;
import com.atlassian.plugin.PluginArtifact;
import com.atlassian.plugin.PluginDependencies;
import com.atlassian.plugin.PluginException;
import com.atlassian.plugin.PluginInformation;
import com.atlassian.plugin.PluginInternal;
import com.atlassian.plugin.PluginPermission;
import com.atlassian.plugin.PluginState;
import com.atlassian.plugin.Resourced;
import com.atlassian.plugin.Resources;
import com.atlassian.plugin.elements.ResourceDescriptor;
import com.atlassian.plugin.elements.ResourceLocation;
import com.atlassian.plugin.util.VersionStringComparator;
import com.atlassian.util.concurrent.CopyOnWriteMap;
import com.google.common.base.Function;
import com.google.common.base.Predicate;
import com.google.common.base.Supplier;
import com.google.common.base.Suppliers;
import com.google.common.collect.ImmutableSet;
import com.google.common.collect.Iterables;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Nonnull;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.concurrent.CopyOnWriteArraySet;
import java.util.concurrent.atomic.AtomicReference;

import static com.google.common.base.Suppliers.memoize;

/**
 * Represents the base class for all plugins. Note: This class has a natural ordering that is inconsistent with equals.
 * <p/>
 * p1.equals(p2) == true will give p1.compareTo(p2) == 0 but the opposite is not guaranteed because we keep a map of
 * plugins versus loaders in the DefaultPluginManager .
 * <p/>
 * A plugin with the same key and version may well be loaded from multiple loaders (in fact with UPM it's almost
 * guaranteed) so we CANNOT override equals.
 */

public abstract class AbstractPlugin implements PluginInternal, Plugin.EnabledMetricsSource, Comparable<Plugin> {
    private static final Logger log = LoggerFactory.getLogger(AbstractPlugin.class);

    private final Map<String, ModuleDescriptor<?>> modules = CopyOnWriteMap.<String, ModuleDescriptor<?>>builder().stableViews().newLinkedMap();
    private final Set<ModuleDescriptor<?>> dynamicModules = new CopyOnWriteArraySet<>();
    private String name;
    private String i18nNameKey;
    private String key;
    private boolean enabledByDefault = true;
    private PluginInformation pluginInformation = new PluginInformation();
    private boolean system;
    private Resourced resources = Resources.EMPTY_RESOURCES;
    private int pluginsVersion = 1;
    private final Date dateLoaded = new Date();
    /**
     * The date that this plugin most recently entered {@link PluginState#ENABLING}.
     */
    private volatile Date dateEnabling;
    /**
     * The date that this plugin most recently entered {@link PluginState#ENABLED}.
     */
    private volatile Date dateEnabled;
    private final AtomicReference<PluginState> pluginState = new AtomicReference<>(PluginState.UNINSTALLED);

    private final Supplier<Set<String>> permissions;

    private volatile boolean bundledPlugin = false;

    protected final PluginArtifact pluginArtifact;

    /**
     * Compatibility constructor to assist users during the migration to split API/core.
     *
     * @deprecated since 4.0, will be removed in 5.0
     */
    @Deprecated
    @Internal
    public AbstractPlugin() {
        this(null);
    }

    public AbstractPlugin(final PluginArtifact pluginArtifact) {
        this.pluginArtifact = pluginArtifact;
        permissions = memoize(new Supplier<Set<String>>() {
            @Override
            public Set<String> get() {
                return getPermissionsInternal();
            }
        });
    }

    public String getName() {
        return !StringUtils.isBlank(name) ? name : !StringUtils.isBlank(i18nNameKey) ? "" : getKey();
    }

    public void setName(final String name) {
        this.name = name;
    }

    /**
     * @return the logger used internally
     */
    protected Logger getLog() {
        return log;
    }

    public String getI18nNameKey() {
        return i18nNameKey;
    }

    public void setI18nNameKey(final String i18nNameKey) {
        this.i18nNameKey = i18nNameKey;
    }

    public String getKey() {
        return key;
    }

    public void setKey(final String key) {
        this.key = key;
    }

    public void addModuleDescriptor(final ModuleDescriptor<?> moduleDescriptor) {
        modules.put(moduleDescriptor.getKey(), moduleDescriptor);
    }

    protected void removeModuleDescriptor(final String key) {
        modules.remove(key);
    }

    /**
     * Returns the module descriptors for this plugin
     *
     * @return An unmodifiable list of the module descriptors.
     */
    public Collection<ModuleDescriptor<?>> getModuleDescriptors() {
        return modules.values();
    }

    public ModuleDescriptor<?> getModuleDescriptor(final String key) {
        return modules.get(key);
    }

    public <T> List<ModuleDescriptor<T>> getModuleDescriptorsByModuleClass(final Class<T> aClass) {
        final List<ModuleDescriptor<T>> result = new ArrayList<>();
        for (final ModuleDescriptor<?> moduleDescriptor : modules.values()) {
            final Class<?> moduleClass = moduleDescriptor.getModuleClass();
            if (moduleClass != null && aClass.isAssignableFrom(moduleClass)) {
                @SuppressWarnings("unchecked")
                final ModuleDescriptor<T> typedModuleDescriptor = (ModuleDescriptor<T>) moduleDescriptor;
                result.add(typedModuleDescriptor);
            }
        }
        return result;
    }

    public PluginState getPluginState() {
        return pluginState.get();
    }

    protected void setPluginState(final PluginState state) {
        if (log.isDebugEnabled()) {
            log.debug("Plugin " + getKey() + " going from " + getPluginState() + " to " + state);
        }

        pluginState.set(state);
        updateEnableTimes(state);
    }

    /**
     * Only sets the plugin state if it is in the expected state.
     *
     * @param requiredExistingState The expected state
     * @param desiredState          The desired state
     * @return True if the set was successful, false if not in the expected state
     * @since 2.4
     */
    protected boolean compareAndSetPluginState(final PluginState requiredExistingState, final PluginState desiredState) {
        if (log.isDebugEnabled()) {
            log.debug("Plugin {} trying to go from {} to {} but only if in {}",
                    new Object[]{getKey(), getPluginState(), desiredState, requiredExistingState});
        }
        final boolean changed = pluginState.compareAndSet(requiredExistingState, desiredState);
        if (changed) {
            updateEnableTimes(desiredState);
        }
        return changed;
    }

    private void updateEnableTimes(final PluginState state) {
        final Date now = new Date();
        if (PluginState.ENABLING == state) {
            dateEnabling = now;
            dateEnabled = null;
        } else if (PluginState.ENABLED == state) {
            // Automatically transition through ENABLING if we haven't already been there
            if (dateEnabling == null) {
                dateEnabling = now;
            }
            dateEnabled = now;
        }
        // else it's not a state change we are tracking
    }

    public boolean isEnabledByDefault() {
        return enabledByDefault && ((pluginInformation == null) || pluginInformation.satisfiesMinJavaVersion());
    }

    public void setEnabledByDefault(final boolean enabledByDefault) {
        this.enabledByDefault = enabledByDefault;
    }

    public int getPluginsVersion() {
        return pluginsVersion;
    }

    public void setPluginsVersion(final int pluginsVersion) {
        this.pluginsVersion = pluginsVersion;
    }

    public PluginInformation getPluginInformation() {
        return pluginInformation;
    }

    public void setPluginInformation(final PluginInformation pluginInformation) {
        this.pluginInformation = pluginInformation;
    }

    public void setResources(final Resourced resources) {
        this.resources = resources != null ? resources : Resources.EMPTY_RESOURCES;
    }

    public List<ResourceDescriptor> getResourceDescriptors() {
        return resources.getResourceDescriptors();
    }

    public List<ResourceDescriptor> getResourceDescriptors(final String type) {
        return resources.getResourceDescriptors(type);
    }

    public ResourceLocation getResourceLocation(final String type, final String name) {
        return resources.getResourceLocation(type, name);
    }

    /**
     * @deprecated
     */
    @Deprecated
    public ResourceDescriptor getResourceDescriptor(final String type, final String name) {
        return resources.getResourceDescriptor(type, name);
    }

    /**
     * @return true if the plugin has been enabled
     */
    @Deprecated
    public boolean isEnabled() {
        return getPluginState() == PluginState.ENABLED;
    }

    public void enable() {
        log.debug("Enabling plugin '{}'", getKey());

        final PluginState state = pluginState.get();
        if ((state == PluginState.ENABLED) || (state == PluginState.ENABLING)) {
            log.debug("Plugin '{}' is already enabled, not doing anything.", getKey());
            return;
        }

        try {
            log.debug("Plugin '{}' is NOT already enabled, actually enabling.", getKey());
            final PluginState desiredState = enableInternal();
            // This code is a bit baroque because it preserves historic behaviour, namely performing the state change even
            // if warning the transition is illegal. Race conditions are resolved by requiring subclasses to signal whether
            // or not they have taken over state change (by returning PENDING from enableInternal).
            if (desiredState != PluginState.PENDING) {
                if ((desiredState != PluginState.ENABLED) && (desiredState != PluginState.ENABLING)) {
                    log.warn("Illegal state transition to {} for plugin '{}' on enable()", desiredState, getKey());
                }
                setPluginState(desiredState);
            }
            // else enableInternal has taken over state management and we need not do anything
        } catch (final PluginException ex) {
            log.warn("Unable to enable plugin '{}'", getKey());
            log.warn("Because of this exception", ex);
            throw ex;
        }

        log.debug("Enabled plugin '{}'", getKey());
    }

    /**
     * Perform any internal enabling logic.
     *
     * This method is called by enable to allow subclasses to customize enable behaviour. If a PluginState other than
     * {@link PluginState#PENDING} is returned, it will be passed to {@link #setPluginState(PluginState)}. If a subclass
     * returns {@link PluginState#PENDING}, no state is set, and it is assumed the subclass has taken responsibility for
     * transitioning state via direct calls to {@link #setPluginState(PluginState)}.
     *
     * Subclasses should only throw {@link PluginException}.
     *
     * @return One of {@link PluginState#ENABLED}, {@link PluginState#ENABLING}, or {@link PluginState#PENDING}
     * @throws PluginException If the plugin could not be enabled
     * @since 2.2.0
     */
    protected PluginState enableInternal() throws PluginException {
        return PluginState.ENABLED;
    }

    public final void disable() {
        if (pluginState.get() == PluginState.DISABLED) {
            return;
        }

        log.debug("Disabling plugin '{}'", getKey());

        try {
            setPluginState(PluginState.DISABLING);
            disableInternal();
            setPluginState(PluginState.DISABLED);
        } catch (final PluginException ex) {
            setPluginState(PluginState.ENABLED);
            log.warn("Unable to disable plugin '" + getKey() + "'", ex);
            throw ex;
        }

        log.debug("Disabled plugin '{}'", getKey());
    }

    /**
     * Perform any internal disabling logic. Subclasses should only throw {@link PluginException}.
     *
     * @throws PluginException If the plugin could not be disabled
     * @since 2.2.0
     */
    protected void disableInternal() throws PluginException {
    }

    public Set<String> getRequiredPlugins() {
        return getDependencies().getAll();
    }

    @Nonnull
    @Override
    public PluginDependencies getDependencies() {
        return new PluginDependencies();
    }

    @Override
    public final Set<String> getActivePermissions() {
        return permissions.get();
    }

    private Set<String> getPermissionsInternal() {
        return ImmutableSet.copyOf(Iterables.transform(getPermissionsForCurrentInstallationMode(),
                new Function<PluginPermission, String>() {
                    @Override
                    public String apply(final PluginPermission p) {
                        return p.getName();
                    }
                }
        ));
    }

    private Iterable<PluginPermission> getPermissionsForCurrentInstallationMode() {
        return Iterables.filter(getPluginInformation().getPermissions(),
                new Predicate<PluginPermission>() {
                    @Override
                    public boolean apply(final PluginPermission p) {
                        return isInstallationModeUndefinedOrEqualToCurrentInstallationMode(p.getInstallationMode());
                    }
                }
        );
    }

    private boolean isInstallationModeUndefinedOrEqualToCurrentInstallationMode(
            final Option<InstallationMode> installationMode) {
        return installationMode.fold(Suppliers.ofInstance(Boolean.TRUE),
                new Function<InstallationMode, Boolean>() {
                    @Override
                    public Boolean apply(final InstallationMode mode) {
                        return mode.equals(getInstallationMode());
                    }
                });
    }

    @Override
    public final boolean hasAllPermissions() {
        return getActivePermissions().contains(Permissions.ALL_PERMISSIONS);
    }

    public InstallationMode getInstallationMode() {
        return InstallationMode.LOCAL;
    }

    public void close() {
        uninstall();
    }

    public final void install() {
        log.debug("Installing plugin '{}'.", getKey());

        if (pluginState.get() == PluginState.INSTALLED) {
            log.debug("Plugin '{}' is already installed, not doing anything.", getKey());
            return;
        }

        try {
            installInternal();
            setPluginState(PluginState.INSTALLED);
        } catch (final PluginException ex) {
            log.warn("Unable to install plugin '" + getKey() + "'.", ex);
            throw ex;
        }

        log.debug("Installed plugin '{}'.", getKey());
    }

    /**
     * Perform any internal installation logic. Subclasses should only throw {@link PluginException}.
     *
     * @throws PluginException If the plugin could not be installed
     * @since 2.2.0
     */
    protected void installInternal() throws PluginException {
        log.debug("Actually installing plugin '{}'.", getKey());
    }

    public final void uninstall() {
        if (pluginState.get() == PluginState.UNINSTALLED) {
            return;
        }

        log.debug("Uninstalling plugin '{}'", getKey());

        try {
            uninstallInternal();
            setPluginState(PluginState.UNINSTALLED);
        } catch (final PluginException ex) {
            log.warn("Unable to uninstall plugin '" + getKey() + "'", ex);
            throw ex;
        }

        log.debug("Uninstalled plugin '{}'", getKey());
    }

    /**
     * Perform any internal uninstallation logic. Subclasses should only throw {@link PluginException}.
     *
     * @throws PluginException If the plugin could not be uninstalled
     * @since 2.2.0
     */
    protected void uninstallInternal() throws PluginException {
    }

    /**
     * Setter for the enabled state of a plugin. If this is set to false then the plugin will not execute.
     */
    @Deprecated
    public void setEnabled(final boolean enabled) {
        if (enabled) {
            enable();
        } else {
            disable();
        }
    }

    public boolean isSystemPlugin() {
        return system;
    }

    public boolean containsSystemModule() {
        for (final ModuleDescriptor<?> moduleDescriptor : modules.values()) {
            if (moduleDescriptor.isSystemModule()) {
                return true;
            }
        }
        return false;
    }

    public void setSystemPlugin(final boolean system) {
        this.system = system;
    }

    public void resolve() {
        // By default, no need to do anything
    }

    public Date getDateLoaded() {
        return dateLoaded;
    }

    @Override
    public Date getDateInstalled() {
        return new Date(dateLoaded.getTime());
    }

    @Override
    @ExperimentalApi
    public Date getDateEnabling() {
        return dateEnabling;
    }

    @Override
    @ExperimentalApi
    public Date getDateEnabled() {
        return dateEnabled;
    }

    @Override
    public boolean isBundledPlugin() {
        return bundledPlugin;
    }

    @Override
    public void setBundledPlugin(final boolean bundledPlugin) {
        this.bundledPlugin = bundledPlugin;
    }

    @Override
    public PluginArtifact getPluginArtifact() {
        return pluginArtifact;
    }

    @Override
    public Optional<String> getScopeKey() {
        return pluginInformation.getScopeKey();
    }

    @Override
    public Iterable<ModuleDescriptor<?>> getDynamicModuleDescriptors() {
        return ImmutableSet.copyOf(dynamicModules);
    }

    @Override
    public boolean addDynamicModuleDescriptor(final ModuleDescriptor<?> module) {
        addModuleDescriptor(module);
        return dynamicModules.add(module);
    }

    @Override
    public boolean removeDynamicModuleDescriptor(final ModuleDescriptor<?> module) {
        removeModuleDescriptor(module.getKey());
        return dynamicModules.remove(module);
    }

    /**
     * Compares this Plugin to another Plugin for order. The primary sort field is the key, and the secondary field
     * is the version number.
     *
     * @param otherPlugin The plugin to be compared.
     * @return a negative integer, zero, or a positive integer as this Plugin is less than, equal to, or greater than
     * the specified Plugin.
     * @see VersionStringComparator
     * @see Comparable#compareTo
     */
    public int compareTo(@Nonnull final Plugin otherPlugin) {
        if (otherPlugin.getKey() == null) {
            if (getKey() == null) {
                // both null keys - not going to bother checking the version,
                // who cares?
                return 0;
            }
            return 1;
        }
        if (getKey() == null) {
            return -1;
        }

        // If the compared plugin doesn't have the same key, the current object
        // is greater
        if (!otherPlugin.getKey().equals(getKey())) {
            return getKey().compareTo(otherPlugin.getKey());
        }

        final String thisVersion = cleanVersionString((getPluginInformation() != null ? getPluginInformation().getVersion() : null));
        final String otherVersion = cleanVersionString((otherPlugin.getPluginInformation() != null ? otherPlugin.getPluginInformation().getVersion() : null));

        // Valid versions should come after invalid versions because when we
        // find multiple instances of a plugin, we choose the "latest".
        if (!VersionStringComparator.isValidVersionString(thisVersion)) {
            if (!VersionStringComparator.isValidVersionString(otherVersion)) {
                // both invalid
                return 0;
            }
            return -1;
        }
        if (!VersionStringComparator.isValidVersionString(otherVersion)) {
            return 1;
        }

        //if they are both equivalent snapshots use timestamps to order them
        if (VersionStringComparator.isSnapshotVersion(thisVersion) && VersionStringComparator.isSnapshotVersion(otherVersion)) {
            final int comparison = new VersionStringComparator().compare(thisVersion, otherVersion);
            if (comparison == 0) {
                return this.getDateInstalled().compareTo(otherPlugin.getDateInstalled());
            } else {
                return comparison;
            }
        }

        return new VersionStringComparator().compare(thisVersion, otherVersion);
    }

    @Internal
    public static String cleanVersionString(final String version) {
        if ((version == null) || version.trim().equals("")) {
            return "0";
        }
        return version.replaceAll(" ", "");
    }

    @Override
    public String toString() {
        final PluginInformation info = getPluginInformation();
        return getKey() + ":" + (info == null ? "?" : info.getVersion());
    }
}
