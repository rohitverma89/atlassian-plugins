package com.atlassian.plugin.impl;

import com.atlassian.annotations.ExperimentalApi;
import com.atlassian.plugin.InstallationMode;
import com.atlassian.plugin.ModuleDescriptor;
import com.atlassian.plugin.Plugin;
import com.atlassian.plugin.PluginArtifact;
import com.atlassian.plugin.PluginDependencies;
import com.atlassian.plugin.PluginInformation;
import com.atlassian.plugin.PluginState;
import com.atlassian.plugin.Resourced;
import com.atlassian.plugin.elements.ResourceDescriptor;
import com.atlassian.plugin.elements.ResourceLocation;

import javax.annotation.Nonnull;
import java.io.InputStream;
import java.net.URL;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.Set;

import static com.google.common.base.Preconditions.checkNotNull;

/**
 * Delegating plugin that supports easy wrapping
 *
 * Note: this class has a natural ordering that is inconsistent with equals
 *
 * @since 2.2.0
 * @deprecated since 4.0.0, to be removed in 5.0.0: No longer used
 */
@Deprecated
public abstract class AbstractDelegatingPlugin
        implements Plugin, Plugin.Resolvable, Plugin.EnabledMetricsSource, Comparable<Plugin> {
    private final Plugin delegate;

    public AbstractDelegatingPlugin(final Plugin delegate) {
        this.delegate = checkNotNull(delegate);
    }

    public int getPluginsVersion() {
        return delegate.getPluginsVersion();
    }

    public void setPluginsVersion(final int version) {
        delegate.setPluginsVersion(version);
    }

    public String getName() {
        return delegate.getName();
    }

    public void setName(final String name) {
        delegate.setName(name);
    }

    public String getI18nNameKey() {
        return delegate.getI18nNameKey();
    }

    public void setI18nNameKey(final String i18nNameKey) {
        delegate.setI18nNameKey(i18nNameKey);
    }

    public String getKey() {
        return delegate.getKey();
    }

    public void setKey(final String aPackage) {
        delegate.setKey(aPackage);
    }

    public void addModuleDescriptor(final ModuleDescriptor<?> moduleDescriptor) {
        delegate.addModuleDescriptor(moduleDescriptor);
    }

    public Collection<ModuleDescriptor<?>> getModuleDescriptors() {
        return delegate.getModuleDescriptors();
    }

    public ModuleDescriptor<?> getModuleDescriptor(final String key) {
        return delegate.getModuleDescriptor(key);
    }

    public <M> List<ModuleDescriptor<M>> getModuleDescriptorsByModuleClass(final Class<M> moduleClass) {
        return delegate.getModuleDescriptorsByModuleClass(moduleClass);
    }

    @Override
    public InstallationMode getInstallationMode() {
        return delegate.getInstallationMode();
    }

    public boolean isEnabledByDefault() {
        return delegate.isEnabledByDefault();
    }

    public void setEnabledByDefault(final boolean enabledByDefault) {
        delegate.setEnabledByDefault(enabledByDefault);
    }

    public PluginInformation getPluginInformation() {
        return delegate.getPluginInformation();
    }

    public void setPluginInformation(final PluginInformation pluginInformation) {
        delegate.setPluginInformation(pluginInformation);
    }

    public void setResources(final Resourced resources) {
        delegate.setResources(resources);
    }

    public PluginState getPluginState() {
        return delegate.getPluginState();
    }

    public boolean isEnabled() {
        return delegate.isEnabled();
    }

    public boolean isSystemPlugin() {
        return delegate.isSystemPlugin();
    }

    public boolean containsSystemModule() {
        return delegate.containsSystemModule();
    }

    public void setSystemPlugin(final boolean system) {
        delegate.setSystemPlugin(system);
    }

    public boolean isBundledPlugin() {
        return delegate.isBundledPlugin();
    }

    public Date getDateLoaded() {
        return delegate.getDateLoaded();
    }

    @Override
    public Date getDateInstalled() {
        return delegate.getDateInstalled();
    }

    @Override
    @ExperimentalApi
    public Date getDateEnabling() {
        return delegate.getDateEnabling();
    }

    @Override
    @ExperimentalApi
    public Date getDateEnabled() {
        return delegate.getDateEnabled();
    }

    @Override
    public PluginArtifact getPluginArtifact() {
        return delegate.getPluginArtifact();
    }

    @Override
    public Optional<String> getScopeKey() {
        return delegate.getScopeKey();
    }

    public boolean isUninstallable() {
        return delegate.isUninstallable();
    }

    public boolean isDeleteable() {
        return delegate.isDeleteable();
    }

    public boolean isDynamicallyLoaded() {
        return delegate.isDynamicallyLoaded();
    }

    public <T> Class<T> loadClass(final String clazz, final Class<?> callingClass) throws ClassNotFoundException {
        return delegate.loadClass(clazz, callingClass);
    }

    public ClassLoader getClassLoader() {
        return delegate.getClassLoader();
    }

    public URL getResource(final String path) {
        return delegate.getResource(path);
    }

    public InputStream getResourceAsStream(final String name) {
        return delegate.getResourceAsStream(name);
    }

    public void setEnabled(final boolean enabled) {
        delegate.setEnabled(enabled);
    }

    public void close() {
        delegate.close();
    }

    public void install() {
        delegate.install();
    }

    public void uninstall() {
        delegate.uninstall();
    }

    public void enable() {
        delegate.enable();
    }

    public void disable() {
        delegate.disable();
    }

    public Set<String> getRequiredPlugins() {
        return delegate.getRequiredPlugins();
    }

    @Nonnull
    @Override
    public PluginDependencies getDependencies() {
        return delegate.getDependencies();
    }

    public List<ResourceDescriptor> getResourceDescriptors() {
        return delegate.getResourceDescriptors();
    }

    public List<ResourceDescriptor> getResourceDescriptors(final String type) {
        return delegate.getResourceDescriptors(type);
    }

    public ResourceDescriptor getResourceDescriptor(final String type, final String name) {
        return delegate.getResourceDescriptor(type, name);
    }

    public ResourceLocation getResourceLocation(final String type, final String name) {
        return delegate.getResourceLocation(type, name);
    }

    public int compareTo(final Plugin o) {
        return delegate.compareTo(o);
    }

    public Plugin getDelegate() {
        return delegate;
    }

    @Override
    public String toString() {
        return delegate.toString();
    }

    public Set<String> getActivePermissions() {
        return delegate.getActivePermissions();
    }

    @Override
    public boolean hasAllPermissions() {
        return delegate.hasAllPermissions();
    }

    @Override
    public void resolve() {
        delegate.resolve();
    }
}
