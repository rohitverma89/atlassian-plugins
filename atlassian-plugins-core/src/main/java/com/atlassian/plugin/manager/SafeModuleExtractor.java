package com.atlassian.plugin.manager;

import com.atlassian.plugin.ModuleDescriptor;
import com.atlassian.plugin.PluginController;
import com.google.common.base.Function;
import com.google.common.base.Predicates;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Nullable;

import static com.google.common.collect.Iterables.filter;
import static com.google.common.collect.Iterables.transform;

/**
 * Safely extracts the module instance from module descriptors,
 * handling exceptions and disabling broken plugins as appropriate.
 *
 * @since 4.0
 */
public class SafeModuleExtractor {
    private static final Logger log = LoggerFactory.getLogger(SafeModuleExtractor.class);

    private final PluginController pluginController;

    public SafeModuleExtractor(PluginController pluginController) {
        this.pluginController = pluginController;
    }

    /**
     * Safely extracts the module instance from the given module descriptors. This method will disable any plugin it
     * can't successfully extract the module instance from.
     */
    public <M> Iterable<M> getModules(final Iterable<? extends ModuleDescriptor<M>> moduleDescriptors) {
        return filter(
                transform(
                        moduleDescriptors,
                        new Function<ModuleDescriptor<M>, M>() {
                            @Override
                            public M apply(@Nullable ModuleDescriptor<M> descriptor) {
                                if (descriptor == null || descriptor.isBroken()) {
                                    return null;
                                }

                                try {
                                    return descriptor.getModule();
                                } catch (final RuntimeException ex) {
                                    final String pluginKey = descriptor.getPlugin().getKey();

                                    log.error("Exception when retrieving plugin module {}, disabling plugin {}",
                                            descriptor.getCompleteKey(), pluginKey, ex);

                                    descriptor.setBroken();
                                    // Don't persist: don't want to create a snowflake config in Cloud instance,
                                    // and the module might work following restart if something has changed.
                                    pluginController.disablePluginWithoutPersisting(pluginKey);
                                    return null;
                                }
                            }
                        }),
                Predicates.notNull());
    }
}
