package com.atlassian.plugin.manager;

import com.atlassian.plugin.ModuleDescriptor;
import com.atlassian.plugin.Plugin;
import com.atlassian.plugin.PluginRestartState;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import static com.atlassian.plugin.manager.PluginEnabledState.UNKNOWN_ENABLED_TIME;
import static com.atlassian.plugin.manager.PluginEnabledState.getPluginEnabledStateWithCurrentTime;
import static com.atlassian.plugin.manager.PluginPersistentState.Util.RESTART_STATE_SEPARATOR;
import static com.atlassian.plugin.manager.PluginPersistentState.Util.buildStateKey;
import static java.util.stream.Collectors.toMap;

/**
 * Interface that represents a configuration state for plugins and plugin modules. The configuration state (enabled
 * or disabled) is separate from the plugins and modules themselves because a plugin may have multiple
 * states depending on the context.
 *
 * @author anatoli
 * @since 2.2.0
 */
public interface PluginPersistentState {
    /**
     * Get the map of all states. Deprecated since 4.5.0
     * @deprecated please use {@link PluginPersistentState#getStatesMap} instead
     * @return The map that maps plugins and modules' keys to an actual enabled state (Boolean.True/Boolean.False).
     */
    @Deprecated
    default Map<String, Boolean> getMap() {
        return getStatesMap().entrySet().stream().collect(toMap(Map.Entry::getKey, e -> e.getValue().isEnabled()));
    }

    /**
     * Get the map of all states with the update timestamp for each of them.
     * @since 4.5.0
     * @return The map that maps plugins and modules' keys to an actual state {@link PluginEnabledState}
     */
    Map<String, PluginEnabledState> getStatesMap();

    /**
     * Whether or not a plugin is enabled, calculated from it's current state AND default state.
     */
    boolean isEnabled(final Plugin plugin);

    /**
     * Whether or not a given plugin module is enabled in this state, calculated from it's current state AND default state.
     */
    boolean isEnabled(final ModuleDescriptor<?> pluginModule);

    /**
     * Get state map of the given plugin and its modules. Deprecated since 4.5.0
     *
     * @param plugin the plugin
     * @deprecated please use {@link PluginPersistentState#getPluginEnabledStateMap(Plugin)} instead
     * @return The map that maps the plugin and its modules' keys to plugin state (Boolean.TRUE/Boolean.FALSE).
     */
    @Deprecated
    default Map<String, Boolean> getPluginStateMap(final Plugin plugin) {
        return getPluginEnabledStateMap(plugin).entrySet().stream()
                .collect(toMap(Map.Entry::getKey, e -> e.getValue().isEnabled()));
    }

    /**
     * Get state map of the given plugin and its modules.
     * @since 4.5.0
     * @param plugin the plugin
     * @return The map that maps the plugin and its modules' keys to plugin state {@link PluginEnabledState}
     */
    Map<String, PluginEnabledState> getPluginEnabledStateMap(final Plugin plugin);

    /**
     * Gets whether the plugin is expected to be upgraded, installed, or removed on next restart
     *
     * @param pluginKey The plugin to query
     * @return The state of the plugin on restart
     */
    PluginRestartState getPluginRestartState(String pluginKey);

    /**
     * Builder for {@link PluginPersistentState} instances.
     * <p>
     * This class is <strong>not thread safe</strong>. It should
     * only be used in a method local context.
     *
     * @since 2.3.0
     */
    final class Builder {

        public static Builder create() {
            return new Builder();
        }

        public static Builder create(final PluginPersistentState state) {
            return new Builder(state);
        }

        private final Map<String, PluginEnabledState> map = new HashMap<>();

        Builder() {
        }

        Builder(final PluginPersistentState state) {
            map.putAll(state.getStatesMap());
        }

        public PluginPersistentState toState() {
            return new DefaultPluginPersistentState(map, true);
        }

        public Builder setEnabled(final ModuleDescriptor<?> pluginModule, final boolean isEnabled) {
            setEnabled(pluginModule.getCompleteKey(), isEnabled);
            return this;
        }

        public Builder setEnabled(final Plugin plugin, final boolean isEnabled) {
            setEnabled(plugin.getKey(), isEnabled);
            return this;
        }

        private Builder setEnabled(final String completeKey, final boolean isEnabled) {
            map.put(completeKey, getPluginEnabledStateWithCurrentTime(isEnabled));
            return this;
        }

        /**
         * reset all plugin's state.
         */
        public Builder setState(final PluginPersistentState state) {
            map.clear();
            map.putAll(state.getStatesMap());
            return this;
        }

        /**
         * Add the plugin state.
         */
        public Builder addPluginEnabledState(final Map<String, PluginEnabledState> state) {
            map.putAll(state);
            return this;
        }

        /**
         * Add the plugin state.
         */
        @Deprecated
        public Builder addState(final Map<String, Boolean> state) {
            map.putAll(state.entrySet().stream().collect(toMap(Map.Entry::getKey, e -> new PluginEnabledState(e.getValue(), UNKNOWN_ENABLED_TIME))));
            return this;
        }

        /**
         * Remove a plugin's state.
         */
        public Builder removeState(final String key) {
            map.remove(key);
            return this;
        }

        public Builder setPluginRestartState(final String pluginKey, final PluginRestartState state) {
            // Remove existing state, if any
            for (final PluginRestartState st : PluginRestartState.values()) {
                map.remove(buildStateKey(pluginKey, st));
            }

            if (state != PluginRestartState.NONE) {
                map.put(buildStateKey(pluginKey, state), getPluginEnabledStateWithCurrentTime(true));
            }
            return this;
        }

        public Builder clearPluginRestartState() {
            final Set<String> keys = new HashSet<>(map.keySet());
            for (final String key : keys) {
                if (key.contains(RESTART_STATE_SEPARATOR)) {
                    map.remove(key);
                }
            }
            return this;
        }
    }

    class Util {
        public static final String RESTART_STATE_SEPARATOR = "--";

        static String buildStateKey(final String pluginKey, final PluginRestartState state) {
            final StringBuilder sb = new StringBuilder();
            sb.append(state.name());
            sb.append(RESTART_STATE_SEPARATOR);
            sb.append(pluginKey);
            return sb.toString();
        }
    }
}