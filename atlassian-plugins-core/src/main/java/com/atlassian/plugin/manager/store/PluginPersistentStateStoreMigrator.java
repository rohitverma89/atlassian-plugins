package com.atlassian.plugin.manager.store;

import com.atlassian.plugin.manager.PluginEnabledState;
import com.atlassian.plugin.manager.PluginPersistentStateModifier;
import com.atlassian.plugin.manager.PluginPersistentStateStore;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.HashMap;
import java.util.Map;

import static com.atlassian.plugin.manager.PluginPersistentState.Util.RESTART_STATE_SEPARATOR;

/**
 * Contains tasks that are specific to migrating data in the product's PluginPersistentStateStore.
 *
 * @since v4.2
 */
public class PluginPersistentStateStoreMigrator {
    private static final Logger log = LoggerFactory.getLogger(PluginPersistentStateStoreMigrator.class);

    private PluginPersistentStateStoreMigrator() {}

    /**
     * Removes directives (e.g. ;singleton:=true) from the plugin keys currently stored in the product's
     * PluginPersistentStateStore.
     *
     * <p><b>It is responsibility of the product</b> to initiate and call when needed this util method in order
     * to migrate its PluginPersistentStateStore(s).
     *
     * @param store the product PluginPersistentStateStore
     */
    public static void removeDirectives(final PluginPersistentStateStore store) {
        new PluginPersistentStateModifier(store).apply(builder -> {
            Map<String, PluginEnabledState> state = builder.toState().getStatesMap();
            Map<String, PluginEnabledState> newState = new HashMap<>(state.size());
            for (Map.Entry<String, PluginEnabledState> entry : state.entrySet()) {
                String key = entry.getKey();
                String newKey = removeDirectivesFromKey(key);
                if (newKey == null) {
                    continue;
                }

                builder.removeState(key);
                if (state.containsKey(newKey)) {
                    log.warn("{} contains both {} and {}", store, key, newKey);
                }
                newState.put(newKey, entry.getValue());
            }
            builder.addPluginEnabledState(newState);
        });
    }

    /**
     *
     * Removes any directives if they are part of the key. E.g. if the key is foo;singleton:=true-1.0.0
     * than it will become foo-1.0.0. If the key doesn't contain any directives, then {@code null} is returned,
     * i.e., {@code null} indicates no-change/no-migration is needed for this key.
     *
     * @param key the original key that needs to be processed
     * @return see above
     */
    public static String removeDirectivesFromKey(String key) {
        if (key.contains(RESTART_STATE_SEPARATOR)) {
            return null;
        }

        int versionBeg = key.indexOf('-');
        if (versionBeg == -1) {
            versionBeg = key.length();
        }

        int directiveBeg = key.indexOf(';');
        if (directiveBeg > -1 && directiveBeg < versionBeg) {
            return key.substring(0, directiveBeg) + key.substring(versionBeg);
        }
        return null;
    }
}
