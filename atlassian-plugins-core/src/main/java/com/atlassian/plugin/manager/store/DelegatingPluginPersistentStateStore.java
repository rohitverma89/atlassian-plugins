package com.atlassian.plugin.manager.store;

import com.atlassian.plugin.manager.PluginPersistentState;
import com.atlassian.plugin.manager.PluginPersistentStateStore;

/**
 * Delegating wrapper for {@link PluginPersistentStateStore}.
 *
 * @since 3.2.0
 */
public abstract class DelegatingPluginPersistentStateStore implements PluginPersistentStateStore {
    /**
     * Obtain the delegate to use for dispatching a request.
     *
     * @return the {@link PluginPersistentStateStore} to delegate calls to, must not be null.
     */
    public abstract PluginPersistentStateStore getDelegate();

    @Override
    public void save(final PluginPersistentState state) {
        getDelegate().save(state);
    }

    @Override
    public PluginPersistentState load() {
        return getDelegate().load();
    }
}
