package com.atlassian.plugin.manager;

import com.atlassian.plugin.ModuleCompleteKey;
import com.atlassian.plugin.ModuleDescriptor;
import com.atlassian.plugin.ModuleDescriptorFactory;
import com.atlassian.plugin.Plugin;
import com.atlassian.plugin.PluginAccessor;
import com.atlassian.plugin.PluginInternal;
import com.atlassian.plugin.PluginParseException;
import com.atlassian.plugin.PluginRegistry;
import com.atlassian.plugin.PluginRestartState;
import com.atlassian.plugin.PluginState;
import com.atlassian.plugin.classloader.PluginsClassLoader;
import com.atlassian.plugin.event.PluginEventManager;
import com.atlassian.plugin.predicate.EnabledModulePredicate;
import com.atlassian.plugin.predicate.ModuleDescriptorOfClassPredicate;
import com.atlassian.plugin.predicate.ModuleDescriptorOfTypePredicate;
import com.atlassian.plugin.predicate.ModuleDescriptorPredicate;
import com.atlassian.plugin.predicate.ModuleOfClassPredicate;
import com.atlassian.plugin.predicate.PluginPredicate;
import com.atlassian.plugin.scope.EverythingIsActiveScopeManager;
import com.atlassian.plugin.scope.ScopeManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Nullable;
import java.io.InputStream;
import java.util.Collection;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static com.atlassian.plugin.util.Assertions.notNull;

public class ProductPluginAccessorBase implements PluginAccessor {
    private static final Logger log = LoggerFactory.getLogger(ProductPluginAccessorBase.class);

    private final PluginRegistry.ReadOnly pluginRegistry;
    private final ModuleDescriptorFactory moduleDescriptorFactory;
    private final ClassLoader classLoader;
    private final PluginPersistentStateStore store;
    private final ScopeManager scopeManager;

    /**
     * @deprecated Use constructor which has ScopeManager instead.
     */
    @Deprecated
    public ProductPluginAccessorBase(final PluginRegistry.ReadOnly pluginRegistry,
                                     final PluginPersistentStateStore store,
                                     final ModuleDescriptorFactory moduleDescriptorFactory,
                                     final PluginEventManager pluginEventManager) {
        this(pluginRegistry, store, moduleDescriptorFactory, pluginEventManager, new EverythingIsActiveScopeManager());
    }

    public ProductPluginAccessorBase(final PluginRegistry.ReadOnly pluginRegistry,
                                     final PluginPersistentStateStore store,
                                     final ModuleDescriptorFactory moduleDescriptorFactory,
                                     final PluginEventManager pluginEventManager,
                                     final ScopeManager scopeManager) {
        this.pluginRegistry = pluginRegistry;
        this.moduleDescriptorFactory = notNull("ModuleDescriptorFactory", moduleDescriptorFactory);
        this.classLoader = new PluginsClassLoader(null, this, pluginEventManager);
        this.store = notNull("PluginPersistentStateStore", store);
        this.scopeManager = notNull("ScopeManager", scopeManager);
    }

    @Override
    public Collection<Plugin> getPlugins() {
        return pluginRegistry.getAll();
    }

    @Override
    public Collection<Plugin> getPlugins(final PluginPredicate pluginPredicate) {
        notNull("pluginPredicate", pluginPredicate);
        return getPlugins().stream()
                .filter(pluginPredicate::matches)
                .collect(Collectors.toList());
    }

    @Override
    public Collection<Plugin> getEnabledPlugins() {
        return getPlugins(p -> PluginState.ENABLED.equals(p.getPluginState()));
    }

    /**
     * Get the modules of all the given descriptor. If any of the getModule() calls fails, the error is recorded in the
     * logs
     *
     * @param moduleDescriptors the collection of module descriptors to get the modules from.
     * @return a {@link Collection} modules that can be any type of object. This collection will not contain any null
     * value.
     */
    private <M> Stream<M> getModules(final Stream<ModuleDescriptor<M>> moduleDescriptors) {
        return moduleDescriptors
                .filter(Objects::nonNull)
                .map(md -> {
                    try {
                        return md.getModule();
                    } catch (final RuntimeException ex) {
                        log.error("Exception when retrieving plugin module {}", md.getCompleteKey(), ex);
                        md.setBroken();
                        return null;
                    }
                })
                .filter(Objects::nonNull);
    }

    /**
     * Get the all the module descriptors from the given collection of plugins, filtered by the predicate.
     * <p>
     * Be careful, your predicate must filter ModuleDescriptors that are not M, this method does not guarantee that the
     * descriptors are of the correct type by itself.
     *
     * @param plugins a collection of {@link Plugin}s
     * @return a stream of {@link ModuleDescriptor descriptors}
     */
    private <M> Stream<ModuleDescriptor<M>> getModuleDescriptors(final Collection<Plugin> plugins, final ModuleDescriptorPredicate<M> predicate) {
        return plugins.stream()
                .flatMap(p -> p.getModuleDescriptors().stream())
                .map(m -> {
                    // hack way to get typed descriptors from plugin and
                    // keep generics happy
                    @SuppressWarnings("unchecked")
                    final ModuleDescriptor<M> cast = (ModuleDescriptor<M>) m;
                    return cast;
                })
                .filter(predicate::matches);
    }

    @Override
    public <M> Collection<M> getModules(final ModuleDescriptorPredicate<M> moduleDescriptorPredicate) {
        notNull("moduleDescriptorPredicate", moduleDescriptorPredicate);
        return getModules(getModuleDescriptors(getPlugins(), moduleDescriptorPredicate))
                .collect(Collectors.toList());
    }

    @Override
    public <M> Collection<ModuleDescriptor<M>> getModuleDescriptors(final ModuleDescriptorPredicate<M> moduleDescriptorPredicate) {
        notNull("moduleDescriptorPredicate", moduleDescriptorPredicate);
        return getModuleDescriptors(getPlugins(), moduleDescriptorPredicate).collect(Collectors.toList());
    }

    @Override
    public Plugin getPlugin(final String key) {
        return pluginRegistry.get(notNull("Plugin key ", key));
    }

    @Override
    public Plugin getEnabledPlugin(final String pluginKey) {
        if (!isPluginEnabled(pluginKey)) {
            return null;
        }
        return getPlugin(pluginKey);
    }

    private ModuleDescriptor<?> getPluginModule(final ModuleCompleteKey key) {
        final Plugin plugin = getPlugin(key.getPluginKey());
        if (plugin == null) {
            return null;
        }
        return plugin.getModuleDescriptor(key.getModuleKey());
    }

    @Override
    public ModuleDescriptor<?> getPluginModule(@Nullable final String completeKey) {
        return getPluginModule(new ModuleCompleteKey(completeKey));
    }

    private boolean isPluginModuleEnabled(final ModuleCompleteKey key) {
        if (!isPluginEnabled(key.getPluginKey())) {
            return false;
        }
        final ModuleDescriptor<?> pluginModule = getPluginModule(key);
        return (pluginModule != null) && pluginModule.isEnabled();
    }

    @Override
    public ModuleDescriptor<?> getEnabledPluginModule(@Nullable final String completeKey) {
        final ModuleCompleteKey key = new ModuleCompleteKey(completeKey);

        // If it's disabled, return null
        if (!isPluginModuleEnabled(key)) {
            return null;
        }

        return getEnabledPlugin(key.getPluginKey()).getModuleDescriptor(key.getModuleKey());
    }

    @Override
    public boolean isPluginEnabled(final String key) throws IllegalArgumentException {
        final Plugin plugin = pluginRegistry.get(notNull("Plugin key", key));

        return plugin != null && plugin.getPluginState() == PluginState.ENABLED;
    }

    @Override
    public boolean isPluginModuleEnabled(@Nullable final String completeKey) {
        return (completeKey != null) && isPluginModuleEnabled(new ModuleCompleteKey(completeKey));
    }

    /**
     * Get all module descriptor that are enabled and for which the module is an instance of the given class.
     *
     * @param moduleClass the class of the module within the module descriptor.
     * @return a stream of {@link ModuleDescriptor}s
     */
    private <M> Stream<ModuleDescriptor<M>> getEnabledModuleDescriptorsByModuleClass(final Class<M> moduleClass) {
        final ModuleOfClassPredicate<M> ofType = new ModuleOfClassPredicate<>(moduleClass);
        final EnabledModulePredicate<M> enabled = new EnabledModulePredicate<>();
        return getModuleDescriptors(getEnabledPlugins(), (ModuleDescriptor<? extends M> md) -> ofType.matches(md) && enabled.matches(md));
    }

    @Override
    public <M> List<M> getEnabledModulesByClass(final Class<M> moduleClass) {
        return getModules(getEnabledModuleDescriptorsByModuleClass(moduleClass))
                .collect(Collectors.toList());
    }

    private <M> List<M> getEnabledModulesByClassAndPredicate(final Class<M> moduleClass, final ModuleDescriptorPredicate<M> predicate) {
        return getModules(getEnabledModuleDescriptorsByModuleClass(moduleClass).filter(predicate::matches))
                .collect(Collectors.toList());
    }

    @Override
    public <M> List<M> getEnabledModulesByClassAndDescriptor(final Class<ModuleDescriptor<M>>[] descriptorClazz, final Class<M> moduleClass) {
        return getEnabledModulesByClassAndPredicate(moduleClass, new ModuleDescriptorOfClassPredicate<>(descriptorClazz));
    }

    @Override
    public <M> List<M> getEnabledModulesByClassAndDescriptor(final Class<ModuleDescriptor<M>> moduleDescriptorClass, final Class<M> moduleClass) {
        return getEnabledModulesByClassAndPredicate(moduleClass, new ModuleDescriptorOfClassPredicate<>(moduleDescriptorClass));
    }

    @Override
    public <D extends ModuleDescriptor<?>> List<D> getEnabledModuleDescriptorsByClass(final Class<D> descriptorClazz) {
        notNull("Descriptor class", descriptorClazz);
        return getEnabledPlugins().stream()
                .flatMap(p -> p.getModuleDescriptors().stream())
                .filter(descriptorClazz::isInstance)
                .filter((new EnabledModulePredicate<>())::matches)
                .map(descriptorClazz::cast)
                .collect(Collectors.toList());
    }

    @Override
    public <D extends ModuleDescriptor<?>> List<D> getActiveModuleDescriptorsByClass(Class<D> descriptorClazz) {
        return getEnabledModuleDescriptorsByClass(descriptorClazz)
                .stream()
                .filter(m -> ScopeManager.isActive(scopeManager, m))
                .collect(Collectors.toList());
    }

    @Override
    public <D extends ModuleDescriptor<?>> List<D> getEnabledModuleDescriptorsByClass(final Class<D> descriptorClazz, final boolean verbose) {
        return getEnabledModuleDescriptorsByClass(descriptorClazz);
    }

    @Override
    public <M> List<ModuleDescriptor<M>> getEnabledModuleDescriptorsByType(final String type)
            throws PluginParseException {
        final ModuleDescriptorOfTypePredicate<M> ofType = new ModuleDescriptorOfTypePredicate<>(moduleDescriptorFactory, type);
        final EnabledModulePredicate<M> enabled = new EnabledModulePredicate<>();
        return getModuleDescriptors(getEnabledPlugins(), (ModuleDescriptor<? extends M> md) -> ofType.matches(md) && enabled.matches(md))
                .collect(Collectors.toList());
    }

    @Override
    public InputStream getDynamicResourceAsStream(final String resourcePath) {
        return getClassLoader().getResourceAsStream(resourcePath);
    }

    @Override
    public InputStream getPluginResourceAsStream(final String pluginKey, final String resourcePath) {
        final Plugin plugin = getEnabledPlugin(pluginKey);
        if (plugin == null) {
            log.error("Attempted to retreive resource " + resourcePath + " for non-existent or inactive plugin " + pluginKey);
            return null;
        }

        return plugin.getResourceAsStream(resourcePath);
    }

    @Override
    public Class<?> getDynamicPluginClass(final String className) throws ClassNotFoundException {
        return getClassLoader().loadClass(className);
    }

    @Override
    public ClassLoader getClassLoader() {
        return classLoader;
    }

    @Override
    public boolean isSystemPlugin(final String key) {
        final Plugin plugin = getPlugin(key);
        return (plugin != null) && plugin.isSystemPlugin();
    }

    @Override
    public PluginRestartState getPluginRestartState(final String key) {
        return store.load().getPluginRestartState(key);
    }

    @Override
    public Iterable<ModuleDescriptor<?>> getDynamicModules(final Plugin plugin) {
        // check the type
        if (plugin instanceof PluginInternal) {
            return ((PluginInternal) plugin).getDynamicModuleDescriptors();
        }
        throw new IllegalArgumentException(plugin + " does not implement com.atlassian.plugin.PluginInternal it is a " + plugin.getClass().getCanonicalName());
    }
}
