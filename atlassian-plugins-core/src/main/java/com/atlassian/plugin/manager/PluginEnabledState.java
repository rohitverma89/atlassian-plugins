package com.atlassian.plugin.manager;

import java.io.Serializable;

import static java.lang.System.currentTimeMillis;

/**
 * The state of a plugin with the current status (enabled/disabled) and a timestamp of last state update
 */
public class PluginEnabledState implements Serializable {
    public static long UNKNOWN_ENABLED_TIME = 0;

    private final boolean enabled;
    private final long timestamp;

    public PluginEnabledState(boolean enabled, long timestamp) {
        this.enabled = enabled;
        this.timestamp = timestamp;
    }

    @Deprecated
    public PluginEnabledState(boolean enabled) {
        this(enabled, UNKNOWN_ENABLED_TIME);
    }

    public Boolean isEnabled() {
        return enabled;
    }

    public long getTimestamp() {
        return timestamp;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        PluginEnabledState that = (PluginEnabledState) o;

        return enabled == that.enabled;
    }

    @Override
    public int hashCode() {
        return (enabled ? 1 : 0);
    }

    public static PluginEnabledState getPluginEnabledStateWithCurrentTime(boolean enabled){
        return new PluginEnabledState(enabled, currentTimeMillis());
    }
}
