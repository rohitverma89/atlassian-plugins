package com.atlassian.plugin.scope;

/**
 * Allows scope checks to be introduced ahead of TCS based implementation
 *
 * @since 4.1
 */
public class EverythingIsActiveScopeManager implements ScopeManager {

    @Override
    public boolean isScopeActive(String scopeKey) {
        return true;
    }
}
