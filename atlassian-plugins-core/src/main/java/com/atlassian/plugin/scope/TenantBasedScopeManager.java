package com.atlassian.plugin.scope;

/**
 * Yet to be implemented.
 * <p>
 * This service will be backed up by TCS and will do scope checks for implicitly derived user/tenant
 *
 * @since 4.1
 */
public class TenantBasedScopeManager implements ScopeManager {
    @Override
    public boolean isScopeActive(String scopeKey) {
        throw new UnsupportedOperationException("Not implemented");
    }
}
