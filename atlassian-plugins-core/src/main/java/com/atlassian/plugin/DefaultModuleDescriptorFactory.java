package com.atlassian.plugin;

import com.atlassian.plugin.hostcontainer.DefaultHostContainer;
import com.atlassian.plugin.hostcontainer.HostContainer;
import com.atlassian.plugin.util.ClassLoaderUtils;
import com.atlassian.util.concurrent.CopyOnWriteMap;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import static java.util.Collections.unmodifiableMap;

/**
 * Default implementation of a descriptor factory that allows filtering of
 * descriptor keys
 */
public class DefaultModuleDescriptorFactory implements ModuleDescriptorFactory {
    private static Logger log = LoggerFactory.getLogger(DefaultModuleDescriptorFactory.class);

    private final Map<String, Class<? extends ModuleDescriptor>> moduleDescriptorClasses = CopyOnWriteMap.<String, Class<? extends ModuleDescriptor>>builder().stableViews()
            .newHashMap();
    private final List<String> permittedModuleKeys = new ArrayList<String>();
    private final HostContainer hostContainer;

    /**
     * @deprecated Since 2.2.0, use
     * {@link #DefaultModuleDescriptorFactory(HostContainer)}
     * instead
     */
    @Deprecated
    public DefaultModuleDescriptorFactory() {
        this(new DefaultHostContainer());
    }

    /**
     * Instantiates a descriptor factory that uses the host container to create
     * descriptors
     *
     * @param hostContainer The host container implementation for descriptor
     *                      creation
     * @since 2.2.0
     */
    public DefaultModuleDescriptorFactory(final HostContainer hostContainer) {
        this.hostContainer = hostContainer;
    }

    public Class<? extends ModuleDescriptor> getModuleDescriptorClass(final String type) {
        return moduleDescriptorClasses.get(type);
    }

    public ModuleDescriptor<?> getModuleDescriptor(final String type) throws PluginParseException, IllegalAccessException, InstantiationException, ClassNotFoundException {
        if (shouldSkipModuleOfType(type)) {
            return null;
        }

        final Class<? extends ModuleDescriptor> moduleDescriptorClazz = getModuleDescriptorClass(type);

        if (moduleDescriptorClazz == null) {
            throw new PluginParseException("Cannot find ModuleDescriptor class for plugin of type '" + type + "'.");
        }

        return hostContainer.create(moduleDescriptorClazz);
    }

    protected boolean shouldSkipModuleOfType(final String type) {
        synchronized (permittedModuleKeys) {
            return !permittedModuleKeys.isEmpty() && !permittedModuleKeys.contains(type);
        }
    }

    @SuppressWarnings("unused")
    public void setModuleDescriptors(final Map<String, String> moduleDescriptorClassNames) {
        for (final Entry<String, String> entry : moduleDescriptorClassNames.entrySet()) {
            final Class<ModuleDescriptor<?>> descriptorClass = getClassFromEntry(entry);
            if (descriptorClass != null) {
                addModuleDescriptor(entry.getKey(), descriptorClass);
            }
        }
    }

    private <D extends ModuleDescriptor<?>> Class<D> getClassFromEntry(final Map.Entry<String, String> entry) {
        if (shouldSkipModuleOfType(entry.getKey())) {
            return null;
        }

        try {
            final Class<D> descriptorClass = ClassLoaderUtils.loadClass(entry.getValue(), getClass());

            if (!ModuleDescriptor.class.isAssignableFrom(descriptorClass)) {
                log.error("Configured plugin module descriptor class " + entry.getValue() + " does not inherit from ModuleDescriptor");
                return null;
            }
            return descriptorClass;
        } catch (final ClassNotFoundException e) {
            log.error("Unable to add configured plugin module descriptor " + entry.getKey() + ". Class not found: " + entry.getValue());
            return null;
        }
    }

    public boolean hasModuleDescriptor(final String type) {
        return moduleDescriptorClasses.containsKey(type);
    }

    public void addModuleDescriptor(final String type, final Class<? extends ModuleDescriptor> moduleDescriptorClass) {
        moduleDescriptorClasses.put(type, moduleDescriptorClass);
    }

    public void removeModuleDescriptorForType(final String type) {
        moduleDescriptorClasses.remove(type);
    }

    protected final Map<String, Class<? extends ModuleDescriptor>> getDescriptorClassesMap() {
        return unmodifiableMap(moduleDescriptorClasses);
    }

    /**
     * Sets the list of module keys that will be loaded. If this list is empty,
     * then the factory will permit all recognised module types to load. This
     * allows you to run the plugin system in a 'restricted mode'
     *
     * @param permittedModuleKeys List of (String) keys
     */
    public void setPermittedModuleKeys(List<String> permittedModuleKeys) {
        if (permittedModuleKeys == null) {
            permittedModuleKeys = Collections.emptyList();
        }

        synchronized (this.permittedModuleKeys) {
            // synced
            this.permittedModuleKeys.clear();
            this.permittedModuleKeys.addAll(permittedModuleKeys);
        }
    }
}
