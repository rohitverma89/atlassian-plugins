package com.atlassian.plugin;

import com.google.common.base.Function;
import com.google.common.base.Predicate;
import com.google.common.collect.Sets;
import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Nullable;
import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Set;
import java.util.jar.JarEntry;
import java.util.jar.JarFile;
import java.util.jar.Manifest;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.zip.ZipEntry;

import static com.google.common.base.Preconditions.checkNotNull;
import static com.google.common.collect.Iterators.any;
import static com.google.common.collect.Iterators.filter;
import static com.google.common.collect.Iterators.forEnumeration;
import static com.google.common.collect.Iterators.transform;

/**
 * The implementation of PluginArtifact that is backed by a jar file.
 *
 * @see PluginArtifact
 * @since 2.0.0
 */
public final class JarPluginArtifact implements PluginArtifact, PluginArtifact.AllowsReference, PluginArtifact.HasExtraModuleDescriptors {
    private static final Logger log = LoggerFactory.getLogger(JarPluginArtifact.class);

    private final File jarFile;
    final com.atlassian.plugin.ReferenceMode referenceMode;

    /**
     * Construct a PluginArtifact for a jar file which does not allow reference installation.
     *
     * @param jarFile the jar file comprising the artifact.
     */
    public JarPluginArtifact(File jarFile) {
        this(jarFile, com.atlassian.plugin.ReferenceMode.FORBID_REFERENCE);
    }

    /**
     * Construct a PluginArtifact for a jar file and specify whether reference installation is supported.
     *
     * @param jarFile       the jar file comprising the artifact.
     * @param referenceMode specifies whether this artifact may be installed by reference.
     */
    public JarPluginArtifact(File jarFile, com.atlassian.plugin.ReferenceMode referenceMode) {
        this.jarFile = checkNotNull(jarFile);
        this.referenceMode = referenceMode;
    }

    /**
     * Construct a PluginArtifact for a jar file and specify whether reference installation is supported.
     *
     * @param jarFile       the jar file comprising the artifact.
     * @param referenceMode The legacy {@link com.atlassian.plugin.PluginArtifact.AllowsReference.ReferenceMode}  whose
     *                      modern equivalent {@link com.atlassian.plugin.ReferenceMode} specifies whether this artifact may be installed by reference.
     * @deprecated since 4.0.0, to be removed in 5.0.0: Use {@link JarPluginArtifact(com.atlassian.plugin.ReferenceMode)}
     * which uses the non legacy {@link com.atlassian.plugin.ReferenceMode}.
     */
    public JarPluginArtifact(File jarFile, com.atlassian.plugin.PluginArtifact.AllowsReference.ReferenceMode referenceMode) {
        this(jarFile, referenceMode.toModern());
    }

    public boolean doesResourceExist(String name) {
        InputStream in = null;
        try {
            in = getResourceAsStream(name);
            return (in != null);
        } finally {
            IOUtils.closeQuietly(in);
        }
    }

    /**
     * @return an input stream for the this file in the jar. Closing this stream also closes the jar file this stream comes from.
     */
    public InputStream getResourceAsStream(String fileName) throws PluginParseException {
        checkNotNull(fileName, "The file name must not be null");

        final JarFile jar = open();
        final ZipEntry entry = jar.getEntry(fileName);
        if (entry == null) {
            closeJarQuietly(jar);
            return null;
        }

        try {
            return new BufferedInputStream(jar.getInputStream(entry)) {
                // because we do not expose a handle to the jar file this stream is associated with, we need to make sure
                // we explicitly close the jar file when we're done with the stream (else we'll have a file handle leak)
                public void close() throws IOException {
                    super.close();
                    jar.close();
                }
            };
        } catch (IOException e) {
            throw new PluginParseException("Cannot retrieve " + fileName + " from plugin JAR [" + jarFile + "]", e);
        }
    }

    public String getName() {
        return jarFile.getName();
    }

    @Override
    public String toString() {
        return getName();
    }

    /**
     * @return a buffered file input stream of the file on disk. This input stream
     * is not resettable.
     */
    public InputStream getInputStream() {
        try {
            return new BufferedInputStream(new FileInputStream(jarFile));
        } catch (FileNotFoundException e) {
            throw new PluginParseException("Could not open JAR file: " + jarFile, e);
        }
    }

    public File toFile() {
        return jarFile;
    }

    @Override
    public boolean containsJavaExecutableCode() {
        final JarFile jar = open();
        try {
            final Manifest manifest = getManifest(jar);
            return hasBundleActivator(manifest)
                    ||
                    hasSpringContext(manifest)
                    ||
                    any(forEnumeration(jar.entries()), new Predicate<JarEntry>() {
                        @Override
                        public boolean apply(JarEntry entry) {
                            return isJavaClass(entry) || isJavaLibrary(entry) || isSpringContext(entry);
                        }
                    });
        } finally {
            closeJarQuietly(jar);
        }
    }

    @Override
    public boolean containsSpringContext() {
        final JarFile jar = open();
        try {
            final Manifest manifest = getManifest(jar);
            return hasSpringContext(manifest)
                    ||
                    any(forEnumeration(jar.entries()), new Predicate<JarEntry>() {
                        @Override
                        public boolean apply(JarEntry entry) {
                            return isSpringContext(entry);
                        }
                    });
        } finally {
            closeJarQuietly(jar);
        }
    }

    @Override
    public Set<String> extraModuleDescriptorFiles(String rootFolder) {
        final JarFile jar = open();
        try {
            final Matcher m = Pattern.compile(Pattern.quote(rootFolder) + "/[^/.]*\\.(?i)xml$").matcher("");
            return Sets.newHashSet(transform(filter(forEnumeration(jar.entries()),
                    new Predicate<JarEntry>() {
                        @Override
                        public boolean apply(JarEntry entry) {
                            m.reset(entry.getName());
                            return m.find();
                        }
                    }), new Function<JarEntry, String>() {
                @Override
                public String apply(@Nullable JarEntry jarEntry) {
                    return jarEntry.getName();
                }
            }));
        } finally {
            closeJarQuietly(jar);
        }
    }

    @Override
    public com.atlassian.plugin.ReferenceMode getReferenceMode() {
        return referenceMode;
    }

    /**
     * Legacy interface implementation which forwards to {@link #getReferenceMode} and {@link ReferenceMode#allowsReference}.
     *
     * @return {@code getReferenceMode().allowsReference()}
     * @deprecated since 4.0.0, to be removed in 5.0.0: Equivalent functionality is now provided via {@link #getReferenceMode()}.
     */
    @Override
    public boolean allowsReference() {
        return getReferenceMode().allowsReference();
    }

    private boolean isJavaClass(ZipEntry entry) {
        return entry.getName().endsWith(".class");
    }

    private boolean isJavaLibrary(ZipEntry entry) {
        return entry.getName().endsWith(".jar");
    }

    private boolean isSpringContext(ZipEntry entry) {
        final String entryName = entry.getName();
        return entryName.startsWith("META-INF/spring/") && entryName.endsWith(".xml");
    }

    private boolean hasSpringContext(Manifest manifest) {
        return hasManifestEntry(manifest, "Spring-Context");
    }

    private boolean hasBundleActivator(Manifest manifest) {
        return hasManifestEntry(manifest, "Bundle-Activator");
    }

    private boolean hasManifestEntry(Manifest manifest, String manifestEntryName) {
        return manifest != null
                && manifest.getMainAttributes() != null
                && manifest.getMainAttributes().getValue(manifestEntryName) != null;
    }

    private JarFile open() {
        try {
            return new JarFile(jarFile);
        } catch (IOException e) {
            throw new PluginParseException("Cannot open JAR file: " + jarFile, e);
        }
    }

    private Manifest getManifest(JarFile jar) {
        try {
            return jar.getManifest();
        } catch (IOException e) {
            throw new PluginParseException("Cannot get manifest for JAR file: " + jarFile, e);
        }
    }

    private void closeJarQuietly(JarFile jar) {
        if (jar != null) {
            try {
                jar.close();
            } catch (IOException ignored) {
                log.debug("Exception closing jar file " + jarFile + ".", ignored);
            }
        }
    }
}
