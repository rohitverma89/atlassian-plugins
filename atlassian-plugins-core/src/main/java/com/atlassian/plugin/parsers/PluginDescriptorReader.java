package com.atlassian.plugin.parsers;

import com.atlassian.fugue.Option;
import com.atlassian.plugin.Application;
import com.atlassian.plugin.InstallationMode;
import com.atlassian.plugin.Plugin;
import com.atlassian.plugin.Resources;
import com.atlassian.plugin.util.PluginUtils;
import com.google.common.base.Function;
import com.google.common.base.Predicate;
import com.google.common.base.Predicates;
import com.google.common.collect.ImmutableList;
import org.dom4j.Document;
import org.dom4j.Element;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Nullable;
import java.util.List;
import java.util.Set;

import static com.atlassian.fugue.Iterables.findFirst;
import static com.atlassian.plugin.parsers.PluginInformationReader.PLUGIN_INFO;
import static com.atlassian.plugin.parsers.XmlDescriptorParserUtils.removeAllNamespaces;
import static com.google.common.base.Preconditions.checkNotNull;
import static com.google.common.collect.ImmutableSet.copyOf;
import static com.google.common.collect.Iterables.filter;
import static com.google.common.collect.Iterables.transform;

/**
 * A simple class to read the basic elements of a plugin descriptor.
 *
 * @since 3.0.0
 */
public final class PluginDescriptorReader {
    private static final Logger log = LoggerFactory.getLogger(PluginDescriptorReader.class);

    static final String RESOURCE = "resource";

    private final Document descriptor;
    private final Set<Application> applications;

    public PluginDescriptorReader(Document descriptor, Set<Application> applications) {
        this.descriptor = removeAllNamespaces(checkNotNull(descriptor));
        this.applications = copyOf(checkNotNull(applications));
    }

    public Document getDescriptor() {
        return descriptor;
    }

    private Element getPluginElement() {
        return descriptor.getRootElement();
    }

    public String getPluginKey() {
        return getPluginElement().attributeValue("key");
    }

    public String getPluginName() {
        return getPluginElement().attributeValue("name");
    }

    public boolean isSystemPlugin() {
        return Boolean.valueOf(getPluginElement().attributeValue("system"));
    }

    public Option<String> getI18nPluginNameKey() {
        return Option.option(getPluginElement().attributeValue("i18n-name-key"));
    }

    public boolean isEnabledByDefault() {
        return !"disabled".equalsIgnoreCase(getPluginElement().attributeValue("state"));
    }

    public Option<Element> getPluginInformation() {
        return findFirst(elements(getPluginElement()), new ElementWithName(PLUGIN_INFO));
    }

    public PluginInformationReader getPluginInformationReader() {
        return new PluginInformationReader(getPluginInformation(), applications, getPluginsVersion());
    }

    public Iterable<Element> getModules(final InstallationMode installationMode) {
        return filter(filter(elements(getPluginElement()), Predicates.not(Predicates.or(new ElementWithName(PLUGIN_INFO), new ElementWithName(RESOURCE)))),
                new Predicate<Element>() {
                    @Override
                    public boolean apply(Element module) {
                        if (!PluginUtils.doesModuleElementApplyToApplication(module, applications, installationMode)) {
                            log.debug("Ignoring module descriptor for this application: {}", module.attributeValue("key"));
                            return false;
                        }
                        return true;
                    }
                });
    }

    public Iterable<ModuleReader> getModuleReaders(InstallationMode installationMode) {
        return transform(getModules(installationMode), new Function<Element, ModuleReader>() {
            @Override
            public ModuleReader apply(Element module) {
                return new ModuleReader(module);
            }
        });
    }

    public Resources getResources() {
        return Resources.fromXml(getPluginElement());
    }

    public int getPluginsVersion() {
        String val = getPluginElement().attributeValue("pluginsVersion");
        if (val == null) {
            val = getPluginElement().attributeValue("plugins-version");
        }
        if (val != null) {
            try {
                return Integer.parseInt(val);
            } catch (final NumberFormatException e) {
                throw new RuntimeException("Could not parse pluginsVersion: " + e.getMessage(), e);
            }
        } else {
            return Plugin.VERSION_1;
        }
    }

    @SuppressWarnings("unchecked")
    static List<Element> elements(Element e) {
        return e.elements();
    }

    @SuppressWarnings("unchecked")
    static List<Element> elements(Element e, String name) {
        return e != null ? e.elements(name) : ImmutableList.of();
    }

    private static final class ElementWithName implements Predicate<Element> {
        private final String name;

        private ElementWithName(String name) {
            this.name = checkNotNull(name);
        }

        @Override
        public boolean apply(@Nullable Element element) {
            return element != null && name.equalsIgnoreCase(element.getName());
        }
    }
}
