package com.atlassian.plugin.parsers;

import com.atlassian.plugin.Application;
import com.atlassian.plugin.PluginParseException;

import java.io.InputStream;
import java.util.Set;

/**
 * A factory which creates a DescriptorParserFactory by composing together multiple XML descriptors
 *
 * @since 3.2.16
 */
public interface CompositeDescriptorParserFactory extends DescriptorParserFactory {
    /**
     * Creates a new {@link DescriptorParser} for getting plugin descriptor information
     * from the provided source data and supplemental module descriptors.
     *
     * @param source              the stream of data which represents the descriptor. The stream will
     *                            only be read once, so it need not be resettable.
     * @param supplementalSources streams of data representing supplemental plugin information
     * @param applications        The list of application keys to match for module descriptors
     * @return an instance of the descriptor parser tied to this InputStream
     * @throws com.atlassian.plugin.PluginParseException if there was a problem creating the descriptor parser
     *                                                   due to an invalid source stream.
     * @since 3.2.15
     */
    DescriptorParser getInstance(InputStream source, Iterable<InputStream> supplementalSources, Set<Application> applications) throws PluginParseException;
}
