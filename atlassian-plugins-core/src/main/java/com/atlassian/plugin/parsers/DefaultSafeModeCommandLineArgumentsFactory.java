package com.atlassian.plugin.parsers;


import com.atlassian.util.concurrent.LazyReference;
import com.google.common.annotations.VisibleForTesting;

import javax.annotation.Nonnull;

import static java.lang.System.getProperty;

/**
 * This is used by the application this library is bundled with, it shouldn't be changed without also changing all the products it is bundled with.
 */
public class DefaultSafeModeCommandLineArgumentsFactory implements SafeModeCommandLineArgumentsFactory {
    private static final String PARAMETER_SYSTEM_PROPERTY = "atlassian.plugins.startup.options";
    private LazyReference<SafeModeCommandLineArguments> safeModeCommandLineArguments = new LazyReference<SafeModeCommandLineArguments>() {
        @Override
        protected SafeModeCommandLineArguments create() throws Exception {
            return new SafeModeCommandLineArguments(getProperty(PARAMETER_SYSTEM_PROPERTY, ""));
        }
    };

    /**
     * The name of the system property that contains the startup parameters passed to start-<product>.sh
     * (for example in the case <code>start-jira.sh -fg --disable-plugins=com.atlassian.xyz</code> this system property
     * would be set to <code>-fg --disable-plugins=com.atlassian.xyz</code>
     * This requires support from the host product
     *
     * @return The name of the system property
     */
    @VisibleForTesting
    @Nonnull
    String getParameterSystemProperty() {
        return PARAMETER_SYSTEM_PROPERTY;
    }

    @Override
    public SafeModeCommandLineArguments get() {
        return safeModeCommandLineArguments.get();

    }
}
