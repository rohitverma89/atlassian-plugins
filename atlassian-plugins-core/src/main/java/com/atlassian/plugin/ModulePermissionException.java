package com.atlassian.plugin;

import com.google.common.collect.ImmutableSet;

import java.util.Set;

import static com.google.common.base.Preconditions.checkNotNull;

/**
 * This exception is thrown by {@link ModuleDescriptor module descriptors} when a set of their required permissions is
 * not met by the plugin declaring them.
 *
 * @since 3.0
 */
public final class ModulePermissionException extends PluginException {
    private final String moduleKey;
    private final Set<String> permissions;

    public ModulePermissionException(String moduleKey, Set<String> permissions) {
        super("Could not load module " + moduleKey + ". The plugin is missing the following permissions: " + permissions);
        this.moduleKey = checkNotNull(moduleKey);
        this.permissions = ImmutableSet.copyOf(checkNotNull(permissions));
    }

    public String getModuleKey() {
        return moduleKey;
    }

    public Set<String> getPermissions() {
        return permissions;
    }
}
