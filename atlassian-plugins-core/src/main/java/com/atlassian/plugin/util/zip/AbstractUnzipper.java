package com.atlassian.plugin.util.zip;

import com.google.common.annotations.VisibleForTesting;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FilenameFilter;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

public abstract class AbstractUnzipper implements Unzipper {
    protected static Logger log = LoggerFactory.getLogger(FileUnzipper.class);
    protected File destDir;

    protected File saveEntry(InputStream is, ZipEntry entry) throws IOException {
        final File file = new File(destDir, normaliseAndVerify(entry.getName()));

        if (entry.isDirectory()) {
            file.mkdirs();
        } else {
            final File dir = new File(file.getParent());
            dir.mkdirs();

            FileOutputStream fos = null;
            try {
                fos = new FileOutputStream(file);
                IOUtils.copy(is, fos);
                fos.flush();
            } catch (FileNotFoundException fnfe) {
                log.error("Error extracting a file to '" + destDir + File.separator + entry.getName() + "'. This destination is invalid for writing an extracted file stream to. ");
                return null;
            } finally {
                IOUtils.closeQuietly(fos);
            }
        }
        file.setLastModified(entry.getTime());

        return file;
    }

    protected ZipEntry[] entries(ZipInputStream zis) throws IOException {
        List<ZipEntry> entries = new ArrayList<ZipEntry>();
        try {
            ZipEntry zipEntry = zis.getNextEntry();
            while (zipEntry != null) {
                entries.add(zipEntry);
                zis.closeEntry();
                zipEntry = zis.getNextEntry();
            }
        } finally {
            IOUtils.closeQuietly(zis);
        }

        return entries.toArray(new ZipEntry[entries.size()]);
    }

    public void conditionalUnzip() throws IOException {
        Map<String, Long> zipContentsAndLastModified = new HashMap<String, Long>();

        ZipEntry[] zipEntries = entries();
        for (int i = 0; i < zipEntries.length; i++) {
            zipContentsAndLastModified.put(zipEntries[i].getName(), zipEntries[i].getTime());
        }

        // If the jar contents of the directory does not match the contents of the zip
        // The we will nuke the bundled plugins directory and re-extract.
        Map<String, Long> targetDirContents = getContentsOfTargetDir(destDir);
        if (!targetDirContents.equals(zipContentsAndLastModified)) {
            // Note: clean, not delete, as destdir may be a symlink (PLUG-606).
            if (destDir.exists()) {
                FileUtils.cleanDirectory(destDir);
            }
            unzip();
        } else {
            log.debug("Target directory contents match zip contents. Do nothing.");
        }
    }

    /**
     * Normalises the supplied path name, by removing all intermediate {@code ..} constructs
     * and ensures that resulting the path name is not a relative path.
     * <p>
     * Examples are:
     * <ul>
     * <li>Passing {@code foo} will return {@code foo}
     * <li>Passing {@code dir/../foo} will return {@code foo}
     * <li>Passing {@code dir/../../foo} will throw an IllegalArgumentException
     * </ul>
     *
     * @param name the name to be normalised and verified
     * @return the normalised name, that is not relative.
     */
    @VisibleForTesting
    static String normaliseAndVerify(String name) {
        final String normalised = FilenameUtils.normalizeNoEndSeparator(name);
        if (StringUtils.isBlank(normalised)) {
            throw new IllegalArgumentException("Path name " + name + " is illegal");
        }
        return normalised;
    }

    private Map<String, Long> getContentsOfTargetDir(File dir) {
        // Create filter that lists only jars
        final FilenameFilter filter = new FilenameFilter() {
            @Override
            public boolean accept(File dir, String name) {
                return name.endsWith(".jar");
            }
        };


        if (!dir.isDirectory()) {
            return Collections.emptyMap();
        }

        File[] files = dir.listFiles();
        if (files == null) {
            return Collections.emptyMap();
        }

        Map<String, Long> targetDirContents = new HashMap<String, Long>();
        for (File child : files) {
            if (log.isDebugEnabled()) {
                log.debug("Examining entry in zip: " + child);
            }
            targetDirContents.put(child.getName(), child.lastModified());
        }

        return targetDirContents;
    }
}
