package com.atlassian.plugin.util;

/**
 * Utilities for Java Enum classes.
 *
 * @since v3.2.20
 */
public class EnumUtils {
    /**
     * Obtain an enum value by looking up the name in a given property, defaulting if necessary.
     *
     * @param propertyName the name of the property containing the stringified enum value.
     * @param values       the value of the enumeration, usually {@code E.values()}.
     * @param defaultValue the value to default to if the property is unset or does not match {@code E.name()} for some element
     *                     of {@code values}.
     * @return the corresponding enum value if the property is set and recognized, or {@code defaultValue} otherwise.
     * @since v3.2.20
     */
    public static <E extends Enum<E>> E enumValueFromProperty(final String propertyName, final E[] values, final E defaultValue) {
        final String propertyValue = System.getProperty(propertyName);
        if (null != propertyValue) {
            for (final E value : values) {
                if (value.name().equalsIgnoreCase(propertyValue)) {
                    return value;
                }
            }
        }
        return defaultValue;
    }
}
