package com.atlassian.plugin.util;

import com.atlassian.annotations.Internal;

import java.util.Collection;

/**
 * Utility functions for constructing regular expressions.
 *
 * The methods here take and return strings to faciliate composing operations without
 * needing to compile temporary patterns.
 */
@Internal
public class RegularExpressions {
    /**
     * Obtain a regular expression which matches any one of a given collection of expressions.
     *
     * If the provided collection is empty, a regular expression which matches no string (not even
     * the empty string) is returned.
     *
     * @param expressions the individual expressions to compose.
     * @return an expression which matches when any one of expressions matches.
     */
    public static String anyOf(final Collection<String> expressions) {
        if (expressions.isEmpty()) {
            // Nothing matches "Something followed by start of string"
            return ".\\A";
        } else {
            // We're going to construct the following expression - spaces added for clarity, <i> are
            // the expressions:
            // (?:(?: <0> )|(?: <1> )|(?: <2> ... )|(?: <n-1> ))
            // Figure out how big the resulting expression will be - break first 6 up as 1 + 5 which
            // is what the loop below does also:
            // ( ?:(?: <0> )|(?: <1> )|(?: <2> ... )|(?: <n-1> ))
            int capacity = 1 + 5 * expressions.size() + 2;
            for (final String expression : expressions) {
                capacity += expression.length();
            }
            // Now construct the compound regular expression
            final StringBuilder compound = new StringBuilder(capacity);
            compound.append('(');
            for (final String expression : expressions) {
                compound.append(")|(?:");
                compound.append(expression);
            }
            compound.append("))");
            // Now compound starts ()|(?: and we want (?:(?:
            compound.setCharAt(1, '?');
            compound.setCharAt(2, ':');
            return compound.toString();
        }
    }
}
