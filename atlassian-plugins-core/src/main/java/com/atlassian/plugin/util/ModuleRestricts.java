package com.atlassian.plugin.util;

import com.atlassian.fugue.Option;
import com.atlassian.plugin.Application;
import com.atlassian.plugin.InstallationMode;
import com.google.common.base.Function;
import com.google.common.base.Objects;
import com.google.common.base.Predicate;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.Iterables;
import org.apache.commons.lang.StringUtils;
import org.dom4j.Element;

import java.util.List;
import java.util.Set;

import static com.atlassian.fugue.Suppliers.alwaysTrue;
import static com.google.common.base.Preconditions.checkNotNull;
import static com.google.common.base.Preconditions.checkState;
import static com.google.common.collect.Iterables.filter;
import static com.google.common.collect.Iterables.transform;
import static com.google.common.collect.Lists.newArrayList;

/**
 * Represents a list of {@link ModuleRestrict}. This represents the restricts
 * as used in the plugin descriptor:
 * <code><br>
 * &nbsp;&nbsp;&lt;module key="module-key"><br>
 * &nbsp;&nbsp;&nbsp;&nbsp;&lt;restrict ...>...&lt;/restrict><br>
 * &nbsp;&nbsp;&nbsp;&nbsp;&lt;restrict ...>...&lt;/restrict><br>
 * &nbsp;&nbsp;&lt;/module>
 * </code>
 *
 * @since 3.0
 */
final class ModuleRestricts {
    final Iterable<ModuleRestrict> restricts;

    private ModuleRestricts() {
        this(ImmutableList.<ModuleRestrict>of());
    }

    private ModuleRestricts(Iterable<ModuleRestrict> restricts) {
        this.restricts = ImmutableList.copyOf(restricts);
    }

    static ModuleRestricts parse(Element moduleElement) {
        final String applicationKeys = moduleElement.attributeValue("application");
        if (applicationKeys != null) {
            return parseApplicationsFromAttribute(applicationKeys);
        } else if (!moduleElement.elements("restrict").isEmpty()) {
            @SuppressWarnings("unchecked")
            final List<Element> restrict = moduleElement.elements("restrict");
            return parseApplicationsFromRestrictElements(restrict);
        } else {
            return new ModuleRestricts();
        }
    }

    private static ModuleRestricts parseApplicationsFromRestrictElements(List<Element> restrictElements) {
        return new ModuleRestricts(Iterables.transform(restrictElements, new Function<Element, ModuleRestrict>() {
            @Override
            public ModuleRestrict apply(Element restrictElement) {
                final String application = restrictElement.attributeValue("application");
                checkState(application != null, "No application defined for 'restrict' element.");
                return new ModuleRestrict(application, parseInstallationMode(restrictElement), parseVersionRange(restrictElement));
            }
        }));
    }

    private static Option<InstallationMode> parseInstallationMode(Element restrictElement) {
        return InstallationMode.of(restrictElement.attributeValue("mode"));
    }

    private static VersionRange parseVersionRange(Element restrictElement) {
        final String version = restrictElement.attributeValue("version");
        if (version != null) {
            return VersionRange.parse(version);
        } else {
            final List<Element> versionElements = restrictElement.elements("version");
            if (!versionElements.isEmpty()) {
                VersionRange range = VersionRange.empty();
                for (Element versionElement : versionElements) {
                    range = range.or(VersionRange.parse(versionElement.getText()));
                }
                return range;
            } else {
                return VersionRange.all();
            }
        }
    }

    private static ModuleRestricts parseApplicationsFromAttribute(String applicationKeys) {
        final String[] keys = applicationKeys.split("\\s*,[,\\s]*");
        final Iterable<ModuleRestrict> restricts = transform(filter(newArrayList(keys), new IsNotBlankPredicate()), new Function<String, ModuleRestrict>() {
            @Override
            public ModuleRestrict apply(String app) {
                return new ModuleRestrict(app);
            }
        });

        return new ModuleRestricts(restricts);
    }

    public boolean isValidFor(Set<Application> applications, InstallationMode mode) {
        if (Iterables.isEmpty(restricts)) {
            return true;
        }

        for (Application application : applications) {
            if (Iterables.any(restricts, new RestrictMatchesApplication(application, mode))) {
                return true;
            }
        }
        return false;
    }

    @Override
    public String toString() {
        return restricts.toString();
    }

    static final class ModuleRestrict {
        final String application;
        final Option<InstallationMode> mode;
        final VersionRange version;

        ModuleRestrict(String application) {
            this(application, Option.<InstallationMode>none());
        }

        ModuleRestrict(String application, Option<InstallationMode> mode) {
            this(application, mode, VersionRange.all());
        }

        ModuleRestrict(String application, Option<InstallationMode> mode, VersionRange version) {
            this.application = checkNotNull(application);
            this.mode = checkNotNull(mode);
            this.version = checkNotNull(version);
        }

        @Override
        public String toString() {
            return Objects.toStringHelper("restrict")
                    .add("application", application)
                    .add("mode", mode)
                    .add("range", version)
                    .toString();
        }
    }

    private static final class IsNotBlankPredicate implements Predicate<String> {
        @Override
        public boolean apply(String input) {
            return StringUtils.isNotBlank(input);
        }
    }

    private static final class RestrictMatchesApplication implements Predicate<ModuleRestrict> {
        private final Application app;
        private final InstallationMode installationMode;

        public RestrictMatchesApplication(Application app, InstallationMode installationMode) {
            this.app = checkNotNull(app);
            this.installationMode = checkNotNull(installationMode);
        }

        @Override
        public boolean apply(ModuleRestrict restrict) {
            return restrict.application.equals(app.getKey())
                    && isInstallModeValid(restrict.mode)
                    && restrict.version.isInRange(app.getVersion());
        }

        private boolean isInstallModeValid(Option<InstallationMode> mode) {
            return mode.fold(alwaysTrue(), new Function<InstallationMode, Boolean>() {
                @Override
                public Boolean apply(InstallationMode m) {
                    return m.equals(installationMode);
                }
            });
        }
    }
}
