package com.atlassian.plugin.exception;

import com.atlassian.annotations.Internal;
import com.atlassian.plugin.Plugin;

/**
 * This allows the Host application to intercept a plugin exception and do something meaningful with it.   Its is not
 * intended for plugin developers to consume at all.
 *
 * @since v3.0
 */
@Internal
public interface PluginExceptionInterception {
    /**
     * This is called when a plugin cant be enabled because an exception has been thrown.
     * <p/>
     * This is an intercept point for the host to do something meaningful like better logging or failed plugin
     * tracking.
     * <p/>
     * It must NOT propagate the exception out of this block because this will stop the plugin system in general for
     * starting.
     *
     * @param plugin          the plugin that has failed to be enabled
     * @param pluginException the exception that was thrown during plugin enable
     * @return true if the underlying plugin system should log a message.  If this is false then the assumption is that
     * host application has done the appropriate logging
     */
    boolean onEnableException(Plugin plugin, Exception pluginException);
}
