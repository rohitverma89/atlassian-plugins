/*
 * Created by IntelliJ IDEA.
 * User: Mike
 * Date: Jul 29, 2004
 * Time: 4:28:18 PM
 */
package com.atlassian.plugin.descriptors;

import com.atlassian.plugin.ModuleDescriptor;
import com.atlassian.plugin.ModulePermissionException;
import com.atlassian.plugin.Permissions;
import com.atlassian.plugin.Plugin;
import com.atlassian.plugin.PluginParseException;
import com.atlassian.plugin.RequirePermission;
import com.atlassian.plugin.StateAware;
import com.atlassian.plugin.elements.ResourceDescriptor;
import com.atlassian.plugin.impl.StaticPlugin;
import com.atlassian.plugin.mock.MockAnimal;
import com.atlassian.plugin.mock.MockAnimalModuleDescriptor;
import com.atlassian.plugin.mock.MockMineral;
import com.atlassian.plugin.module.ModuleFactory;
import com.atlassian.plugin.util.ClassLoaderUtils;
import com.google.common.collect.ImmutableSet;
import org.dom4j.DocumentException;
import org.dom4j.DocumentHelper;
import org.dom4j.Element;
import org.junit.Test;

import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.concurrent.atomic.AtomicBoolean;

import static java.lang.Boolean.FALSE;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertSame;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class TestAbstractModuleDescriptor {
    public static final String ANNOTATION_PERMISSION = "annotation_permission";

    @Test
    public void testAssertModuleClassImplements() throws DocumentException, PluginParseException {
        ModuleDescriptor descriptor = new AbstractModuleDescriptor(ModuleFactory.LEGACY_MODULE_FACTORY) {
            public void init(Plugin plugin, Element element) throws PluginParseException {
                super.init(plugin, element);
                enabled();
                assertModuleClassImplements(MockMineral.class);
            }

            public Object getModule() {
                return null;
            }
        };

        try {
            descriptor.init(new StaticPlugin(), DocumentHelper.parseText("<animal key=\"key\" name=\"bear\" class=\"com.atlassian.plugin.mock.MockBear\" />").getRootElement());
            ((StateAware) descriptor).enabled();
            fail("Should have blown up.");
        } catch (PluginParseException e) {
            assertEquals("Given module class: com.atlassian.plugin.mock.MockBear does not implement com.atlassian.plugin.mock.MockMineral", e.getMessage());
        }

        // now succeed
        descriptor.init(new StaticPlugin(), DocumentHelper.parseText("<animal key=\"key\" name=\"bear\" class=\"com.atlassian.plugin.mock.MockGold\" />").getRootElement());
    }

    @Test
    public void testLoadClassFromNewModuleFactory() {
        ModuleFactory moduleFactory = mock(ModuleFactory.class);
        AbstractModuleDescriptor moduleDescriptor = new StringModuleDescriptor(moduleFactory, "foo");
        Plugin plugin = mock(Plugin.class);
        moduleDescriptor.loadClass(plugin, "foo");
        assertEquals(String.class, moduleDescriptor.getModuleClass());
    }

    @Test
    public void testLoadClassFromNewModuleFactoryWithExtendsNumberType() {
        ModuleFactory moduleFactory = mock(ModuleFactory.class);
        AbstractModuleDescriptor moduleDescriptor = new ExtendsNumberModuleDescriptor(moduleFactory, "foo");
        Plugin plugin = mock(Plugin.class);

        try {
            moduleDescriptor.loadClass(plugin, "foo");
            fail("Should have complained about extends type");
        } catch (IllegalStateException ex) {
            // success
        }
    }

    @Test
    public void testLoadClassFromNewModuleFactoryWithExtendsNothingType() {
        ModuleFactory moduleFactory = mock(ModuleFactory.class);
        AbstractModuleDescriptor moduleDescriptor = new ExtendsNothingModuleDescriptor(moduleFactory, "foo");
        Plugin plugin = mock(Plugin.class);

        try {
            moduleDescriptor.loadClass(plugin, "foo");
            fail("Should have complained about extends type");
        } catch (IllegalStateException ex) {
            // success
        }
    }

    @Test
    public void testGetModuleReturnClass() {
        AbstractModuleDescriptor desc = new MockAnimalModuleDescriptor();
        assertEquals(MockAnimal.class, desc.getModuleReturnClass());
    }

    @Test
    public void testGetModuleReturnClassWithExtendsNumber() {
        ModuleFactory moduleFactory = mock(ModuleFactory.class);
        AbstractModuleDescriptor moduleDescriptor = new ExtendsNothingModuleDescriptor(moduleFactory, "foo");
        assertEquals(Object.class, moduleDescriptor.getModuleReturnClass());
    }

    @Test
    public void testLoadClassFromNewModuleFactoryButUnknownType() {
        ModuleFactory moduleFactory = mock(ModuleFactory.class);
        AbstractModuleDescriptor moduleDescriptor = new AbstractModuleDescriptor(moduleFactory) {
            public AbstractModuleDescriptor init() {
                moduleClassName = "foo";
                return this;
            }

            @Override
            public Object getModule() {
                return null;
            }
        }.init();
        try {
            Plugin plugin = mock(Plugin.class);
            moduleDescriptor.loadClass(plugin, "foo");
            fail("Should have complained about unknown type");
        } catch (IllegalStateException ex) {
            // success
        }
    }

    @Test
    public void testSingletonness() throws DocumentException, PluginParseException {
        ModuleDescriptor descriptor = makeSingletonDescriptor();

        // try a default descriptor with no singleton="" element. Should _be_ a singleton
        descriptor.init(new StaticPlugin(), DocumentHelper.parseText("<animal key=\"key\" name=\"bear\" class=\"com.atlassian.plugin.mock.MockBear\" />").getRootElement());
        ((StateAware) descriptor).enabled();
        Object module = descriptor.getModule();
        assertTrue(module == descriptor.getModule());

        // now try a default descriptor with singleton="true" element. Should still be a singleton
        descriptor = makeSingletonDescriptor();
        descriptor.init(new StaticPlugin(), DocumentHelper.parseText("<animal key=\"key\" name=\"bear\" class=\"com.atlassian.plugin.mock.MockBear\" singleton=\"true\" />").getRootElement());
        ((StateAware) descriptor).enabled();
        module = descriptor.getModule();
        assertTrue(module == descriptor.getModule());

        // now try reiniting as a non-singleton
        descriptor = makeSingletonDescriptor();
        descriptor.init(new StaticPlugin(), DocumentHelper.parseText("<animal key=\"key\" name=\"bear\" class=\"com.atlassian.plugin.mock.MockBear\" singleton=\"false\" />").getRootElement());
        ((StateAware) descriptor).enabled();
        module = descriptor.getModule();
        assertTrue(module != descriptor.getModule());
    }

    @Test
    public void testGetResourceDescriptor() throws DocumentException, PluginParseException {
        ModuleDescriptor descriptor = makeSingletonDescriptor();
        descriptor.init(new StaticPlugin(), DocumentHelper.parseText("<animal key=\"key\" name=\"bear\" class=\"com.atlassian.plugin.mock.MockBear\">" +
                "<resource type='velocity' name='view' location='foo' />" +
                "</animal>").getRootElement());

        assertNull(descriptor.getResourceLocation("foo", "bar"));
        assertNull(descriptor.getResourceLocation("velocity", "bar"));
        assertNull(descriptor.getResourceLocation("foo", "view"));
        assertEquals(new ResourceDescriptor(DocumentHelper.parseText("<resource type='velocity' name='view' location='foo' />").getRootElement()).getResourceLocationForName("view").getLocation(), descriptor.getResourceLocation("velocity", "view").getLocation());
    }

    @Test
    public void testGetResourceDescriptorByType() throws DocumentException, PluginParseException {
        ModuleDescriptor descriptor = makeSingletonDescriptor();
        descriptor.init(new StaticPlugin(), DocumentHelper.parseText("<animal key=\"key\" name=\"bear\" class=\"com.atlassian.plugin.mock.MockBear\">" +
                "<resource type='velocity' name='view' location='foo' />" +
                "<resource type='velocity' name='input-params' location='bar' />" +
                "</animal>").getRootElement());

        final List resourceDescriptors = descriptor.getResourceDescriptors("velocity");
        assertNotNull(resourceDescriptors);
        assertEquals(2, resourceDescriptors.size());

        ResourceDescriptor resourceDescriptor = (ResourceDescriptor) resourceDescriptors.get(0);
        assertEquals(new ResourceDescriptor(DocumentHelper.parseText("<resource type='velocity' name='view' location='foo' />").getRootElement()), resourceDescriptor);

        resourceDescriptor = (ResourceDescriptor) resourceDescriptors.get(1);
        assertEquals(new ResourceDescriptor(DocumentHelper.parseText("<resource type='velocity' name='input-params' location='bar' />").getRootElement()), resourceDescriptor);
    }

    @Test
    public void testDestroy() {
        AbstractModuleDescriptor descriptor = spy(new StringModuleDescriptor(ModuleFactory.LEGACY_MODULE_FACTORY, null));
        Plugin plugin = mock(Plugin.class);
        descriptor.setPlugin(plugin);
        // enable the module descriptor first; otherwise, disabled() won't be called from destroy()
        descriptor.enabled();

        descriptor.destroy();

        verify(descriptor).disabled();
        assertSame(plugin, descriptor.getPlugin());
    }

    @Test
    public void testDestroyWhenDisabled() {
        AbstractModuleDescriptor descriptor = spy(new StringModuleDescriptor(ModuleFactory.LEGACY_MODULE_FACTORY, null));
        Plugin plugin = mock(Plugin.class);
        descriptor.setPlugin(plugin);

        descriptor.destroy();

        // it's never enabled, so disabled() won't be called
        verify(descriptor, never()).disabled();
        assertSame(plugin, descriptor.getPlugin());
    }

    @Test
    public void testDestroyDelegatesToDeprecatedOverride() {
        final AtomicBoolean called = new AtomicBoolean();

        AbstractModuleDescriptor descriptor = new StringModuleDescriptor(ModuleFactory.LEGACY_MODULE_FACTORY, null) {
            @Override
            @Deprecated
            public void destroy(Plugin plugin) {
                called.set(true);
            }
        };

        Plugin plugin = mock(Plugin.class);
        descriptor.setPlugin(plugin);

        descriptor.destroy();

        assertTrue(called.get());
    }

    @Test
    public void testCheckPermissionsForModuleDescriptorAndPluginDefinedWithNoPermission() {
        final AbstractModuleDescriptor moduleDescriptor = makeSingletonDescriptor();
        moduleDescriptor.setPlugin(mock(Plugin.class));

        try {
            moduleDescriptor.checkPermissions();
        } catch (ModulePermissionException e) {
            fail("Didn't expected the exception here" + e);
        }
    }

    @Test
    public void testCheckPermissionsForModuleDescriptorAndPluginWithSamePermission() {
        final String somePermission = "some_permission";
        final AbstractModuleDescriptor moduleDescriptor = new StringModuleDescriptor(ModuleFactory.LEGACY_MODULE_FACTORY, this.getClass().getName()) {
            @Override
            protected Set<String> getRequiredPermissions() {
                return ImmutableSet.of(somePermission);
            }
        };

        final Plugin plugin = mock(Plugin.class);
        moduleDescriptor.setPlugin(plugin);
        when(plugin.getActivePermissions()).thenReturn(ImmutableSet.of(somePermission, ANNOTATION_PERMISSION));

        try {
            moduleDescriptor.checkPermissions();
        } catch (ModulePermissionException e) {
            fail("Didn't expected the exception here" + e);
        }
    }

    @Test
    public void testCheckPermissionsForModuleDescriptorWithPermissionsAndPluginWithAllPermission() {
        final Set<String> permissions = ImmutableSet.of("some_permission");
        final AbstractModuleDescriptor moduleDescriptor = new StringModuleDescriptor(ModuleFactory.LEGACY_MODULE_FACTORY, this.getClass().getName()) {
            @Override
            protected Set<String> getRequiredPermissions() {
                return permissions;
            }
        };

        final Plugin plugin = mock(Plugin.class);
        moduleDescriptor.setPlugin(plugin);
        when(plugin.hasAllPermissions()).thenReturn(true);
        when(plugin.getActivePermissions()).thenReturn(ImmutableSet.of(Permissions.ALL_PERMISSIONS));

        try {
            moduleDescriptor.checkPermissions();
        } catch (ModulePermissionException e) {
            fail("Didn't expected the exception here" + e);
        }
    }

    @Test
    public void testCheckPermissionsForModuleDescriptorWithPermissionsNotInPluginPermissions() {
        final Set<String> permissions = ImmutableSet.of("some_module_permission", "one_plugin_permission");
        final AbstractModuleDescriptor moduleDescriptor = new StringModuleDescriptor(ModuleFactory.LEGACY_MODULE_FACTORY, this.getClass().getName()) {
            @Override
            protected Set<String> getRequiredPermissions() {
                return permissions;
            }
        };

        final Plugin plugin = mock(Plugin.class);
        moduleDescriptor.setPlugin(plugin);
        ImmutableSet<String> pluginPermissions = ImmutableSet.of("one_plugin_permission", "two_plugin_permission", ANNOTATION_PERMISSION);
        when(plugin.getActivePermissions()).thenReturn(pluginPermissions);

        try {
            moduleDescriptor.checkPermissions();
            fail("We expected some exception here");
        } catch (ModulePermissionException e) {
            assertEquals(ImmutableSet.of("some_module_permission"), e.getPermissions());
        }
    }

    @Test
    public void testCheckPermissionsForModuleDescriptorWithPermissionsAllInPluginPermissions() {
        final Set<String> permissions = ImmutableSet.of("two_plugin_permission", "one_plugin_permission");
        final AbstractModuleDescriptor moduleDescriptor = new StringModuleDescriptor(ModuleFactory.LEGACY_MODULE_FACTORY, this.getClass().getName()) {
            @Override
            protected Set<String> getRequiredPermissions() {
                return permissions;
            }
        };

        final Plugin plugin = mock(Plugin.class);
        moduleDescriptor.setPlugin(plugin);
        ImmutableSet<String> pluginPermissions = ImmutableSet.of("one_plugin_permission", "two_plugin_permission", "three_plugin_permission", ANNOTATION_PERMISSION);
        when(plugin.getActivePermissions()).thenReturn(pluginPermissions);

        moduleDescriptor.checkPermissions();
    }

    @Test
    public void setModuleIsNotBrokenAfterConstruction() {
        final ModuleDescriptor<String> md = newStringModuleDescriptor();
        assertThat(md.isBroken(), is(false));
    }

    @Test
    public void setBrokenMakesModuleBroken() {
        final ModuleDescriptor<String> md = newStringModuleDescriptor();
        md.setBroken();
        assertThat(md.isBroken(), is(true));
    }

    @Test
    public void enableClearesBrokenState() throws Exception {
        final StringModuleDescriptor md = newStringModuleDescriptor();
        md.init(new StaticPlugin(), DocumentHelper.parseText("<animal key=\"key\" name=\"bear\" class=\"com.atlassian.plugin.mock.MockBear\" />").getRootElement());
        md.setBroken();
        md.enabled();
        assertThat(md.isBroken(), is(false));
    }

    @Test
    public void verifyInheritingEmptyScopeFromPlugin() throws Exception {
        StaticPlugin plugin = new StaticPlugin() {
            public Optional<String> getScopeKey() {
                return Optional.empty();
            }
        };
        ModuleDescriptor descriptor = makeSingletonDescriptor();
        descriptor.init(plugin, DocumentHelper.parseText("<animal key=\"key\" name=\"bear\" class=\"com.atlassian.plugin.mock.MockBear\"/>").getRootElement());

        assertThat(descriptor.getScopeKey().isPresent(), equalTo(FALSE));
    }

    @Test
    public void verifyInheritingPresentScopeFromPlugin() throws Exception {
        StaticPlugin plugin = new StaticPlugin() {
            public Optional<String> getScopeKey() {
                return Optional.of("licenses/jira-service-desk");
            }
        };
        ModuleDescriptor descriptor = makeSingletonDescriptor();
        descriptor.init(plugin, DocumentHelper.parseText("<animal key=\"key\" name=\"bear\" class=\"com.atlassian.plugin.mock.MockBear\"/>").getRootElement());

        assertThat(descriptor.getScopeKey().get(), equalTo("licenses/jira-service-desk"));
    }

    @Test
    public void scopeIsEmptyWhenModuleDescopedAndPluginIsScoped() throws Exception {
        StaticPlugin plugin = new StaticPlugin() {
            public Optional<String> getScopeKey() {
                return Optional.of("licenses/jira-service-desk");
            }
        };
        ModuleDescriptor descriptor = makeSingletonDescriptor();
        descriptor.init(plugin, DocumentHelper.parseText("<animal key=\"key\" name=\"bear\" scoped=\"false\" class=\"com.atlassian.plugin.mock.MockBear\"/>").getRootElement());

        assertThat(descriptor.getScopeKey().isPresent(), equalTo(FALSE));
    }

    @Test
    public void scopeIsDerivedFromPluginWhenModuleIsScoped() throws Exception {
        StaticPlugin plugin = new StaticPlugin() {
            public Optional<String> getScopeKey() {
                return Optional.of("licenses/jira-service-desk");
            }
        };
        ModuleDescriptor descriptor = makeSingletonDescriptor();
        descriptor.init(plugin, DocumentHelper.parseText("<animal key=\"key\" name=\"bear\" scoped=\"true\" class=\"com.atlassian.plugin.mock.MockBear\"/>").getRootElement());

        assertThat(descriptor.getScopeKey().get(), equalTo("licenses/jira-service-desk"));
    }

    private StringModuleDescriptor newStringModuleDescriptor() {
        return new StringModuleDescriptor(ModuleFactory.LEGACY_MODULE_FACTORY, this.getClass().getName());
    }

    private AbstractModuleDescriptor makeSingletonDescriptor() {
        AbstractModuleDescriptor descriptor = new AbstractModuleDescriptor(ModuleFactory.LEGACY_MODULE_FACTORY) {
            Object module;

            public void init(Plugin plugin, Element element) throws PluginParseException {
                super.init(plugin, element);
            }

            public Object getModule() {
                try {
                    if (!isSingleton()) {
                        return ClassLoaderUtils.loadClass(getModuleClass().getName(), TestAbstractModuleDescriptor.class).newInstance();
                    } else {
                        if (module == null) {
                            module = ClassLoaderUtils.loadClass(getModuleClass().getName(), TestAbstractModuleDescriptor.class).newInstance();
                        }

                        return module;
                    }
                } catch (Exception e) {
                    throw new RuntimeException("What happened Dave?");
                }
            }
        };
        return descriptor;
    }

    @RequirePermission(ANNOTATION_PERMISSION)
    private static class StringModuleDescriptor extends AbstractModuleDescriptor<String> {
        public StringModuleDescriptor(ModuleFactory moduleFactory, String className) {
            super(moduleFactory);
            moduleClassName = className;
        }

        @Override
        public String getModule() {
            return null;
        }

    }

    private static class ExtendsNumberModuleDescriptor<T extends Number> extends AbstractModuleDescriptor<T> {
        public ExtendsNumberModuleDescriptor(ModuleFactory moduleFactory, String className) {
            super(moduleFactory);
            moduleClassName = className;
        }

        @Override
        public T getModule() {
            return null;
        }
    }

    private static class ExtendsNothingModuleDescriptor<T> extends AbstractModuleDescriptor<T> {
        public ExtendsNothingModuleDescriptor(ModuleFactory moduleFactory, String className) {
            super(moduleFactory);
            moduleClassName = className;
        }

        @Override
        public T getModule() {
            return null;
        }
    }
}
