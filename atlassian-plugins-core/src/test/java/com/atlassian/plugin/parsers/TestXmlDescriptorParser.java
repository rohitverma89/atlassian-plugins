package com.atlassian.plugin.parsers;

import com.atlassian.fugue.Option;
import com.atlassian.fugue.Suppliers;
import com.atlassian.plugin.Application;
import com.atlassian.plugin.InstallationMode;
import com.atlassian.plugin.ModuleDescriptorFactory;
import com.atlassian.plugin.Plugin;
import com.atlassian.plugin.PluginArtifact;
import com.atlassian.plugin.PluginInformation;
import com.atlassian.plugin.PluginParseException;
import com.atlassian.plugin.PluginPermission;
import com.atlassian.plugin.classloader.PluginClassLoader;
import com.atlassian.plugin.impl.DefaultDynamicPlugin;
import com.atlassian.plugin.mock.MockAnimalModuleDescriptor;
import com.atlassian.plugin.util.ClassLoaderUtils;
import com.google.common.base.Function;
import com.google.common.collect.ImmutableSet;
import com.google.common.collect.Iterables;
import com.mockobjects.dynamic.C;
import com.mockobjects.dynamic.Mock;
import junit.framework.TestCase;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.net.URL;
import java.util.Set;

import static com.atlassian.fugue.Option.option;
import static com.atlassian.fugue.Option.some;
import static org.hamcrest.Matchers.empty;
import static org.hamcrest.Matchers.hasItem;
import static org.hamcrest.Matchers.hasItems;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;

@SuppressWarnings({"deprecation"}) //suppress deprecation warnings because we still need to test deprecated methods.
public class TestXmlDescriptorParser extends TestCase {
    private static final String MISSING_INFO_TEST_FILE = "test-missing-plugin-info.xml";
    private static final String DUMMY_PLUGIN_FILE = "pooh-test-plugin.jar";

    public TestXmlDescriptorParser(String name) {
        super(name);
    }

    // CONF-12680 Test for missing plugin-info
    public void testMissingPluginInfo() {
        // mock up some supporting objects
        PluginClassLoader classLoader = new PluginClassLoader(new File(getTestFile("ap-plugins") + "/" + DUMMY_PLUGIN_FILE));
        Mock mockFactory = new Mock(ModuleDescriptorFactory.class);
        mockFactory.expect("getModuleDescriptorClass", "unknown-plugin");

        // create a Plugin for testing
        Plugin testPlugin = new DefaultDynamicPlugin((PluginArtifact) new Mock(PluginArtifact.class).proxy(), classLoader);

        try {
            XmlDescriptorParser parser = new XmlDescriptorParser(new FileInputStream(getTestFile(MISSING_INFO_TEST_FILE)), ImmutableSet.<Application>of());
            parser.configurePlugin((ModuleDescriptorFactory) mockFactory.proxy(), testPlugin);

            PluginInformation info = testPlugin.getPluginInformation();
            assertNotNull("Info should not be null", info);
        } catch (PluginParseException e) {
            e.printStackTrace();
            fail("Plugin information parsing should not fail.");
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            // This shouldn't happen
            fail("Error setting up test");
        }
    }

    public void testPluginsApplicationVersionMinMax() {
        XmlDescriptorParser parser = parse(null,
                "<atlassian-plugin key='foo'>",
                "  <plugin-info>",
                "    <application-version min='3' max='4' />",
                "  </plugin-info>",
                "</atlassian-plugin>");
        assertEquals(3, (int) parser.getPluginInformation().getMinVersion());
        assertEquals(4, (int) parser.getPluginInformation().getMaxVersion());
    }

    public void testPluginsApplicationVersionMinMaxWithOnlyMin() {
        XmlDescriptorParser parser = parse(null,
                "<atlassian-plugin key='foo'>",
                "  <plugin-info>",
                "    <application-version min='3' />",
                "  </plugin-info>",
                "</atlassian-plugin>");
        assertEquals(3, (int) parser.getPluginInformation().getMinVersion());
        assertEquals(0, (int) parser.getPluginInformation().getMaxVersion());
    }

    public void testPluginsApplicationVersionMinMaxWithOnlyMax() {
        XmlDescriptorParser parser = parse(null,
                "<atlassian-plugin key='foo'>",
                "  <plugin-info>",
                "    <application-version max='3' />",
                "  </plugin-info>",
                "</atlassian-plugin>");
        assertEquals(3, (int) parser.getPluginInformation().getMaxVersion());
        assertEquals(0, (int) parser.getPluginInformation().getMinVersion());
    }

    // Also CONF-12680 test for missing "essential metadata"

    public void testPluginsVersion() {
        String xml = "<atlassian-plugin key=\"foo\" pluginsVersion=\"2\" />";
        XmlDescriptorParser parser = new XmlDescriptorParser(new ByteArrayInputStream(xml.getBytes()), ImmutableSet.<Application>of());
        assertEquals(2, parser.getPluginsVersion());
    }

    public void testPluginsVersionAfterConfigure() {
        XmlDescriptorParser parser = new XmlDescriptorParser(new ByteArrayInputStream("<atlassian-plugin key=\"foo\" plugins-version=\"2\" />".getBytes()), ImmutableSet.<Application>of());
        // mock up some supporting objects
        PluginClassLoader classLoader = new PluginClassLoader(new File(getTestFile("ap-plugins") + "/" + DUMMY_PLUGIN_FILE));
        Mock mockFactory = new Mock(ModuleDescriptorFactory.class);
        mockFactory.expect("getModuleDescriptorClass", "unknown-plugin");

        // create a Plugin for testing
        Plugin testPlugin = new DefaultDynamicPlugin((PluginArtifact) new Mock(PluginArtifact.class).proxy(), classLoader);
        parser.configurePlugin((ModuleDescriptorFactory) mockFactory.proxy(), testPlugin);
        assertEquals(2, testPlugin.getPluginsVersion());
    }

    public void testPluginWithModules() {
        XmlDescriptorParser parser = parse(null,
                "<atlassian-plugin key='foo'>",
                "  <animal key='bear' />",
                "</atlassian-plugin>");
        // mock up some supporting objects
        PluginClassLoader classLoader = new PluginClassLoader(new File(getTestFile("ap-plugins") + "/" + DUMMY_PLUGIN_FILE));
        Mock mockFactory = new Mock(ModuleDescriptorFactory.class);
        mockFactory.expectAndReturn("getModuleDescriptorClass", C.args(C.eq("animal")), MockAnimalModuleDescriptor.class);

        // create a Plugin for testing
        Plugin testPlugin = new DefaultDynamicPlugin((PluginArtifact) new Mock(PluginArtifact.class).proxy(), classLoader);
        parser.configurePlugin((ModuleDescriptorFactory) mockFactory.proxy(), testPlugin);
        assertNotNull(testPlugin.getModuleDescriptor("bear"));
    }

    public void testPluginWithModulesNoApplicationKey() {
        XmlDescriptorParser parser = parse(null,
                "<atlassian-plugin key='foo'>",
                "  <animal key='bear' application='foo'/>",
                "</atlassian-plugin>");
        // mock up some supporting objects
        PluginClassLoader classLoader = new PluginClassLoader(new File(getTestFile("ap-plugins") + "/" + DUMMY_PLUGIN_FILE));
        Mock mockFactory = new Mock(ModuleDescriptorFactory.class);
        mockFactory.expectAndReturn("getModuleDescriptorClass", C.args(C.eq("animal")), MockAnimalModuleDescriptor.class);

        // create a Plugin for testing
        Plugin testPlugin = new DefaultDynamicPlugin((PluginArtifact) new Mock(PluginArtifact.class).proxy(), classLoader);
        parser.configurePlugin((ModuleDescriptorFactory) mockFactory.proxy(), testPlugin);
        assertNull(testPlugin.getModuleDescriptor("bear"));
    }

    public void testPluginWithSomeNonApplicationModules() {
        XmlDescriptorParser parser = parse(newApplication("myapp"),
                "<atlassian-plugin key='foo'>",
                "  <animal key='bear' application='myapp'/>",
                "  <animal key='bear2' application='otherapp'/>",
                "</atlassian-plugin>");
        // mock up some supporting objects
        PluginClassLoader classLoader = new PluginClassLoader(new File(getTestFile("ap-plugins") + "/" + DUMMY_PLUGIN_FILE));
        Mock mockFactory = new Mock(ModuleDescriptorFactory.class);
        mockFactory.expectAndReturn("getModuleDescriptorClass", C.args(C.eq("animal")), MockAnimalModuleDescriptor.class);

        // create a Plugin for testing
        Plugin testPlugin = new DefaultDynamicPlugin((PluginArtifact) new Mock(PluginArtifact.class).proxy(), classLoader);
        parser.configurePlugin((ModuleDescriptorFactory) mockFactory.proxy(), testPlugin);
        assertNotNull(testPlugin.getModuleDescriptor("bear"));
        assertNull(testPlugin.getModuleDescriptor("bear2"));
    }

    public void testPluginWithSomeRestrictionOnModulesNoVersion() {
        XmlDescriptorParser parser = parse(newApplication("myapp"),
                "<atlassian-plugin key='foo'>",
                "  <animal key='bear'>",
                "    <restrict application='myapp'/>",
                "  </animal>",
                "  <animal key='bear2' application='otherapp'/>",
                "</atlassian-plugin>");
        // mock up some supporting objects
        PluginClassLoader classLoader = new PluginClassLoader(new File(getTestFile("ap-plugins") + "/" + DUMMY_PLUGIN_FILE));
        Mock mockFactory = new Mock(ModuleDescriptorFactory.class);
        mockFactory.expectAndReturn("getModuleDescriptorClass", C.args(C.eq("animal")), MockAnimalModuleDescriptor.class);

        // create a Plugin for testing
        Plugin testPlugin = new DefaultDynamicPlugin((PluginArtifact) new Mock(PluginArtifact.class).proxy(), classLoader);
        parser.configurePlugin((ModuleDescriptorFactory) mockFactory.proxy(), testPlugin);
        assertNotNull(testPlugin.getModuleDescriptor("bear"));
        assertNull(testPlugin.getModuleDescriptor("bear2"));
    }

    public void testPluginWithSomeRestrictionOnModulesVersionAsAttribute() {
        XmlDescriptorParser parser = parse(newApplication("myapp", "2.0"),
                "<atlassian-plugin key='foo'>",
                "  <animal key='bear'>",
                "    <restrict application='myapp' version='[2.0]' />",
                "  </animal>",
                "  <animal key='bear2'>",
                "    <restrict application='myapp' version='[1.0]' />",
                "  </animal>",
                "</atlassian-plugin>");
        // mock up some supporting objects
        PluginClassLoader classLoader = new PluginClassLoader(new File(getTestFile("ap-plugins") + "/" + DUMMY_PLUGIN_FILE));
        Mock mockFactory = new Mock(ModuleDescriptorFactory.class);
        mockFactory.matchAndReturn("getModuleDescriptorClass", C.args(C.eq("animal")), MockAnimalModuleDescriptor.class);

        // create a Plugin for testing
        Plugin testPlugin = new DefaultDynamicPlugin((PluginArtifact) new Mock(PluginArtifact.class).proxy(), classLoader);
        parser.configurePlugin((ModuleDescriptorFactory) mockFactory.proxy(), testPlugin);
        assertNotNull(testPlugin.getModuleDescriptor("bear"));
        assertNull(testPlugin.getModuleDescriptor("bear2"));
    }

    public void testPluginWithSomeRestrictionOnModulesVersionAsElement() {
        XmlDescriptorParser parser = parse(newApplication("myapp", "2.0"),
                "<atlassian-plugin key='foo'>",
                "  <animal key='bear'>",
                "    <restrict application='myapp'>",
                "      <version>(,3.0)</version>",
                "      <version>[5.0,)</version>",
                "    </restrict>",
                "  </animal>",
                "  <animal key='bear2'>",
                "    <restrict application='myapp'>",
                "      <version>(,2.0)</version>",
                "      <version>[5.0,)</version>",
                "    </restrict>",
                "  </animal>",
                "</atlassian-plugin>");
        // mock up some supporting objects
        PluginClassLoader classLoader = new PluginClassLoader(new File(getTestFile("ap-plugins") + "/" + DUMMY_PLUGIN_FILE));
        Mock mockFactory = new Mock(ModuleDescriptorFactory.class);
        mockFactory.matchAndReturn("getModuleDescriptorClass", C.args(C.eq("animal")), MockAnimalModuleDescriptor.class);

        // create a Plugin for testing
        Plugin testPlugin = new DefaultDynamicPlugin((PluginArtifact) new Mock(PluginArtifact.class).proxy(), classLoader);
        parser.configurePlugin((ModuleDescriptorFactory) mockFactory.proxy(), testPlugin);
        assertNotNull(testPlugin.getModuleDescriptor("bear"));
        assertNull(testPlugin.getModuleDescriptor("bear2"));
    }

    public void testPluginWithSystemAttribute() {
        XmlDescriptorParser parser = parse(null,
                "<atlassian-plugin key='foo' system='true'>",
                "</atlassian-plugin>");

        // mock up some supporting objects
        PluginClassLoader classLoader = new PluginClassLoader(new File(getTestFile("ap-plugins") + "/" + DUMMY_PLUGIN_FILE));
        Mock mockFactory = new Mock(ModuleDescriptorFactory.class);
        mockFactory.expectAndReturn("getModuleDescriptorClass", C.args(C.eq("animal")), MockAnimalModuleDescriptor.class);

        // create a Plugin for testing
        Plugin testPlugin = new DefaultDynamicPlugin((PluginArtifact) new Mock(PluginArtifact.class).proxy(), classLoader);
        parser.configurePlugin((ModuleDescriptorFactory) mockFactory.proxy(), testPlugin);
        // PLUG-415 Plugins2 plugins now need to be able to be declared as system.
        assertEquals("This plugin should be a system plugin - bundled plugins2 plugins are system plugins.", true, testPlugin.isSystemPlugin());
    }

    public void testPluginWithoutSystemAttribute() {
        XmlDescriptorParser parser = parse(null,
                "<atlassian-plugin key='foo' >",
                "</atlassian-plugin>");

        // mock up some supporting objects
        PluginClassLoader classLoader = new PluginClassLoader(new File(getTestFile("ap-plugins") + "/" + DUMMY_PLUGIN_FILE));
        Mock mockFactory = new Mock(ModuleDescriptorFactory.class);
        mockFactory.expectAndReturn("getModuleDescriptorClass", C.args(C.eq("animal")), MockAnimalModuleDescriptor.class);

        // create a Plugin for testing
        Plugin testPlugin = new DefaultDynamicPlugin((PluginArtifact) new Mock(PluginArtifact.class).proxy(), classLoader);
        parser.configurePlugin((ModuleDescriptorFactory) mockFactory.proxy(), testPlugin);
        assertEquals("This plugin should not be a system plugin.", false, testPlugin.isSystemPlugin());
    }

    public void testPluginsVersionWithDash() {
        String xml = "<atlassian-plugin key=\"foo\" plugins-version=\"2\" />";
        XmlDescriptorParser parser = new XmlDescriptorParser(new ByteArrayInputStream(xml.getBytes()), ImmutableSet.<Application>of());
        assertEquals(2, parser.getPluginsVersion());
    }

    public void testPluginsVersionMissing() {
        String xml = "<atlassian-plugin key=\"foo\" />";
        XmlDescriptorParser parser = new XmlDescriptorParser(new ByteArrayInputStream(xml.getBytes()), ImmutableSet.<Application>of());
        assertEquals(1, parser.getPluginsVersion());
    }

    public void testPluginsResourcesAvailableToModuleDescriptors() {
        XmlDescriptorParser parser = parse(null,
                "<atlassian-plugin key='foo'>",
                "  <resource type='velocity' name='edit'>Show an input box here.</resource>",
                "  <animal key='bear' />",
                "</atlassian-plugin>");
        // mock up some supporting objects
        PluginClassLoader classLoader = new PluginClassLoader(new File(getTestFile("ap-plugins") + "/" + DUMMY_PLUGIN_FILE));
        Mock mockFactory = new Mock(ModuleDescriptorFactory.class);
        MockAnimalModuleDescriptor descriptor = new MockAnimalModuleDescriptor("velocity", "edit");
        mockFactory.expectAndReturn("getModuleDescriptor", C.args(C.eq("animal")), descriptor);

        // create a Plugin for testing
        Plugin testPlugin = new DefaultDynamicPlugin((PluginArtifact) new Mock(PluginArtifact.class).proxy(), classLoader);
        parser.configurePlugin((ModuleDescriptorFactory) mockFactory.proxy(), testPlugin);
        assertNotNull(testPlugin.getModuleDescriptor("bear"));

        mockFactory.verify();
    }

    public void testPluginPermissionsIsAllPermissionsForPluginsWithNoVersionByDefault() {
        XmlDescriptorParser parser = parse(null,
                "<atlassian-plugin key='foo'>",
                "  <plugin-info>",
                "  </plugin-info>",
                "</atlassian-plugin>");

        final Set<PluginPermission> parsedPermissions = parser.getPluginInformation().getPermissions();
        assertEquals(1, parsedPermissions.size());
        assertSame(PluginPermission.ALL, Iterables.get(parsedPermissions, 0));
    }

    public void testPluginPermissionsIsAllPermissionsForPlugins1ByDefault() {
        XmlDescriptorParser parser = parse(null,
                "<atlassian-plugin key='foo' plugins-version='1'>",
                "  <plugin-info>",
                "  </plugin-info>",
                "</atlassian-plugin>");

        final Set<PluginPermission> parsedPermissions = parser.getPluginInformation().getPermissions();
        assertEquals(1, parsedPermissions.size());
        assertSame(PluginPermission.ALL, Iterables.get(parsedPermissions, 0));
    }

    public void testPluginPermissionsIsAllPermissionsForPlugins2ByDefault() {
        XmlDescriptorParser parser = parse(null,
                "<atlassian-plugin key='foo' plugins-version='2'>",
                "  <plugin-info>",
                "  </plugin-info>",
                "</atlassian-plugin>");

        final Set<PluginPermission> parsedPermissions = parser.getPluginInformation().getPermissions();
        assertEquals(1, parsedPermissions.size());
        assertSame(PluginPermission.ALL, Iterables.get(parsedPermissions, 0));
    }

    public void testPluginPermissionsIsFilteredByApplications() {
        XmlDescriptorParser parser = parse(newApplication("my-app"),
                "<atlassian-plugin key='foo' plugins-version='2'>",
                "  <plugin-info>",
                "    <permissions>",
                "      <permission application='my-other-app'>some_permission</permission>",
                "      <permission application='my-app'>some_other_permission</permission>",
                "      <permission application='my-other-app' installation-mode='local'>yet_another_permission</permission>",
                "    </permissions>",
                "  </plugin-info>",
                "</atlassian-plugin>");

        final Set<PluginPermission> parsedPermissions = parser.getPluginInformation().getPermissions();
        assertEquals(1, parsedPermissions.size());
        assertEquals("some_other_permission", Iterables.get(parsedPermissions, 0).getName());
    }

    public void testPluginPermissions() {
        XmlDescriptorParser parser = parse(newApplication("my-other-app"),
                "<atlassian-plugin key='foo' plugins-version='2'>",
                "  <plugin-info>",
                "    <permissions>",
                "      <permission>some_permission</permission>",
                "      <permission application='my-app'>some_other_permission</permission>",
                "      <permission installation-mode='local'>yet_another_permission</permission>",
                "    </permissions>",
                "  </plugin-info>",
                "</atlassian-plugin>");

        final Set<PluginPermission> parsedPermissions = parser.getPluginInformation().getPermissions();
        assertEquals(2, parsedPermissions.size());

        final PluginPermission firstPermission = Iterables.get(parsedPermissions, 0);
        assertEquals("some_permission", firstPermission.getName());
        assertEquals(Option.<InstallationMode>none(), firstPermission.getInstallationMode());

        final PluginPermission secondPermission = Iterables.get(parsedPermissions, 1);
        assertEquals("yet_another_permission", secondPermission.getName());
        assertEquals(some(InstallationMode.LOCAL), secondPermission.getInstallationMode());
    }

    public void testPluginPermissionsIsEmptyForRemotablePluginByDefault() {
        XmlDescriptorParser parser = parse(null,
                "<atlassian-plugin key='foo' plugins-version='3'>",
                "  <plugin-info>",
                "  </plugin-info>",
                "</atlassian-plugin>");

        assertTrue(parser.getPluginInformation().getPermissions().isEmpty());
    }

    public void testPluginPermissionsIsEmptyForBlankPermissions() {
        XmlDescriptorParser parser = parse(null,
                "<atlassian-plugin key='foo' plugins-version='3'>",
                "  <plugin-info>",
                "    <permissions>",
                "      <permission> \t </permission>",
                "      <permission></permission>",
                "    </permissions>",
                "  </plugin-info>",
                "</atlassian-plugin>");

        assertTrue(parser.getPluginInformation().getPermissions().isEmpty());
    }

    public void testStartupIsParsed() {
        final XmlDescriptorParser parser = parse(null,
                "<atlassian-plugin>",
                "  <plugin-info>",
                "    <startup>early</startup>",
                "  </plugin-info>",
                "</atlassian-plugin>");

        assertEquals(parser.getPluginInformation().getStartup(), "early");
    }

    public void testNoScanFolderWhenNoScanModulesElementIsProvided() {
        final XmlDescriptorParser parser = parse(null,
                "<atlassian-plugin>",
                "  <plugin-info>",
                "  </plugin-info>",
                "</atlassian-plugin>");

        assertThat(parser.getPluginInformation().getModuleScanFolders(), is(empty()));
    }

    public void testDefaultScanFolderWhenNoFolderProvided() {
        final XmlDescriptorParser parser = parse(null,
                "<atlassian-plugin>",
                "  <plugin-info>",
                "    <scan-modules/>",
                "  </plugin-info>",
                "</atlassian-plugin>");

        assertThat(parser.getPluginInformation().getModuleScanFolders(), hasItem("META-INF/atlassian"));
    }

    public void testDefaultScanFolderWhenFoldersAreProvided() {
        final XmlDescriptorParser parser = parse(null,
                "<atlassian-plugin>",
                "  <plugin-info>",
                "    <scan-modules>",
                "      <folder>folder1</folder>",
                "      <folder>folder2</folder>",
                "    </scan-modules>",
                "  </plugin-info>",
                "</atlassian-plugin>");

        assertThat(parser.getPluginInformation().getModuleScanFolders(), hasItems("folder1", "folder2"));
    }


    private String getTestFile(String filename) {
        final URL url = ClassLoaderUtils.getResource(filename, this.getClass());
        return url.getFile();
    }

    private static XmlDescriptorParser parse(Application application, String... lines) {
        StringBuffer sb = new StringBuffer();
        for (String line : lines) {
            sb.append(line.replace('\'', '"')).append('\n');
        }
        InputStream in = new ByteArrayInputStream(sb.toString().getBytes());
        return new XmlDescriptorParser(in, option(application).fold(
                Suppliers.ofInstance(ImmutableSet.<Application>of()),
                new Function<Application, Set<Application>>() {
                    @Override
                    public Set<Application> apply(Application app) {
                        return ImmutableSet.of(app);
                    }
                }));
    }

    private Application newApplication(final String appKey) {
        return newApplication(appKey, null);
    }

    private Application newApplication(final String appKey, final String version) {
        return new Application() {
            @Override
            public String getKey() {
                return appKey;
            }

            @Override
            public String getVersion() {
                return version;
            }

            @Override
            public String getBuildNumber() {
                return null;
            }
        };
    }
}
