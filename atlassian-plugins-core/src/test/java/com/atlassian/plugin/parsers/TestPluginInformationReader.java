package com.atlassian.plugin.parsers;

import com.atlassian.fugue.Option;
import com.atlassian.plugin.Application;
import com.atlassian.plugin.Plugin;
import org.dom4j.Element;
import org.junit.Test;

import java.io.ByteArrayInputStream;
import java.util.Collections;
import java.util.Optional;

import static com.atlassian.fugue.Option.none;
import static com.atlassian.fugue.Option.option;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.contains;
import static org.hamcrest.Matchers.emptyIterable;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;

public class TestPluginInformationReader {
    @Test
    public void getStartupReturnsNoneWhenAbsent() {
        final String xml = "<plugin-info></plugin-info>";
        PluginInformationReader reader = getPluginInformationReader(xml);
        assertThat(reader.getStartup(), is(none(String.class)));
    }

    @Test
    public void getStartupReturnsTrimmedTextWhenPresent() {
        final String xml = "<plugin-info><startup> early  </startup></plugin-info>";
        PluginInformationReader reader = getPluginInformationReader(xml);
        assertThat(reader.getStartup(), is(option("early")));
    }

    @Test
    public void getScanModulesReturnsEmptyIteratorWhenAbsent() {
        final String xml = "<plugin-info></plugin-info>";
        PluginInformationReader reader = getPluginInformationReader(xml);
        assertThat(reader.getModuleScanFolders(), emptyIterable());
    }

    @Test
    public void getScanModulesReturnsDefaultWhenPresent() {
        final String xml = "<plugin-info><scan-modules/></plugin-info>";
        PluginInformationReader reader = getPluginInformationReader(xml);
        assertThat(reader.getModuleScanFolders(), contains("META-INF/atlassian"));
    }

    @Test
    public void getScanModulesReturnsFoldersWhenPresent() {
        final String xml = "<plugin-info><scan-modules><folder>META-INF/foo</folder><folder>META-INF/foo/bar</folder></scan-modules></plugin-info>";
        PluginInformationReader reader = getPluginInformationReader(xml);
        assertThat(reader.getModuleScanFolders(), contains("META-INF/foo", "META-INF/foo/bar"));
    }

    @Test
    public void getDefaultFolderCanBeSpecifiedInScanFolders() {
        final String xml = "<plugin-info><scan-modules><folder>META-INF/atlassian</folder><folder>META-INF/foo/bar</folder></scan-modules></plugin-info>";
        PluginInformationReader reader = getPluginInformationReader(xml);
        assertThat(reader.getModuleScanFolders(), contains("META-INF/atlassian", "META-INF/foo/bar"));
    }

    @Test
    public void getScopeKeyWhenMissing() {
        final String xml = "<plugin-info></plugin-info>";
        PluginInformationReader reader = getPluginInformationReader(xml);
        assertThat(reader.getScopeKey(), equalTo(Optional.empty()));
    }

    @Test
    public void getScopeKeyWhenPresent() {
        final String xml = "<plugin-info><scope key = \"/license/jira-service-desk\"/></plugin-info>";
        PluginInformationReader reader = getPluginInformationReader(xml);
        assertThat(reader.getScopeKey(), equalTo(Optional.of("/license/jira-service-desk")));
    }

    @Test(expected = IllegalArgumentException.class)
    public void getBlankScopeKey() {
        getPluginInformationReader("<plugin-info><scope key = \"\"/></plugin-info>").getScopeKey();
    }

    private PluginInformationReader getPluginInformationReader(final String xml) {
        final ByteArrayInputStream inputStream = new ByteArrayInputStream(xml.getBytes());
        final Element element = XmlDescriptorParser.createDocument(inputStream).getRootElement();
        return new PluginInformationReader(option(element), Collections.<Application>emptySet(), Plugin.VERSION_3);
    }
}
