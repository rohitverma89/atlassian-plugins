package com.atlassian.plugin.parsers;

import com.google.common.base.Joiner;
import com.google.common.collect.ImmutableMap;
import org.dom4j.Document;
import org.dom4j.DocumentHelper;
import org.dom4j.Node;
import org.dom4j.XPath;
import org.junit.Test;

import java.io.ByteArrayInputStream;
import java.io.UnsupportedEncodingException;

import static com.atlassian.plugin.parsers.XmlDescriptorParserUtils.removeAllNamespaces;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

public final class XmlDescriptorParserUtilsTest {
    @Test
    public void testRemoveAllNamespaces() throws Exception {
        final String xml = Joiner.on('\n').join(
                "<document xmlns='http://www.example.com/schema/main'",
                "          xmlns:xsi='http://www.w3.org/2001/XMLSchema-instance'",
                "          xmlns:n1='http://www.example.com/schema/n1'>",
                "    <element1><sub-element1>Text 1</sub-element1></element1>",
                "    <n1:element2><n1:sub-element2>Text 2</n1:sub-element2></n1:element2>",
                "</document>");


        final Document docs = getDocument(xml);

        XPath xPath = createXPathWithNamespace("/main:document/main:element1/main:sub-element1");
        Node el = xPath.selectSingleNode(docs);
        assertNotNull(el);
        assertEquals("Text 1", el.getText());

        xPath = DocumentHelper.createXPath("/document/element1/sub-element1");
        assertNull(xPath.selectSingleNode(docs));

        xPath = createXPathWithNamespace("/main:document/n1:element2/n1:sub-element2");
        el = xPath.selectSingleNode(docs);
        assertNotNull(el);
        assertEquals("Text 2", el.getText());

        xPath = DocumentHelper.createXPath("/document/element2/sub-element2");
        assertNull(xPath.selectSingleNode(docs));

        removeAllNamespaces(docs);

        xPath = createXPathWithNamespace("/main:document/main:element1/main:sub-element1");
        assertNull(xPath.selectSingleNode(docs));

        xPath = DocumentHelper.createXPath("/document/element1/sub-element1");
        el = xPath.selectSingleNode(docs);
        assertNotNull(el);
        assertEquals("Text 1", el.getText());

        xPath = createXPathWithNamespace("/main:document/n1:element2/n1:sub-element2");
        assertNull(xPath.selectSingleNode(docs));

        xPath = DocumentHelper.createXPath("/document/element2/sub-element2");
        el = xPath.selectSingleNode(docs);
        assertNotNull(el);
        assertEquals("Text 2", el.getText());
    }

    private XPath createXPathWithNamespace(String xpathExpression) {
        final XPath xPath = DocumentHelper.createXPath(xpathExpression);
        xPath.setNamespaceURIs(ImmutableMap.of(
                "main", "http://www.example.com/schema/main",
                "n1", "http://www.example.com/schema/n1"));
        return xPath;
    }

    private Document getDocument(String xml) throws UnsupportedEncodingException {
        return XmlDescriptorParser.createDocument(new ByteArrayInputStream(xml.getBytes("UTF-8")));
    }
}
