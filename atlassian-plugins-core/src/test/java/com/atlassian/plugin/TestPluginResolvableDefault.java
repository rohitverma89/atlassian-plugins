package com.atlassian.plugin;

import org.junit.Test;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;

public class TestPluginResolvableDefault {
    @Test
    public void resolveIsForwardedForPlugin() {
        final Plugin plugin = mock(Plugin.class);
        plugin.resolve();
        verify(plugin).resolve();
    }
}
