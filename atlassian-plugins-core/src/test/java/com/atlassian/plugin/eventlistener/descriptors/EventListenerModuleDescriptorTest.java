package com.atlassian.plugin.eventlistener.descriptors;


import com.atlassian.event.api.EventPublisher;
import com.atlassian.plugin.ModuleDescriptor;
import com.atlassian.plugin.module.ModuleFactory;
import org.junit.Rule;
import org.junit.Test;
import org.junit.contrib.java.lang.system.RestoreSystemProperties;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static com.atlassian.plugin.eventlistener.descriptors.EventListenerModuleDescriptor.FALLBACK_MODE;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class EventListenerModuleDescriptorTest {

    @Mock
    ModuleFactory factory;

    @Mock
    EventPublisher publisher;

    @Mock
    Object module;

    @Rule
    public RestoreSystemProperties restoreSystemProperties = new RestoreSystemProperties(FALLBACK_MODE);

    @Test
    public void enabled() throws Exception {
        final EventListenerModuleDescriptor descriptor = new EventListenerModuleDescriptor(factory, publisher);
        descriptor.enabled();

        verify(publisher).register(descriptor);
    }

    @Test
    public void disabled() throws Exception {
        final EventListenerModuleDescriptor descriptor = new EventListenerModuleDescriptor(factory, publisher);
        descriptor.disabled();

        verify(publisher).unregister(descriptor);
    }

    @Test
    public void enabledInFallbackMode() throws Exception {
        System.setProperty(FALLBACK_MODE, "true");
        when(factory.createModule(anyString(), any(ModuleDescriptor.class))).thenReturn(module);

        final EventListenerModuleDescriptor descriptor = new EventListenerModuleDescriptor(factory, publisher);
        descriptor.enabled();

        verify(publisher).register(module);
    }

    @Test
    public void disabledInFallbackMode() throws Exception {
        System.setProperty(FALLBACK_MODE, "true");

        when(factory.createModule(anyString(), any(ModuleDescriptor.class))).thenReturn(module);

        final EventListenerModuleDescriptor descriptor = new EventListenerModuleDescriptor(factory, publisher);
        descriptor.disabled();

        verify(publisher).unregister(module);
    }
}