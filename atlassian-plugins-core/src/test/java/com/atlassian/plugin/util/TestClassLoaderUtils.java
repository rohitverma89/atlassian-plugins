package com.atlassian.plugin.util;

import org.junit.Test;

import java.util.concurrent.Callable;

import static org.junit.Assert.assertEquals;

@SuppressWarnings("AssertEqualsBetweenInconvertibleTypes")
public class TestClassLoaderUtils {
    @Test
    public void testLoadClassWithContextClassLoader() throws Exception {
        withContextClassLoaderTest(Thread.currentThread().getContextClassLoader(), new Callable<Void>() {
            @Override
            public Void call() throws Exception {
                assertEquals(TestClassLoaderUtils.class, ClassLoaderUtils.loadClass(TestClassLoaderUtils.class.getName(), this.getClass()));
                return null;
            }
        });
    }

    @Test(expected = ClassNotFoundException.class)
    public void testLoadNoSuchClassWithContextClassLoader() throws Exception {
        withContextClassLoaderTest(Thread.currentThread().getContextClassLoader(), new Callable<Void>() {
            @Override
            public Void call() throws Exception {
                ClassLoaderUtils.loadClass("some.class", null);
                return null;
            }
        });
    }

    @Test
    public void testLoadClassWithNullContextClassLoader() throws Exception {
        withContextClassLoaderTest(null, new Callable<Void>() {
            @Override
            public Void call() throws Exception {
                assertEquals(TestClassLoaderUtils.class, ClassLoaderUtils.loadClass(TestClassLoaderUtils.class.getName(), this.getClass()));
                return null;
            }
        });
    }

    @Test(expected = ClassNotFoundException.class)
    public void testLoadNoSuchClassWithNullContextClassLoader() throws Exception {
        withContextClassLoaderTest(null, new Callable<Void>() {
            @Override
            public Void call() throws Exception {
                ClassLoaderUtils.loadClass("some.class", null);
                return null;
            }
        });
    }

    private void withContextClassLoaderTest(ClassLoader contextClassLoader, Callable<Void> test) throws Exception {
        ClassLoader currentClassLoader = Thread.currentThread().getContextClassLoader();
        Thread.currentThread().setContextClassLoader(contextClassLoader);
        try {
            test.call();
        } finally {
            Thread.currentThread().setContextClassLoader(currentClassLoader);
        }
    }
}
