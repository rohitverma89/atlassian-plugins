package com.atlassian.plugin.util.resource;

import com.atlassian.plugin.test.CapturedLogging;
import org.apache.commons.io.IOUtils;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.contrib.java.lang.system.RestoreSystemProperties;
import org.junit.rules.TemporaryFolder;

import java.io.File;
import java.io.InputStream;

import static com.atlassian.plugin.test.CapturedLogging.didLogDebug;
import static com.atlassian.plugin.test.CapturedLogging.didLogWarn;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.contains;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.not;
import static org.hamcrest.Matchers.notNullValue;
import static org.hamcrest.Matchers.nullValue;

public class TestAlternativeDirectoryResourceLoader {
    @Rule
    public RestoreSystemProperties restoreSystemProperties = new RestoreSystemProperties(
            AlternativeDirectoryResourceLoader.PLUGIN_RESOURCE_DIRECTORIES);

    @Rule
    public TemporaryFolder temporaryFolder = new TemporaryFolder(new File("target"));

    @Rule
    public CapturedLogging capturedLogging = new CapturedLogging(AlternativeDirectoryResourceLoader.class);

    private File base;
    private File classes;
    private File com;

    @Before
    public void setUp() throws Exception {
        base = temporaryFolder.getRoot();
        com = temporaryFolder.newFolder("classes", "com");
        classes = com.getParentFile();
        temporaryFolder.newFile("kid.txt");
    }

    @Test
    public void testGetResource() throws Exception {
        System.setProperty(AlternativeDirectoryResourceLoader.PLUGIN_RESOURCE_DIRECTORIES, base.getAbsolutePath());
        final AlternativeResourceLoader loader = new AlternativeDirectoryResourceLoader();
        assertThat(loader.getResource("classes"), equalTo(classes.toURL()));
        assertThat(loader.getResource("com"), nullValue());
        assertThat(loader.getResource("asdfasdfasf"), nullValue());
    }

    @Test
    public void testGetResourceWithTwoDefined() throws Exception {
        System.setProperty(AlternativeDirectoryResourceLoader.PLUGIN_RESOURCE_DIRECTORIES,
                base.getAbsolutePath() + "," + classes.getAbsolutePath());
        final AlternativeResourceLoader loader = new AlternativeDirectoryResourceLoader();
        assertThat(loader.getResource("classes"), equalTo(classes.toURL()));
        assertThat(loader.getResource("com"), equalTo(com.toURL()));
        assertThat(loader.getResource("asdfasdfasf"), nullValue());
    }

    @Test
    public void testChangesInSystemPropertyAreDynamic() throws Exception {
        //
        // PLUG-1188 - didn't do dynamic setting of alternate resource directories
        //
        // with no property its zero sized
        final AlternativeDirectoryResourceLoader loader = new AlternativeDirectoryResourceLoader();
        assertThat(loader.getResourceDirectories().size(),equalTo(0));

        // just one directory to start with
        System.setProperty(AlternativeDirectoryResourceLoader.PLUGIN_RESOURCE_DIRECTORIES, base.getAbsolutePath());
        assertThat(loader.getResourceDirectories().size(),equalTo(1));

        // add another one and see how it detects that
        System.setProperty(AlternativeDirectoryResourceLoader.PLUGIN_RESOURCE_DIRECTORIES,
                base.getAbsolutePath() + "," + classes.getAbsolutePath());
        assertThat(loader.getResourceDirectories().size(),equalTo(2));

        // and if we set the same value it stays stable
        System.setProperty(AlternativeDirectoryResourceLoader.PLUGIN_RESOURCE_DIRECTORIES,
                base.getAbsolutePath() + "," + classes.getAbsolutePath());
        assertThat(loader.getResourceDirectories().size(),equalTo(2));
    }


    @Test
    public void testGetResourceWithWhitespace() throws Exception {
        System.setProperty(AlternativeDirectoryResourceLoader.PLUGIN_RESOURCE_DIRECTORIES,
                "\n" +
                        "         " + base.getAbsolutePath() + ",\n" +
                        "         " + classes.getAbsolutePath() + "\n" +
                        "         ");
        final AlternativeResourceLoader loader = new AlternativeDirectoryResourceLoader();
        assertThat(loader.getResource("classes"), equalTo(classes.toURL()));
        assertThat(loader.getResource("com"), equalTo(com.toURL()));
        assertThat(loader.getResource("asdfasdfasf"), nullValue());
    }

    @Test
    public void testGetResourceNoProperty() throws Exception {
        final AlternativeResourceLoader loader = new AlternativeDirectoryResourceLoader();
        assertThat(loader.getResource("classes"), nullValue());
        assertThat(loader.getResource("asdfasdfasf"), nullValue());
    }

    @Test
    public void testGetResourceAsStream() throws Exception {
        System.setProperty(AlternativeDirectoryResourceLoader.PLUGIN_RESOURCE_DIRECTORIES, base.getAbsolutePath());
        final AlternativeResourceLoader loader = new AlternativeDirectoryResourceLoader();
        InputStream kidInputStream = null;
        InputStream junkInputStream = null;
        try {
            kidInputStream = loader.getResourceAsStream("kid.txt");
            junkInputStream = loader.getResourceAsStream("asdfasdfasf");
            assertThat(kidInputStream, notNullValue());
            assertThat(junkInputStream, nullValue());
        } finally {
            IOUtils.closeQuietly(kidInputStream);
            IOUtils.closeQuietly(junkInputStream);
        }
    }

    @Test
    public void constructionLogsFoundAsDebugAndNotFoundAsWarning() {
        final File classes = new File(base, "classes");
        final File nonexistent = new File(base, "non-existent");
        System.setProperty(AlternativeDirectoryResourceLoader.PLUGIN_RESOURCE_DIRECTORIES, nonexistent + "," + classes);
        final AlternativeDirectoryResourceLoader loader = new AlternativeDirectoryResourceLoader();
        assertThat(loader.getResourceDirectories(), contains(classes));
        assertThat(capturedLogging, didLogDebug(classes.getPath(), "Found"));
        assertThat(capturedLogging, didLogWarn(nonexistent.getPath(), nonexistent.getAbsolutePath(), "does not exist"));
    }

    @Test
    public void emptyNamePluginResourceDirectoriesDoNotLogWarnings() {
        final File classes = new File(base, "classes");
        System.setProperty(AlternativeDirectoryResourceLoader.PLUGIN_RESOURCE_DIRECTORIES, "," + classes);
        final AlternativeDirectoryResourceLoader loader = new AlternativeDirectoryResourceLoader();
        assertThat(loader.getResourceDirectories(), contains(classes));
        // We should get no warnings at all
        assertThat(capturedLogging, not(didLogWarn()));
    }
}
