package com.atlassian.plugin.loaders;

import com.atlassian.plugin.DefaultModuleDescriptorFactory;
import com.atlassian.plugin.ModuleDescriptor;
import com.atlassian.plugin.ModuleDescriptorFactory;
import com.atlassian.plugin.Plugin;
import com.atlassian.plugin.PluginParseException;
import com.atlassian.plugin.hostcontainer.DefaultHostContainer;
import com.atlassian.plugin.mock.MockAnimalModuleDescriptor;
import com.atlassian.plugin.mock.MockMineralModuleDescriptor;
import com.atlassian.plugin.util.ClassLoaderUtils;
import com.google.common.collect.ImmutableList;
import org.dom4j.Element;
import org.hamcrest.Matchers;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.not;
import static org.hamcrest.Matchers.nullValue;
import static org.hamcrest.Matchers.sameInstance;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class TestSinglePluginLoader {
    @Rule
    public ExpectedException expectedException = ExpectedException.none();

    private SinglePluginLoader loader;

    @Mock
    private Plugin plugin;
    @Mock
    private Element module;
    @Mock
    private ModuleDescriptorFactory moduleDescriptorFactory;
    @Mock
    private ModuleDescriptor moduleDescriptor;

    @Before
    public void before() throws Exception {
        loader = new SinglePluginLoader(ClassLoaderUtils.getResource("test-disabled-plugin.xml", SinglePluginLoader.class));
    }

    @Test
    public void pluginByUrl() throws PluginParseException {
        // URL created should be reentrant and create a different stream each
        // time
        assertThat(loader.getSource(), not(sameInstance(loader.getSource())));
        final DefaultModuleDescriptorFactory moduleDescriptorFactory = new DefaultModuleDescriptorFactory(new DefaultHostContainer());
        moduleDescriptorFactory.addModuleDescriptor("animal", MockAnimalModuleDescriptor.class);
        moduleDescriptorFactory.addModuleDescriptor("mineral", MockMineralModuleDescriptor.class);
        final Iterable<Plugin> plugins = loader.loadAllPlugins(moduleDescriptorFactory);
        assertThat(plugins, Matchers.<Plugin>iterableWithSize(1));
        assertThat(plugins.iterator().next().isEnabledByDefault(), is(false));
    }

    /**
     * @deprecated testing deprecated behaviour
     */
    @Test
    @Deprecated
    public void pluginByInputStreamNotReentrant() throws PluginParseException {
        final SinglePluginLoader loader = new SinglePluginLoader(ClassLoaderUtils.getResourceAsStream("test-disabled-plugin.xml", SinglePluginLoader.class));
        loader.getSource();

        expectedException.expect(IllegalStateException.class);

        loader.getSource();
    }

    @Test
    public void createModule() throws IllegalAccessException, ClassNotFoundException, InstantiationException {
        loader.plugins = ImmutableList.of(plugin);

        when(moduleDescriptorFactory.getModuleDescriptor(anyString())).thenReturn(moduleDescriptor);

        assertThat(loader.createModule(plugin, module, moduleDescriptorFactory), is(moduleDescriptor));

        verify(moduleDescriptor).init(plugin, module);
    }

    @Test
    public void createModuleNoFactory() {
        loader.plugins = ImmutableList.of();

        assertThat(loader.createModule(plugin, module, moduleDescriptorFactory), nullValue());
    }
}
