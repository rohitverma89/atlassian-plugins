package com.atlassian.plugin;

public final class MockApplication implements Application {
    private String key;
    private String version;
    private String buildNumber;

    public String getKey() {
        return key;
    }

    public MockApplication setKey(String key) {
        this.key = key;
        return this;
    }

    public String getVersion() {
        return version;
    }

    public MockApplication setVersion(String version) {
        this.version = version;
        return this;
    }

    public String getBuildNumber() {
        return buildNumber;
    }

    public MockApplication setBuildNumber(String buildNumber) {
        this.buildNumber = buildNumber;
        return this;
    }
}
