package com.atlassian.plugin.manager;

import com.atlassian.plugin.ModuleDescriptorFactory;
import com.atlassian.plugin.Plugin;
import com.atlassian.plugin.PluginRegistry;
import com.atlassian.plugin.classloader.PluginsClassLoader;
import com.atlassian.plugin.descriptors.UnrecognisedModuleDescriptor;
import com.atlassian.plugin.event.PluginEventManager;
import com.google.common.collect.ImmutableList;
import com.atlassian.plugin.scope.ScopeManager;
import org.junit.Rule;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.instanceOf;
import static org.hamcrest.Matchers.is;
import static org.mockito.Mockito.when;

public class TestProductPluginAccessorBase {
    @Rule
    public MockitoRule mockitoRule = MockitoJUnit.rule();

    @Mock
    private PluginRegistry.ReadOnly pluginRegistry;
    @Mock
    private PluginPersistentStateStore pluginsPersistentStateStore;
    @Mock
    private ModuleDescriptorFactory moduleDescriptorFactory;
    @Mock
    private PluginEventManager pluginEventManager;
    @Mock
    private Plugin plugin;

    @InjectMocks
    private ProductPluginAccessorBase pluginAccessor;

    @Mock
    private ScopeManager scopeManager;

    @Test
    public void classLoderIsPluginsClassLoader() {
        ProductPluginAccessorBase pluginAccessor = new ProductPluginAccessorBase(pluginRegistry,
                pluginsPersistentStateStore, moduleDescriptorFactory, pluginEventManager, scopeManager);

        // needs to be a PluginsClassLoader to work properly with DPM
        assertThat(pluginAccessor.getClassLoader(), is(instanceOf(PluginsClassLoader.class)));
    }

    @Test
    public void testUnrecognizedNotHidden() throws Exception {
        when(pluginRegistry.getAll()).thenReturn(ImmutableList.of(plugin));
        when(plugin.getModuleDescriptors()).thenReturn(ImmutableList.of(new UnrecognisedModuleDescriptor()));

        assertThat(pluginAccessor.getModuleDescriptors(d -> true), hasSize(1));
    }
}
