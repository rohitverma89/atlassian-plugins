package com.atlassian.plugin.manager;

import com.atlassian.plugin.Plugin;
import com.atlassian.plugin.PluginDependencies;
import com.atlassian.plugin.PluginException;
import com.atlassian.plugin.PluginState;
import com.atlassian.plugin.impl.UnloadablePlugin;
import com.google.common.base.Function;
import com.google.common.collect.ImmutableSet;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

public class PluginWithDeps extends UnloadablePlugin {
    private final PluginDependencies dependencies;
    private int timesEnabled = 0;
    private int timesDisabled = 0;

    public static final Function<Plugin, String> GET_KEY = new Function<Plugin, String>() {
        @Nonnull
        @Override
        public String apply(@Nullable final Plugin input) {
            return input.getKey();
        }
    };

    public PluginWithDeps(final String key) {
        this(key, new PluginDependencies(null, null, null));
    }

    public PluginWithDeps(final String key, final String... deps) {
        this(key, new PluginDependencies(ImmutableSet.copyOf(deps), null, null));
    }

    public PluginWithDeps(final String key, final PluginDependencies deps) {
        setKey(key);
        dependencies = deps;
        setPluginState(PluginState.DISABLED);
    }

    @Override
    public void enable() {
        super.enable();
        timesEnabled++;
    }

    public int getTimesEnabled() {
        return timesEnabled;
    }

    public int getTimesDisabled() {
        return timesDisabled;
    }

    @Override
    protected void disableInternal() throws PluginException {
        super.disableInternal();
        timesDisabled++;
    }

    @Nonnull
    @Override
    public PluginDependencies getDependencies() {
        return dependencies;
    }
}
