package com.atlassian.plugin.manager;

import com.atlassian.plugin.DefaultModuleDescriptorFactory;
import com.atlassian.plugin.ModuleDescriptor;
import com.atlassian.plugin.ModuleDescriptorFactory;
import com.atlassian.plugin.Permissions;
import com.atlassian.plugin.Plugin;
import com.atlassian.plugin.PluginArtifact;
import com.atlassian.plugin.PluginController;
import com.atlassian.plugin.PluginDependencies;
import com.atlassian.plugin.PluginException;
import com.atlassian.plugin.PluginInstaller;
import com.atlassian.plugin.PluginInternal;
import com.atlassian.plugin.PluginParseException;
import com.atlassian.plugin.PluginRestartState;
import com.atlassian.plugin.PluginState;
import com.atlassian.plugin.StateAware;
import com.atlassian.plugin.event.NotificationException;
import com.atlassian.plugin.event.PluginEventManager;
import com.atlassian.plugin.event.events.BeforePluginDisabledEvent;
import com.atlassian.plugin.event.events.BeforePluginModuleDisabledEvent;
import com.atlassian.plugin.event.events.PluginDependentsChangedEvent;
import com.atlassian.plugin.event.events.PluginDisabledEvent;
import com.atlassian.plugin.event.events.PluginDisablingEvent;
import com.atlassian.plugin.event.events.PluginEnabledEvent;
import com.atlassian.plugin.event.events.PluginEnablingEvent;
import com.atlassian.plugin.event.events.PluginEvent;
import com.atlassian.plugin.event.events.PluginFrameworkShutdownEvent;
import com.atlassian.plugin.event.events.PluginFrameworkShuttingDownEvent;
import com.atlassian.plugin.event.events.PluginInstalledEvent;
import com.atlassian.plugin.event.events.PluginInstallingEvent;
import com.atlassian.plugin.event.events.PluginModuleDisabledEvent;
import com.atlassian.plugin.event.events.PluginModuleDisablingEvent;
import com.atlassian.plugin.event.events.PluginModuleEnabledEvent;
import com.atlassian.plugin.event.events.PluginModuleEnablingEvent;
import com.atlassian.plugin.event.events.PluginModuleEvent;
import com.atlassian.plugin.event.events.PluginUninstalledEvent;
import com.atlassian.plugin.event.events.PluginUninstallingEvent;
import com.atlassian.plugin.event.events.PluginUpgradedEvent;
import com.atlassian.plugin.event.events.PluginUpgradingEvent;
import com.atlassian.plugin.event.impl.DefaultPluginEventManager;
import com.atlassian.plugin.event.listeners.PassListener;
import com.atlassian.plugin.exception.PluginExceptionInterception;
import com.atlassian.plugin.hostcontainer.DefaultHostContainer;
import com.atlassian.plugin.impl.StaticPlugin;
import com.atlassian.plugin.loaders.DiscardablePluginLoader;
import com.atlassian.plugin.loaders.DynamicPluginLoader;
import com.atlassian.plugin.loaders.PluginLoader;
import com.atlassian.plugin.manager.store.MemoryPluginPersistentStateStore;
import com.atlassian.plugin.parsers.DescriptorParser;
import com.google.common.base.Predicates;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableSet;
import org.dom4j.Element;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.mockito.ArgumentCaptor;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

import static com.atlassian.plugin.manager.DefaultPluginManagerMocks.anyPluginEvent;
import static com.atlassian.plugin.manager.DefaultPluginManagerMocks.anyPluginStateChange;
import static com.atlassian.plugin.manager.DefaultPluginManagerMocks.mockPluginLoaderForPlugins;
import static com.atlassian.plugin.manager.DefaultPluginManagerMocks.mockPluginPersistentStateStore;
import static com.atlassian.plugin.manager.DefaultPluginManagerMocks.mockPluginsSortOrder;
import static com.atlassian.plugin.manager.DefaultPluginManagerMocks.mockStateChangePlugin;
import static com.atlassian.plugin.manager.DefaultPluginManagerMocks.mockStateChangePluginModule;
import static com.atlassian.plugin.manager.DefaultPluginManagerMocks.pluginEvent;
import static com.atlassian.plugin.manager.DefaultPluginManagerMocks.pluginModuleEvent;
import static com.atlassian.plugin.manager.DefaultPluginManagerMocks.pluginModuleStateChange;
import static com.atlassian.plugin.manager.DefaultPluginManagerMocks.pluginStateChange;
import static com.google.common.collect.ImmutableList.copyOf;
import static com.google.common.collect.Iterables.filter;
import static com.google.common.collect.Iterables.get;
import static com.google.common.collect.Iterables.getLast;
import static com.google.common.collect.Iterables.indexOf;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.contains;
import static org.hamcrest.Matchers.containsInAnyOrder;
import static org.hamcrest.Matchers.containsString;
import static org.hamcrest.Matchers.empty;
import static org.hamcrest.Matchers.greaterThan;
import static org.hamcrest.Matchers.instanceOf;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.lessThan;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.isA;
import static org.mockito.Mockito.RETURNS_DEEP_STUBS;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.mockito.Mockito.withSettings;

public class TestDefaultPluginManagerController {
    @Rule
    public ExpectedException expectedException = ExpectedException.none();

    /**
     * the object being tested
     */
    private PluginController manager;

    private PluginEventManager pluginEventManager = new DefaultPluginEventManager();

    private ModuleDescriptorFactory moduleDescriptorFactory = new DefaultModuleDescriptorFactory(new DefaultHostContainer());

    private DefaultPluginManager newDefaultPluginManager(PluginLoader... pluginLoaders) {
        DefaultPluginManager dpm = DefaultPluginManager.newBuilder().
                withPluginLoaders(copyOf(pluginLoaders))
                .withModuleDescriptorFactory(moduleDescriptorFactory)
                .withPluginEventManager(pluginEventManager)
                .withStore(new MemoryPluginPersistentStateStore())
                .withVerifyRequiredPlugins(true)
                .build();
        manager = dpm;
        return dpm;
    }

    private PluginController initNewDefaultPluginManager(PluginLoader... pluginLoaders) {
        DefaultPluginManager dpm = newDefaultPluginManager(pluginLoaders);
        dpm.init();
        return dpm;
    }

    @Test
    public void pluginReturnedByLoadAllPluginsButNotUsedIsDiscarded() {
        final String pluginKey = "pluginKey";
        final PluginPersistentStateStore pluginPersistentStateStore = mock(
                PluginPersistentStateStore.class, RETURNS_DEEP_STUBS);
        when(pluginPersistentStateStore.load().getPluginRestartState(pluginKey)).thenReturn(PluginRestartState.NONE);

        DiscardablePluginLoader pluginLoader = mock(DiscardablePluginLoader.class);
        Plugin pluginV1 = mock(Plugin.class, RETURNS_DEEP_STUBS);
        Plugin pluginV2 = mock(Plugin.class, RETURNS_DEEP_STUBS);
        when(pluginV1.getKey()).thenReturn(pluginKey);
        when(pluginV2.getKey()).thenReturn(pluginKey);
        // Set up so that pluginV1 < pluginV2 so DefaultPluginManager should install only pluginV2
        when(pluginV1.compareTo(pluginV2)).thenReturn(-1);
        when(pluginV2.compareTo(pluginV1)).thenReturn(1);
        when(pluginLoader.loadAllPlugins(isA(ModuleDescriptorFactory.class))).thenReturn(Arrays.asList(pluginV1, pluginV2));

        final DefaultPluginManager defaultPluginManager = new DefaultPluginManager(
                pluginPersistentStateStore,
                Arrays.asList((PluginLoader) pluginLoader),
                mock(ModuleDescriptorFactory.class),
                mock(PluginEventManager.class),
                mock(PluginExceptionInterception.class)
        );
        defaultPluginManager.init();
        verify(pluginLoader).discardPlugin(pluginV1);
    }

    @Test
    public void oldPluginReturnedByLoadFoundPluginsIsDiscarded() {
        final String pluginKey = "pluginKey";
        final PluginPersistentStateStore pluginPersistentStateStore = mock(
                PluginPersistentStateStore.class, RETURNS_DEEP_STUBS);
        when(pluginPersistentStateStore.load().getPluginRestartState(pluginKey)).thenReturn(PluginRestartState.NONE);

        DiscardablePluginLoader pluginLoader = mock(DiscardablePluginLoader.class);
        Plugin pluginV1 = mock(Plugin.class, RETURNS_DEEP_STUBS);
        Plugin pluginV2 = mock(Plugin.class, RETURNS_DEEP_STUBS);
        when(pluginV1.getKey()).thenReturn(pluginKey);
        when(pluginV2.getKey()).thenReturn(pluginKey);
        // Set up so that pluginV1 < pluginV2 so DefaultPluginManager should install only pluginV2
        when(pluginV1.compareTo(pluginV2)).thenReturn(-1);
        when(pluginV2.compareTo(pluginV1)).thenReturn(1);
        when(pluginLoader.loadAllPlugins(isA(ModuleDescriptorFactory.class))).thenReturn(Arrays.asList(pluginV2));
        when(pluginLoader.supportsAddition()).thenReturn(true);
        when(pluginLoader.loadFoundPlugins(isA(ModuleDescriptorFactory.class))).thenReturn(Arrays.asList(pluginV1));

        final DefaultPluginManager defaultPluginManager = new DefaultPluginManager(
                pluginPersistentStateStore,
                Arrays.asList((PluginLoader) pluginLoader),
                mock(ModuleDescriptorFactory.class),
                mock(PluginEventManager.class),
                mock(PluginExceptionInterception.class)
        );
        defaultPluginManager.init();
        final int found = defaultPluginManager.scanForNewPlugins();
        assertThat(found, is(1));
        verify(pluginLoader).discardPlugin(pluginV1);
    }

    @Test
    public void upgradePluginDisablesDependentPlugins() {
        final PluginPersistentStateStore pluginPersistentStateStore = mockPluginPersistentStateStore();

        final String pluginKey = "pluginKey";
        final Plugin pluginV1 = mockStateChangePlugin(pluginKey, pluginEventManager);
        final Plugin pluginV2 = mockStateChangePlugin(pluginKey, pluginEventManager);
        when(pluginV1.isDeleteable()).thenReturn(true);

        final String dependentPluginKey = "dependentPluginKey";
        final Plugin dependentPlugin = mockStateChangePlugin(dependentPluginKey, pluginEventManager);
        when(dependentPlugin.isEnabledByDefault()).thenReturn(true);
        when(dependentPlugin.getDependencies()).thenReturn(new PluginDependencies(ImmutableSet.of(pluginKey), null, null));

        // We need to compareTo to work for plugins that participate in the same addPlugins, so set a good global order
        mockPluginsSortOrder(pluginV1, pluginV2, dependentPlugin);

        final DiscardablePluginLoader pluginLoader = mock(DiscardablePluginLoader.class);
        when(pluginLoader.supportsAddition()).thenReturn(true);
        when(pluginLoader.supportsRemoval()).thenReturn(true);
        final List<Plugin> initialPlugins = Arrays.asList(pluginV1, dependentPlugin);
        when(pluginLoader.loadAllPlugins(isA(ModuleDescriptorFactory.class))).thenReturn(initialPlugins);
        when(pluginLoader.loadFoundPlugins(isA(ModuleDescriptorFactory.class))).thenReturn(Arrays.asList(pluginV2));

        final DefaultPluginManager defaultPluginManager = new DefaultPluginManager(
                pluginPersistentStateStore,
                Arrays.asList((PluginLoader) pluginLoader),
                mock(ModuleDescriptorFactory.class),
                mock(PluginEventManager.class),
                mock(PluginExceptionInterception.class)
        );
        defaultPluginManager.init();
        verify(pluginV1).enable();
        verify(dependentPlugin).enable();
        when(pluginV1.getPluginState()).thenReturn(PluginState.ENABLED);
        when(dependentPlugin.getPluginState()).thenReturn(PluginState.ENABLED);

        final int found = defaultPluginManager.scanForNewPlugins();
        assertThat(found, is(1));
        verify(dependentPlugin).disable();
        verify(dependentPlugin, times(2)).enable();
    }

    @Test
    public void scanForNewPluginsScansAllPluginLoaders() {
        final PluginPersistentStateStore pluginPersistentStateStore = mockPluginPersistentStateStore();

        final PluginLoader pluginLoaderAlpha = mockPluginLoaderForPlugins();
        when(pluginLoaderAlpha.supportsAddition()).thenReturn(true);
        final PluginLoader pluginLoaderBeta = mockPluginLoaderForPlugins();
        when(pluginLoaderBeta.supportsAddition()).thenReturn(true);

        final DefaultPluginManager defaultPluginManager = new DefaultPluginManager(
                pluginPersistentStateStore,
                Arrays.asList(pluginLoaderAlpha, pluginLoaderBeta),
                mock(ModuleDescriptorFactory.class),
                mock(PluginEventManager.class),
                mock(PluginExceptionInterception.class)
        );
        defaultPluginManager.init();

        assertThat(defaultPluginManager.getPlugins(), empty());

        final Plugin pluginAlpha = mock(Plugin.class, RETURNS_DEEP_STUBS);
        when(pluginAlpha.getKey()).thenReturn("alpha");
        when(pluginLoaderAlpha.loadFoundPlugins(isA(ModuleDescriptorFactory.class))).thenReturn(Arrays.asList(pluginAlpha));
        final Plugin pluginBeta = mock(Plugin.class, RETURNS_DEEP_STUBS);
        when(pluginBeta.getKey()).thenReturn("beta");
        when(pluginLoaderBeta.loadFoundPlugins(isA(ModuleDescriptorFactory.class))).thenReturn(Arrays.asList(pluginBeta));

        final int found = defaultPluginManager.scanForNewPlugins();

        assertThat(found, is(2));
        assertThat(defaultPluginManager.getPlugins(), containsInAnyOrder(pluginAlpha, pluginBeta));
    }


    @Test
    public void installEventSequencing() {
        final PluginEventManager pluginEventManager = mock(PluginEventManager.class);

        final Plugin alphaPlugin = mockStateChangePlugin("alpha", pluginEventManager);
        final Plugin betaPlugin = mockStateChangePlugin("beta", pluginEventManager);

        final PluginPersistentStateStore pluginPersistentStateStore = mockPluginPersistentStateStore();

        final PluginLoader pluginLoader = mockPluginLoaderForPlugins(alphaPlugin);
        when(pluginLoader.supportsAddition()).thenReturn(true);

        final DefaultPluginManager defaultPluginManager = new DefaultPluginManager(
                pluginPersistentStateStore,
                ImmutableList.of(pluginLoader),
                mock(ModuleDescriptorFactory.class),
                pluginEventManager,
                mock(PluginExceptionInterception.class)
        );

        final ArgumentCaptor<Object> initEvents = ArgumentCaptor.forClass(Object.class);
        doNothing().when(pluginEventManager).broadcast(initEvents.capture());

        defaultPluginManager.init();

        assertThat(filter(initEvents.getAllValues(), PluginEvent.class), contains(
                pluginEvent(PluginInstallingEvent.class, alphaPlugin),
                pluginStateChange(alphaPlugin, PluginState.INSTALLED),
                pluginEvent(PluginInstalledEvent.class, alphaPlugin),
                pluginEvent(PluginEnablingEvent.class, alphaPlugin),
                pluginStateChange(alphaPlugin, PluginState.ENABLED),
                pluginEvent(PluginEnabledEvent.class, alphaPlugin)
        ));

        when(pluginLoader.loadFoundPlugins(isA(ModuleDescriptorFactory.class))).thenReturn(Arrays.asList(betaPlugin));

        final ArgumentCaptor<Object> scanEvents = ArgumentCaptor.forClass(Object.class);
        doNothing().when(pluginEventManager).broadcast(scanEvents.capture());

        final int found = defaultPluginManager.scanForNewPlugins();
        // This is really just checking the test isn't broken
        assertThat(found, is(1));

        assertThat(filter(scanEvents.getAllValues(), PluginEvent.class), contains(
                pluginEvent(PluginInstallingEvent.class, betaPlugin),
                pluginStateChange(betaPlugin, PluginState.INSTALLED),
                pluginEvent(PluginInstalledEvent.class, betaPlugin),
                pluginEvent(PluginEnablingEvent.class, betaPlugin),
                pluginStateChange(betaPlugin, PluginState.ENABLED),
                pluginEvent(PluginEnabledEvent.class, betaPlugin)
        ));
    }


    @Test
    public void upgradeEventSequencing() {
        final PluginEventManager pluginEventManager = mock(PluginEventManager.class);

        final String pluginKey = "pluginKey";
        final Plugin pluginV1 = mockStateChangePlugin(pluginKey, pluginEventManager);
        final Plugin pluginV2 = mockStateChangePlugin(pluginKey, pluginEventManager);

        final Plugin dependentPlugin = mockStateChangePlugin("dependentPluginKey", pluginEventManager);
        when(dependentPlugin.getDependencies()).thenReturn(new PluginDependencies(ImmutableSet.of(pluginKey), null, null));

        mockPluginsSortOrder(dependentPlugin, pluginV1, pluginV2);

        final PluginPersistentStateStore pluginPersistentStateStore = mockPluginPersistentStateStore();

        final PluginLoader pluginLoader = mockPluginLoaderForPlugins(pluginV1, dependentPlugin);
        when(pluginLoader.supportsAddition()).thenReturn(true);
        when(pluginLoader.supportsRemoval()).thenReturn(true);

        final DefaultPluginManager defaultPluginManager = new DefaultPluginManager(
                pluginPersistentStateStore,
                ImmutableList.of(pluginLoader),
                mock(ModuleDescriptorFactory.class),
                pluginEventManager,
                mock(PluginExceptionInterception.class)
        );

        defaultPluginManager.init();

        when(pluginLoader.loadFoundPlugins(isA(ModuleDescriptorFactory.class))).thenReturn(ImmutableList.of(pluginV2));
        final ArgumentCaptor<Object> events = ArgumentCaptor.forClass(Object.class);
        doNothing().when(pluginEventManager).broadcast(events.capture());

        final int found = defaultPluginManager.scanForNewPlugins();
        // This is really just checking the test isn't broken
        assertThat(found, is(1));

        assertThat(filter(events.getAllValues(), PluginEvent.class), contains(
                pluginEvent(BeforePluginDisabledEvent.class, dependentPlugin),
                pluginEvent(PluginDisablingEvent.class, dependentPlugin),
                pluginStateChange(dependentPlugin, PluginState.DISABLED),
                pluginEvent(PluginDisabledEvent.class, dependentPlugin),
                pluginEvent(PluginUpgradingEvent.class, pluginV1),
                pluginEvent(BeforePluginDisabledEvent.class, pluginV1),
                pluginEvent(PluginDisablingEvent.class, pluginV1),
                pluginStateChange(pluginV1, PluginState.DISABLED),
                pluginEvent(PluginDisabledEvent.class, pluginV1),
                pluginStateChange(pluginV2, PluginState.INSTALLED),
                pluginEvent(PluginUpgradedEvent.class, pluginV2),
                pluginEvent(PluginEnablingEvent.class, pluginV2),
                pluginEvent(PluginEnablingEvent.class, dependentPlugin),
                pluginStateChange(pluginV2, PluginState.ENABLED),
                pluginStateChange(dependentPlugin, PluginState.ENABLED),
                pluginEvent(PluginEnabledEvent.class, pluginV2),
                pluginEvent(PluginEnabledEvent.class, dependentPlugin),
                pluginEvent(PluginDependentsChangedEvent.class, pluginV2)
        ));
    }

    @Test
    public void uninstallEventSequencing() {
        final PluginEventManager pluginEventManager = mock(PluginEventManager.class);

        final String pluginKey = "pluginKey";
        final Plugin plugin = mockStateChangePlugin(pluginKey, pluginEventManager);

        final Plugin dependentPlugin = mockStateChangePlugin("dependentPluginKey", pluginEventManager);
        when(dependentPlugin.getDependencies()).thenReturn(new PluginDependencies(ImmutableSet.of(pluginKey), null, null));

        mockPluginsSortOrder(dependentPlugin, plugin);

        final PluginPersistentStateStore pluginPersistentStateStore = mockPluginPersistentStateStore();

        final PluginLoader pluginLoader = mockPluginLoaderForPlugins(plugin, dependentPlugin);
        when(pluginLoader.supportsRemoval()).thenReturn(true);

        final DefaultPluginManager defaultPluginManager = new DefaultPluginManager(
                pluginPersistentStateStore,
                ImmutableList.of(pluginLoader),
                mock(ModuleDescriptorFactory.class),
                pluginEventManager,
                mock(PluginExceptionInterception.class)
        );

        defaultPluginManager.init();

        final ArgumentCaptor<Object> events = ArgumentCaptor.forClass(Object.class);
        doNothing().when(pluginEventManager).broadcast(events.capture());

        defaultPluginManager.uninstall(defaultPluginManager.getPlugin(pluginKey));

        assertThat(filter(events.getAllValues(), PluginEvent.class), contains(
                pluginEvent(BeforePluginDisabledEvent.class, dependentPlugin),
                pluginEvent(PluginDisablingEvent.class, dependentPlugin),
                pluginStateChange(dependentPlugin, PluginState.DISABLED),
                pluginEvent(PluginDisabledEvent.class, dependentPlugin),
                pluginEvent(PluginUninstallingEvent.class, plugin),
                pluginEvent(BeforePluginDisabledEvent.class, plugin),
                pluginEvent(PluginDisablingEvent.class, plugin),
                pluginStateChange(plugin, PluginState.DISABLED),
                pluginEvent(PluginDisabledEvent.class, plugin),
                pluginEvent(PluginUninstalledEvent.class, plugin),
                pluginEvent(PluginDependentsChangedEvent.class, plugin)
        ));
    }

    @Test
    public void uninstallPluginsEventSequencing() {
        final PluginEventManager pluginEventManager = mock(PluginEventManager.class);

        final String pluginKey = "pluginKey";
        final Plugin plugin = mockStateChangePlugin(pluginKey, pluginEventManager);

        final Plugin dependentPlugin = mockStateChangePlugin("dependentPluginKey", pluginEventManager);
        when(dependentPlugin.getDependencies()).thenReturn(new PluginDependencies(ImmutableSet.of(pluginKey), null, null));

        final String dependent2PluginKey = "dependent2PluginKey";
        final Plugin dependent2Plugin = mockStateChangePlugin(dependent2PluginKey, pluginEventManager);
        when(dependent2Plugin.getDependencies()).thenReturn(new PluginDependencies(ImmutableSet.of(pluginKey), null, null));

        mockPluginsSortOrder(dependentPlugin, dependent2Plugin, plugin);

        final PluginPersistentStateStore pluginPersistentStateStore = mockPluginPersistentStateStore();

        final PluginLoader pluginLoader = mockPluginLoaderForPlugins(plugin, dependentPlugin, dependent2Plugin);
        when(pluginLoader.supportsRemoval()).thenReturn(true);

        final DefaultPluginManager defaultPluginManager = new DefaultPluginManager(
                pluginPersistentStateStore,
                ImmutableList.of(pluginLoader),
                mock(ModuleDescriptorFactory.class),
                pluginEventManager,
                mock(PluginExceptionInterception.class)
        );

        defaultPluginManager.init();

        final ArgumentCaptor<Object> events = ArgumentCaptor.forClass(Object.class);
        doNothing().when(pluginEventManager).broadcast(events.capture());

        Plugin[] uninstalled = new Plugin[]{plugin, dependent2Plugin};

        defaultPluginManager.uninstallPlugins(Arrays.asList(uninstalled));


        List<PluginEvent> pluginEvents = events.getAllValues().stream()
                .filter(PluginEvent.class::isInstance)
                .map(PluginEvent.class::cast)
                .collect(Collectors.toList());

        // Check event sequencing for each of the disabled plugins
        Arrays.stream(uninstalled)
                .forEach(p -> assertThat(
                        pluginEvents.stream()
                                .filter(e -> e.getPlugin() == p)
                                .collect(Collectors.toList()),
                        contains(
                                pluginEvent(PluginUninstallingEvent.class, p),
                                pluginEvent(BeforePluginDisabledEvent.class, p),
                                pluginEvent(PluginDisablingEvent.class, p),
                                pluginStateChange(p, PluginState.DISABLED),
                                pluginEvent(PluginDisabledEvent.class, p),
                                pluginEvent(PluginUninstalledEvent.class, p),
                                pluginEvent(PluginDependentsChangedEvent.class, p)
                        )));

        // check event sequencing for all plugins in relation to each other
        assertThat(pluginEvents,
                contains(
                        // dependent is disabled
                        pluginEvent(BeforePluginDisabledEvent.class, dependentPlugin),
                        pluginEvent(PluginDisablingEvent.class, dependentPlugin),
                        pluginStateChange(dependentPlugin, PluginState.DISABLED),
                        pluginEvent(PluginDisabledEvent.class, dependentPlugin),

                        // uninstalling for both
                        anyPluginEvent(PluginUninstallingEvent.class, uninstalled),
                        anyPluginEvent(PluginUninstallingEvent.class, uninstalled),

                        // disabling sequence for one of them
                        anyPluginEvent(BeforePluginDisabledEvent.class, uninstalled),
                        anyPluginEvent(PluginDisablingEvent.class, uninstalled),
                        anyPluginStateChange(PluginState.DISABLED, uninstalled),
                        anyPluginEvent(PluginDisabledEvent.class, uninstalled),

                        // disabling sequence for the second one
                        anyPluginEvent(BeforePluginDisabledEvent.class, uninstalled),
                        anyPluginEvent(PluginDisablingEvent.class, uninstalled),
                        anyPluginStateChange(PluginState.DISABLED, uninstalled),
                        anyPluginEvent(PluginDisabledEvent.class, uninstalled),

                        //uninstalled for both
                        anyPluginEvent(PluginUninstalledEvent.class, uninstalled),
                        anyPluginEvent(PluginUninstalledEvent.class, uninstalled),

                        // dependent changed
                        anyPluginEvent(PluginDependentsChangedEvent.class, uninstalled),
                        anyPluginEvent(PluginDependentsChangedEvent.class, uninstalled)
                ));
    }

    @Test
    public void moduleEnableDisableEventSequencing() {
        final PluginEventManager pluginEventManager = mock(PluginEventManager.class);

        final String pluginKey = "pluginKey";
        final Plugin plugin = mockStateChangePlugin(pluginKey, pluginEventManager);

        final String moduleKeyAlpha = "alpha";
        final ModuleDescriptor moduleAlpha = mockStateChangePluginModule(pluginKey, moduleKeyAlpha, pluginEventManager);
        final String moduleKeyBeta = "beta";
        final ModuleDescriptor moduleBeta = mockStateChangePluginModule(pluginKey, moduleKeyBeta, pluginEventManager);

        when(plugin.getModuleDescriptors()).thenReturn(ImmutableList.<ModuleDescriptor<?>>of(moduleAlpha, moduleBeta));
        when(plugin.getModuleDescriptor(moduleKeyAlpha)).thenReturn(moduleAlpha);
        when(plugin.getModuleDescriptor(moduleKeyBeta)).thenReturn(moduleBeta);

        final PluginPersistentStateStore pluginPersistentStateStore = mockPluginPersistentStateStore();

        final PluginLoader pluginLoader = mockPluginLoaderForPlugins(plugin);

        final DefaultPluginManager defaultPluginManager = new DefaultPluginManager(
                pluginPersistentStateStore,
                ImmutableList.of(pluginLoader),
                mock(ModuleDescriptorFactory.class),
                pluginEventManager,
                mock(PluginExceptionInterception.class)
        );

        defaultPluginManager.init();

        final ArgumentCaptor<Object> disableEventsCaptor = ArgumentCaptor.forClass(Object.class);
        doNothing().when(pluginEventManager).broadcast(disableEventsCaptor.capture());

        defaultPluginManager.disablePlugin(pluginKey);

        final List<Object> disableEvents = disableEventsCaptor.getAllValues();
        final Iterable<PluginModuleEvent> pluginModuleDisableEvents = filter(disableEvents, PluginModuleEvent.class);
        assertThat(pluginModuleDisableEvents, contains(
                pluginModuleEvent(BeforePluginModuleDisabledEvent.class, moduleBeta),
                pluginModuleEvent(PluginModuleDisablingEvent.class, moduleBeta),
                pluginModuleStateChange(moduleBeta, false),
                pluginModuleEvent(PluginModuleDisabledEvent.class, moduleBeta),
                pluginModuleEvent(BeforePluginModuleDisabledEvent.class, moduleAlpha),
                pluginModuleEvent(PluginModuleDisablingEvent.class, moduleAlpha),
                pluginModuleStateChange(moduleAlpha, false),
                pluginModuleEvent(PluginModuleDisabledEvent.class, moduleAlpha)
        ));

        // We now want to check that the plugin events surround the module events
        final int pluginDisablingIndex = indexOf(disableEvents, Predicates.instanceOf(PluginDisablingEvent.class));
        final int firstPluginModuleDisableEvent = disableEvents.indexOf(get(pluginModuleDisableEvents, 0));
        assertThat(firstPluginModuleDisableEvent, greaterThan(pluginDisablingIndex));
        final int lastPluginModuleDisableEventIndex = disableEvents.indexOf(getLast(pluginModuleDisableEvents));
        final int pluginDisabledIndex = indexOf(disableEvents, Predicates.instanceOf(PluginDisabledEvent.class));
        assertThat(lastPluginModuleDisableEventIndex, lessThan(pluginDisabledIndex));

        final ArgumentCaptor<Object> enableEventsCaptor = ArgumentCaptor.forClass(Object.class);
        doNothing().when(pluginEventManager).broadcast(enableEventsCaptor.capture());

        defaultPluginManager.enablePlugins(pluginKey);

        final List<Object> enableEvents = enableEventsCaptor.getAllValues();
        final Iterable<PluginModuleEvent> pluginModuleEnableEvents = filter(enableEvents, PluginModuleEvent.class);
        assertThat(pluginModuleEnableEvents, contains(
                pluginModuleEvent(PluginModuleEnablingEvent.class, moduleAlpha),
                pluginModuleStateChange(moduleAlpha, true),
                pluginModuleEvent(PluginModuleEnabledEvent.class, moduleAlpha),
                pluginModuleEvent(PluginModuleEnablingEvent.class, moduleBeta),
                pluginModuleStateChange(moduleBeta, true),
                pluginModuleEvent(PluginModuleEnabledEvent.class, moduleBeta)
        ));

        // We now want to check that the plugin events surround the module events
        final int pluginEnablingIndex = indexOf(disableEvents, Predicates.instanceOf(PluginDisablingEvent.class));
        final int firstPluginModuleEnableEvent = disableEvents.indexOf(get(pluginModuleDisableEvents, 0));
        assertThat(firstPluginModuleEnableEvent, greaterThan(pluginEnablingIndex));
        final int lastPluginModuleEnableEventIndex = disableEvents.indexOf(getLast(pluginModuleDisableEvents));
        final int pluginEnabledIndex = indexOf(disableEvents, Predicates.instanceOf(PluginDisabledEvent.class));
        assertThat(lastPluginModuleEnableEventIndex, lessThan(pluginEnabledIndex));
    }

    @Test
    public void shutdownEventsAreSent() {
        final PluginPersistentStateStore pluginPersistentStateStore = mockPluginPersistentStateStore();
        final List<PluginLoader> pluginLoaders = Collections.emptyList();
        final ModuleDescriptorFactory moduleDescriptorFactory = mock(ModuleDescriptorFactory.class);
        final PluginEventManager pluginEventManager = mock(PluginEventManager.class);

        final DefaultPluginManager defaultPluginManager = new DefaultPluginManager(
                pluginPersistentStateStore, pluginLoaders, moduleDescriptorFactory, pluginEventManager);

        defaultPluginManager.init();

        // Make PluginEventManager#broadcast throw during shutdown to validate the exception handling in DefaultPluginManager
        final ArgumentCaptor<Object> shutdownEvents = ArgumentCaptor.forClass(Object.class);
        final NotificationException notificationException = new NotificationException(new Throwable());
        doThrow(notificationException).when(pluginEventManager).broadcast(shutdownEvents.capture());

        defaultPluginManager.shutdown();

        // Check that both broadcasts were attempted
        assertThat(shutdownEvents.getAllValues(), contains(
                instanceOf(PluginFrameworkShuttingDownEvent.class), instanceOf(PluginFrameworkShutdownEvent.class)));
    }

    @Test
    public void enableAndDisableEventSequencing() {
        final PluginEventManager pluginEventManager = mock(PluginEventManager.class);

        final String pluginKey = "pluginKey";
        final Plugin plugin = mockStateChangePlugin(pluginKey, pluginEventManager);

        final PluginPersistentStateStore pluginPersistentStateStore = mockPluginPersistentStateStore();

        final PluginLoader pluginLoader = mockPluginLoaderForPlugins(plugin);

        final DefaultPluginManager defaultPluginManager = new DefaultPluginManager(
                pluginPersistentStateStore,
                ImmutableList.of(pluginLoader),
                mock(ModuleDescriptorFactory.class),
                pluginEventManager,
                mock(PluginExceptionInterception.class)
        );

        defaultPluginManager.init();

        final ArgumentCaptor<Object> disableEvents = ArgumentCaptor.forClass(Object.class);
        doNothing().when(pluginEventManager).broadcast(disableEvents.capture());

        defaultPluginManager.disablePlugin(pluginKey);

        assertThat(filter(disableEvents.getAllValues(), PluginEvent.class), contains(
                pluginEvent(BeforePluginDisabledEvent.class, plugin),
                pluginEvent(PluginDisablingEvent.class, plugin),
                pluginStateChange(plugin, PluginState.DISABLED),
                pluginEvent(PluginDisabledEvent.class, plugin)
        ));

        final ArgumentCaptor<Object> enableEvents = ArgumentCaptor.forClass(Object.class);
        doNothing().when(pluginEventManager).broadcast(enableEvents.capture());

        defaultPluginManager.enablePlugins(pluginKey);

        assertThat(filter(enableEvents.getAllValues(), PluginEvent.class), contains(
                pluginEvent(PluginEnablingEvent.class, plugin),
                pluginStateChange(plugin, PluginState.ENABLED),
                pluginEvent(PluginEnabledEvent.class, plugin)
        ));
    }

    @Test
    public void upgradePluginUpgradesPlugin() {
        final String pluginKey = "pluginKey";
        final PluginPersistentStateStore pluginPersistentStateStore = mock(
                PluginPersistentStateStore.class, RETURNS_DEEP_STUBS);
        when(pluginPersistentStateStore.load().getPluginRestartState(pluginKey)).thenReturn(PluginRestartState.NONE);

        Plugin pluginV1 = mock(Plugin.class, RETURNS_DEEP_STUBS);
        Plugin pluginV2 = mock(Plugin.class, RETURNS_DEEP_STUBS);
        when(pluginV1.getKey()).thenReturn(pluginKey);
        when(pluginV2.getKey()).thenReturn(pluginKey);
        when(pluginV1.isDeleteable()).thenReturn(true);
        when(pluginV1.isUninstallable()).thenReturn(true);
        // Set up so that pluginV1 < pluginV2
        when(pluginV1.compareTo(pluginV2)).thenReturn(-1);
        when(pluginV2.compareTo(pluginV1)).thenReturn(1);

        DiscardablePluginLoader pluginLoader = mock(DiscardablePluginLoader.class);
        when(pluginLoader.supportsAddition()).thenReturn(true);
        when(pluginLoader.supportsRemoval()).thenReturn(true);
        when(pluginLoader.loadAllPlugins(isA(ModuleDescriptorFactory.class))).thenReturn(Arrays.asList(pluginV1));
        when(pluginLoader.loadFoundPlugins(isA(ModuleDescriptorFactory.class))).thenReturn(Arrays.asList(pluginV2));

        final DefaultPluginManager defaultPluginManager = new DefaultPluginManager(
                pluginPersistentStateStore,
                Arrays.asList((PluginLoader) pluginLoader),
                mock(ModuleDescriptorFactory.class),
                mock(PluginEventManager.class),
                mock(PluginExceptionInterception.class)
        );
        defaultPluginManager.init();

        assertThat(defaultPluginManager.getPlugin(pluginKey), is(pluginV1));

        final int found = defaultPluginManager.scanForNewPlugins();

        assertThat(found, is(1));
        assertThat(defaultPluginManager.getPlugin(pluginKey), is(pluginV2));

        verify(pluginLoader).removePlugin(pluginV1);
    }


    @Test
    public void uninstallingNotDeletableUninstallablePluginRemovesItFromLoader() {
        final Plugin plugin = mock(Plugin.class, RETURNS_DEEP_STUBS);
        final PluginLoader pluginLoader = mock(PluginLoader.class);
        final DefaultPluginManager defaultPluginManager = setupUninstallTest(false, true, plugin, pluginLoader);
        defaultPluginManager.uninstall(plugin);
        verify(pluginLoader).removePlugin(plugin);
    }

    @Test
    public void uninstallingDeletableUninstallablePluginRemovesItFromLoader() {
        final Plugin plugin = mock(Plugin.class, RETURNS_DEEP_STUBS);
        final PluginLoader pluginLoader = mock(PluginLoader.class);
        final DefaultPluginManager defaultPluginManager = setupUninstallTest(true, true, plugin, pluginLoader);
        defaultPluginManager.uninstall(plugin);
        verify(pluginLoader).removePlugin(plugin);
    }

    @Test
    public void uninstallingDeletableNotUninstallablePluginDoesNotRemoveItFromLoader() {
        final Plugin plugin = mock(Plugin.class, RETURNS_DEEP_STUBS);
        final PluginLoader pluginLoader = mock(PluginLoader.class);
        final DefaultPluginManager defaultPluginManager = setupUninstallTest(true, false, plugin, pluginLoader);
        // We want to ensure the removal is not attempted, so we make removal throw something unexpected
        doThrow(new AssertionError("Unexpected PluginLoader.removePlugin call")).when(pluginLoader).removePlugin(plugin);
        expectedException.expect(PluginException.class);
        expectedException.expectMessage(plugin.getKey());
        defaultPluginManager.uninstall(plugin);
    }

    @Test
    public void uninstallingNotDeletableNotUninstallablePluginDoesNotRemoveItFromLoader() {
        final Plugin plugin = mock(Plugin.class, RETURNS_DEEP_STUBS);
        final PluginLoader pluginLoader = mock(PluginLoader.class);
        final DefaultPluginManager defaultPluginManager = setupUninstallTest(false, false, plugin, pluginLoader);
        // We want to ensure the removal is not attempted, so we make removal throw something unexpected
        doThrow(new AssertionError("Unexpected PluginLoader.removePlugin call")).when(pluginLoader).removePlugin(plugin);
        expectedException.expect(PluginException.class);
        expectedException.expectMessage(plugin.getKey());
        defaultPluginManager.uninstall(plugin);
    }

    private DefaultPluginManager setupUninstallTest(
            final boolean isDeleteable,
            final boolean isUninstallable,
            final Plugin plugin,
            final PluginLoader pluginLoader) {
        final String pluginKey = "uninstall-test-plugin-key";
        when(plugin.getKey()).thenReturn(pluginKey);
        when(plugin.toString()).thenReturn(pluginKey);
        when(plugin.isDeleteable()).thenReturn(isDeleteable);
        when(plugin.isUninstallable()).thenReturn(isUninstallable);
        when(pluginLoader.loadAllPlugins(isA(ModuleDescriptorFactory.class))).thenReturn(Arrays.asList(plugin));
        when(pluginLoader.supportsRemoval()).thenReturn(true);

        final DefaultPluginManager defaultPluginManager = new DefaultPluginManager(
                mock(PluginPersistentStateStore.class, RETURNS_DEEP_STUBS),
                Arrays.asList(pluginLoader),
                mock(ModuleDescriptorFactory.class),
                mock(PluginEventManager.class),
                mock(PluginExceptionInterception.class)
        );
        defaultPluginManager.init();
        return defaultPluginManager;
    }

    @Test
    public void addDynamicModuleNoLoader() {
        final PluginLoader pluginLoader = mock(PluginLoader.class);
        final PluginInternal plugin = mock(PluginInternal.class);

        manager = newDefaultPluginManager(pluginLoader);

        when(plugin.toString()).thenReturn("bleh");

        expectedException.expect(PluginException.class);
        expectedException.expectMessage(containsString("bleh"));

        manager.addDynamicModule(plugin, mock(Element.class));
    }


    @Test
    public void removeDynamicModuleNotPresent() {
        final PluginLoader pluginLoader = mock(PluginLoader.class);
        final PluginInternal plugin = mock(PluginInternal.class);
        final ModuleDescriptor moduleDescriptor = mock(ModuleDescriptor.class, withSettings().extraInterfaces(StateAware.class));

        manager = newDefaultPluginManager(pluginLoader);

        when(plugin.removeDynamicModuleDescriptor(moduleDescriptor)).thenReturn(false);
        when(plugin.toString()).thenReturn("crapPlugin");
        when(moduleDescriptor.getKey()).thenReturn("moduleKey");

        expectedException.expect(PluginException.class);
        expectedException.expectMessage(containsString("crapPlugin"));
        expectedException.expectMessage(containsString("moduleKey"));

        manager.removeDynamicModule(plugin, moduleDescriptor);

        verify((StateAware) moduleDescriptor, never()).enabled();
    }

    @Test
    public void removeDynamicModule() {
        final PluginLoader pluginLoader = mock(PluginLoader.class);
        final PluginInternal plugin = mock(PluginInternal.class);
        final ModuleDescriptor<?> moduleDescriptor = mock(ModuleDescriptor.class, withSettings().extraInterfaces(StateAware.class));

        manager = newDefaultPluginManager(pluginLoader);

        when(plugin.removeDynamicModuleDescriptor(moduleDescriptor)).thenReturn(true);

        manager.removeDynamicModule(plugin, moduleDescriptor);

        verify(plugin).removeDynamicModuleDescriptor(moduleDescriptor);
        verify(moduleDescriptor).destroy();
        verify((StateAware) moduleDescriptor).disabled();
    }

    @Test
    public void testUninstallPluginWithMultiLevelDependencies() throws PluginException, IOException {

        Plugin child = mock(Plugin.class);
        when(child.getKey()).thenReturn("child");
        when(child.isEnabledByDefault()).thenReturn(true);
        when(child.getPluginState()).thenReturn(PluginState.ENABLED);
        when(child.getDependencies()).thenReturn(new PluginDependencies(ImmutableSet.of("parent"), null, null));
        when(child.compareTo(any(Plugin.class))).thenReturn(-1);

        Plugin parent = mock(Plugin.class);
        when(parent.getKey()).thenReturn("parent");
        when(parent.isEnabledByDefault()).thenReturn(true);
        when(parent.getPluginState()).thenReturn(PluginState.ENABLED);
        when(parent.getDependencies()).thenReturn(new PluginDependencies(ImmutableSet.of("grandparent"), null, null));
        when(parent.compareTo(any(Plugin.class))).thenReturn(-1);

        Plugin grandparent = mock(Plugin.class);
        when(grandparent.getKey()).thenReturn("grandparent");
        when(grandparent.isEnabledByDefault()).thenReturn(true);
        when(grandparent.isDeleteable()).thenReturn(true);
        when(grandparent.isUninstallable()).thenReturn(true);
        when(grandparent.getPluginState()).thenReturn(PluginState.ENABLED);
        when(grandparent.compareTo(any(Plugin.class))).thenReturn(-1);
        when(grandparent.getDependencies()).thenReturn(new PluginDependencies());

        PluginLoader pluginLoader = mockPluginLoaderForPlugins(child, parent, grandparent);
        when(pluginLoader.supportsRemoval()).thenReturn(true);

        manager = initNewDefaultPluginManager(pluginLoader);

        manager.uninstall(grandparent);
        verify(grandparent).enable();
        verify(grandparent).disable();
        verify(pluginLoader).removePlugin(grandparent);

        verify(parent).enable();
        verify(parent).disable();
        verify(child).enable();
        verify(child).disable();
    }

    @Test
    public void testUninstallPluginWithDependencies() throws PluginException, IOException {
        Plugin child = mock(Plugin.class);
        when(child.getKey()).thenReturn("child");
        when(child.isEnabledByDefault()).thenReturn(true);
        when(child.getPluginState()).thenReturn(PluginState.ENABLED);
        when(child.getDependencies()).thenReturn(new PluginDependencies(ImmutableSet.of("parent"), null, null));
        when(child.compareTo(any(Plugin.class))).thenReturn(-1);
        Plugin parent = mock(Plugin.class);
        when(parent.getKey()).thenReturn("parent");
        when(parent.isEnabledByDefault()).thenReturn(true);
        when(parent.isDeleteable()).thenReturn(true);
        when(parent.isUninstallable()).thenReturn(true);
        when(parent.getPluginState()).thenReturn(PluginState.ENABLED);
        when(parent.compareTo(any(Plugin.class))).thenReturn(-1);
        when(parent.getDependencies()).thenReturn(new PluginDependencies());

        PluginLoader pluginLoader = mockPluginLoaderForPlugins(child, parent);
        when(pluginLoader.supportsRemoval()).thenReturn(true);

        manager = initNewDefaultPluginManager(pluginLoader);

        manager.uninstall(parent);
        verify(parent).enable();
        verify(parent).disable();
        verify(pluginLoader).removePlugin(parent);

        verify(child).enable();
        verify(child).disable();
    }

    @Test
    public void testInstallPluginsWithTwoButOneFailsValidationWithException() {
        DynamicPluginLoader loader = mock(DynamicPluginLoader.class);
        when(loader.isDynamicPluginLoader()).thenReturn(true);

        moduleDescriptorFactory = mock(ModuleDescriptorFactory.class);
        PluginInstaller installer = mock(PluginInstaller.class);
        DefaultPluginManager pm = newDefaultPluginManager(loader);
        pm.setPluginInstaller(installer);
        PluginArtifact artifactA = mock(PluginArtifact.class);
        Plugin pluginA = mock(Plugin.class);
        when(loader.canLoad(artifactA)).thenReturn("a");
        PluginArtifact artifactB = mock(PluginArtifact.class);
        Plugin pluginB = mock(Plugin.class);
        doThrow(new PluginParseException()).when(loader).canLoad(artifactB);

        when(loader.loadFoundPlugins(moduleDescriptorFactory)).thenReturn(Arrays.asList(pluginA, pluginB));

        try {
            manager.installPlugins(artifactA, artifactB);
            fail("Should have not installed plugins");
        } catch (PluginParseException ex) {
            // this is good
        }

        verify(loader).canLoad(artifactA);
        verify(loader).canLoad(artifactB);
        verify(installer, never()).installPlugin("a", artifactA);
        verify(installer, never()).installPlugin("b", artifactB);
    }

    @Test
    public void testInstallPlugin() throws Exception {
        final PluginPersistentStateStore mockPluginStateStore = mock(PluginPersistentStateStore.class);
        final ModuleDescriptorFactory moduleDescriptorFactory = mock(ModuleDescriptorFactory.class);
        final DynamicPluginLoader mockPluginLoader = mock(DynamicPluginLoader.class);
        when(mockPluginLoader.isDynamicPluginLoader()).thenReturn(true);

        final DescriptorParser mockDescriptorParser = mock(DescriptorParser.class);
        final PluginArtifact pluginArtifact = mock(PluginArtifact.class);
        final PluginInstaller mockRepository = mock(PluginInstaller.class);
        final Plugin plugin = mock(Plugin.class);


        final DefaultPluginManager pluginManager = new DefaultPluginManager(mockPluginStateStore,
                Collections.<PluginLoader>singletonList(mockPluginLoader), moduleDescriptorFactory, pluginEventManager);

        when(mockPluginStateStore.load()).thenReturn(new DefaultPluginPersistentState());
        when(mockPluginStateStore.load()).thenReturn(new DefaultPluginPersistentState());
        when(mockPluginStateStore.load()).thenReturn(new DefaultPluginPersistentState());
        when(mockDescriptorParser.getKey()).thenReturn("test");
        when(mockPluginLoader.loadAllPlugins(moduleDescriptorFactory)).thenReturn((Iterable) Collections.emptyList());
        when(mockPluginLoader.supportsAddition()).thenReturn(true);
        when(mockPluginLoader.loadFoundPlugins(moduleDescriptorFactory)).thenReturn(Collections.singletonList(plugin));
        when(mockPluginLoader.canLoad(pluginArtifact)).thenReturn("test");
        when(plugin.getKey()).thenReturn("test");
        when(plugin.getModuleDescriptors()).thenReturn((Collection) new ArrayList<Object>());
        when(plugin.getModuleDescriptors()).thenReturn((Collection) new ArrayList<Object>());
        when(plugin.isEnabledByDefault()).thenReturn(true);

        when(plugin.isEnabledByDefault()).thenReturn(true);
        when(plugin.isEnabled()).thenReturn(true);
        when(plugin.getPluginState()).thenReturn(PluginState.ENABLED);
        when(plugin.hasAllPermissions()).thenReturn(true);
        when(plugin.getActivePermissions()).thenReturn(ImmutableSet.of(Permissions.ALL_PERMISSIONS));
        when(plugin.getDependencies()).thenReturn(new PluginDependencies());

        pluginManager.setPluginInstaller(mockRepository);
        pluginManager.init();
        final PassListener enabledListener = new PassListener(PluginEnabledEvent.class);
        pluginEventManager.register(enabledListener);
        pluginManager.installPlugin(pluginArtifact);

        assertEquals(plugin, pluginManager.getPlugin("test"));
        assertTrue(pluginManager.isPluginEnabled("test"));

        enabledListener.assertCalled();
    }

    @Test
    public void testInstallPluginsWithOne() {
        DynamicPluginLoader loader = mock(DynamicPluginLoader.class);
        when(loader.isDynamicPluginLoader()).thenReturn(true);

        moduleDescriptorFactory = mock(ModuleDescriptorFactory.class);
        PluginInstaller installer = mock(PluginInstaller.class);
        DefaultPluginManager pm = newDefaultPluginManager(loader);
        pm.setPluginInstaller(installer);
        when(loader.loadAllPlugins(moduleDescriptorFactory)).thenReturn(Arrays.<Plugin>asList());
        PluginArtifact artifact = mock(PluginArtifact.class);
        Plugin plugin = mock(Plugin.class);
        when(loader.canLoad(artifact)).thenReturn("foo");
        when(loader.loadFoundPlugins(moduleDescriptorFactory)).thenReturn(Arrays.asList(plugin));

        pm.init();
        manager.installPlugins(artifact);

        verify(loader).canLoad(artifact);
        verify(installer).installPlugin("foo", artifact);
    }

    @Test
    public void testInstallPluginsWithTwo() {
        DynamicPluginLoader loader = mock(DynamicPluginLoader.class);
        when(loader.isDynamicPluginLoader()).thenReturn(true);

        moduleDescriptorFactory = mock(ModuleDescriptorFactory.class);
        PluginInstaller installer = mock(PluginInstaller.class);
        DefaultPluginManager pm = newDefaultPluginManager(loader);
        pm.setPluginInstaller(installer);
        when(loader.loadAllPlugins(moduleDescriptorFactory)).thenReturn(Arrays.<Plugin>asList());
        PluginArtifact artifactA = mock(PluginArtifact.class);
        Plugin pluginA = mock(Plugin.class);
        when(loader.canLoad(artifactA)).thenReturn("a");
        PluginArtifact artifactB = mock(PluginArtifact.class);
        Plugin pluginB = mock(Plugin.class);
        when(loader.canLoad(artifactB)).thenReturn("b");

        when(loader.loadFoundPlugins(moduleDescriptorFactory)).thenReturn(Arrays.asList(pluginA, pluginB));

        pm.init();
        manager.installPlugins(artifactA, artifactB);

        verify(loader).canLoad(artifactA);
        verify(loader).canLoad(artifactB);
        verify(installer).installPlugin("a", artifactA);
        verify(installer).installPlugin("b", artifactB);
    }

    @Test
    public void testInstallPluginsWithTwoButOneFailsValidation() {
        DynamicPluginLoader loader = mock(DynamicPluginLoader.class);
        when(loader.isDynamicPluginLoader()).thenReturn(true);

        ModuleDescriptorFactory descriptorFactory = mock(ModuleDescriptorFactory.class);
        PluginEventManager eventManager = mock(PluginEventManager.class);
        PluginInstaller installer = mock(PluginInstaller.class);
        DefaultPluginManager pm = newDefaultPluginManager(loader);
        pm.setPluginInstaller(installer);
        PluginArtifact artifactA = mock(PluginArtifact.class);
        Plugin pluginA = mock(Plugin.class);
        when(loader.canLoad(artifactA)).thenReturn("a");
        PluginArtifact artifactB = mock(PluginArtifact.class);
        Plugin pluginB = mock(Plugin.class);
        when(loader.canLoad(artifactB)).thenReturn(null);

        when(loader.loadFoundPlugins(descriptorFactory)).thenReturn(Arrays.asList(pluginA, pluginB));

        try {
            manager.installPlugins(artifactA, artifactB);
            fail("Should have not installed plugins");
        } catch (PluginParseException ex) {
            // this is good
        }

        verify(loader).canLoad(artifactA);
        verify(loader).canLoad(artifactB);
        verify(installer, never()).installPlugin("a", artifactA);
        verify(installer, never()).installPlugin("b", artifactB);
    }

    @Test
    public void testCircularDependencyWouldNotCauseInfiniteLoop() throws PluginException, IOException {

        Plugin p1 = mock(Plugin.class);
        when(p1.getKey()).thenReturn("p1");
        when(p1.isEnabledByDefault()).thenReturn(true);
        when(p1.getPluginState()).thenReturn(PluginState.ENABLED);
        when(p1.getDependencies()).thenReturn(new PluginDependencies(ImmutableSet.of("p2", "parent"), null, null));
        when(p1.compareTo(any(Plugin.class))).thenReturn(-1);

        // Create a circular dependency between p1 and p2. This should not happen, but test anyway.
        Plugin p2 = mock(Plugin.class);
        when(p2.getKey()).thenReturn("p2");
        when(p2.isEnabledByDefault()).thenReturn(true);
        when(p2.getPluginState()).thenReturn(PluginState.ENABLED);
        when(p2.getDependencies()).thenReturn(new PluginDependencies(ImmutableSet.of("p1"), null, null));
        when(p2.compareTo(any(Plugin.class))).thenReturn(-1);

        Plugin parent = mock(Plugin.class);
        when(parent.getKey()).thenReturn("parent");
        when(parent.isEnabledByDefault()).thenReturn(true);
        when(parent.isDeleteable()).thenReturn(true);
        when(parent.isUninstallable()).thenReturn(true);
        when(parent.getPluginState()).thenReturn(PluginState.ENABLED);
        when(parent.compareTo(any(Plugin.class))).thenReturn(-1);
        when(parent.getDependencies()).thenReturn(new PluginDependencies());

        PluginLoader pluginLoader = mockPluginLoaderForPlugins(p1, p2, parent);
        when(pluginLoader.supportsRemoval()).thenReturn(true);

        manager = initNewDefaultPluginManager(pluginLoader);

        manager.uninstall(parent);
        verify(parent, times(1)).enable();
        verify(parent, times(1)).disable();
        verify(pluginLoader).removePlugin(parent);

        verify(p1, times(1)).enable();
        verify(p1, times(1)).disable();
        verify(p2, times(1)).enable();
        verify(p2, times(1)).disable();
    }

    @Test
    public void testThreeCycleDependencyWouldNotCauseInfiniteLoop() throws PluginException, IOException {
        Plugin p1 = mock(Plugin.class);
        when(p1.getKey()).thenReturn("p1");
        when(p1.isEnabledByDefault()).thenReturn(true);
        when(p1.getPluginState()).thenReturn(PluginState.ENABLED);
        when(p1.getDependencies()).thenReturn(new PluginDependencies(ImmutableSet.of("p2", "parent"), null, null));
        when(p1.compareTo(any(Plugin.class))).thenReturn(-1);

        // Create a circular dependency between p1, p2 and p3. This should not happen, but test anyway.
        Plugin p2 = mock(Plugin.class);
        when(p2.getKey()).thenReturn("p2");
        when(p2.isEnabledByDefault()).thenReturn(true);
        when(p2.getPluginState()).thenReturn(PluginState.ENABLED);
        when(p2.getDependencies()).thenReturn(new PluginDependencies(ImmutableSet.of("p3"), null, null));
        when(p2.compareTo(any(Plugin.class))).thenReturn(-1);

        Plugin p3 = mock(Plugin.class);
        when(p3.getKey()).thenReturn("p3");
        when(p3.isEnabledByDefault()).thenReturn(true);
        when(p3.getPluginState()).thenReturn(PluginState.ENABLED);
        when(p3.getDependencies()).thenReturn(new PluginDependencies(ImmutableSet.of("p1"), null, null));
        when(p3.compareTo(any(Plugin.class))).thenReturn(-1);

        Plugin parent = mock(Plugin.class);
        when(parent.getKey()).thenReturn("parent");
        when(parent.isEnabledByDefault()).thenReturn(true);
        when(parent.isDeleteable()).thenReturn(true);
        when(parent.isUninstallable()).thenReturn(true);
        when(parent.getPluginState()).thenReturn(PluginState.ENABLED);
        when(parent.compareTo(any(Plugin.class))).thenReturn(-1);
        when(parent.getDependencies()).thenReturn(new PluginDependencies());

        PluginLoader pluginLoader = mockPluginLoaderForPlugins(p1, p2, p3, parent);
        when(pluginLoader.supportsRemoval()).thenReturn(true);

        manager = initNewDefaultPluginManager(pluginLoader);

        manager.uninstall(parent);
        verify(parent, times(1)).enable();
        verify(parent, times(1)).disable();
        verify(pluginLoader).removePlugin(parent);

        verify(p1, times(1)).enable();
        verify(p1, times(1)).disable();
        verify(p2, times(1)).enable();
        verify(p2, times(1)).disable();
        verify(p3, times(1)).enable();
        verify(p3, times(1)).disable();
    }

    @Test
    public void addDynamicModule() {
        final PluginLoader pluginLoader = mock(PluginLoader.class);
        final PluginInternal plugin = mock(PluginInternal.class);
        final Element module = mock(Element.class);
        final ModuleDescriptor moduleDescriptor = mock(ModuleDescriptor.class, withSettings().extraInterfaces(StateAware.class));

        newDefaultPluginManager(pluginLoader)
                .addPlugins(pluginLoader, ImmutableList.<Plugin>of(plugin));

        when(pluginLoader.createModule(plugin, module, moduleDescriptorFactory)).thenReturn(moduleDescriptor);
        when(moduleDescriptor.isEnabledByDefault()).thenReturn(true);
        when(plugin.addDynamicModuleDescriptor(moduleDescriptor)).thenReturn(true);
        when(plugin.getPluginState()).thenReturn(PluginState.ENABLED);

        assertThat(manager.addDynamicModule(plugin, module), is(moduleDescriptor));

        verify((StateAware) moduleDescriptor).enabled();
    }

    @Test
    public void addDynamicModulePluginDisabled() {
        final PluginLoader pluginLoader = mock(PluginLoader.class);
        final PluginInternal plugin = mock(PluginInternal.class);
        final Element module = mock(Element.class);
        final ModuleDescriptor moduleDescriptor = mock(ModuleDescriptor.class, withSettings().extraInterfaces(StateAware.class));

        newDefaultPluginManager(pluginLoader)
                .addPlugins(pluginLoader, ImmutableList.<Plugin>of(plugin));

        when(pluginLoader.createModule(plugin, module, moduleDescriptorFactory)).thenReturn(moduleDescriptor);
        when(moduleDescriptor.isEnabledByDefault()).thenReturn(true);
        when(plugin.addDynamicModuleDescriptor(moduleDescriptor)).thenReturn(true);
        when(plugin.getPluginState()).thenReturn(PluginState.DISABLED);

        manager.addDynamicModule(plugin, module);

        verify((StateAware) moduleDescriptor, never()).enabled();
    }

    @Test
    public void addDynamicModuleModuleNotEnabledByDefault() {
        final PluginLoader pluginLoader = mock(PluginLoader.class);
        final PluginInternal plugin = mock(PluginInternal.class);
        final Element module = mock(Element.class);
        final ModuleDescriptor moduleDescriptor = mock(ModuleDescriptor.class, withSettings().extraInterfaces(StateAware.class));

        newDefaultPluginManager(pluginLoader)
                .addPlugins(pluginLoader, ImmutableList.<Plugin>of(plugin));

        when(pluginLoader.createModule(plugin, module, moduleDescriptorFactory)).thenReturn(moduleDescriptor);
        when(moduleDescriptor.isEnabledByDefault()).thenReturn(false);
        when(plugin.addDynamicModuleDescriptor(moduleDescriptor)).thenReturn(true);
        when(plugin.getPluginState()).thenReturn(PluginState.ENABLED);

        manager.addDynamicModule(plugin, module);

        verify((StateAware) moduleDescriptor, never()).enabled();
    }

    @Test
    public void addDynamicModuleDuplicateKey() {
        final PluginLoader pluginLoader = mock(PluginLoader.class);
        final PluginInternal plugin = mock(PluginInternal.class);
        final Element module = mock(Element.class);
        final ModuleDescriptor moduleDescriptor = mock(ModuleDescriptor.class, withSettings().extraInterfaces(StateAware.class));
        final ModuleDescriptor existingModuleDescriptor = mock(ModuleDescriptor.class);

        newDefaultPluginManager(pluginLoader)
                .addPlugins(pluginLoader, ImmutableList.<Plugin>of(plugin));

        when(pluginLoader.createModule(plugin, module, moduleDescriptorFactory)).thenReturn(moduleDescriptor);
        when(plugin.toString()).thenReturn("awesomePlugin");
        when(plugin.getModuleDescriptor("moduleKey")).thenReturn(existingModuleDescriptor);
        when(moduleDescriptor.getKey()).thenReturn("moduleKey");

        expectedException.expect(PluginException.class);
        expectedException.expectMessage(containsString("awesomePlugin"));
        expectedException.expectMessage(containsString("moduleKey"));

        manager.addDynamicModule(plugin, module);

        verify((StateAware) moduleDescriptor, never()).enabled();
    }

    @Test
    public void addDynamicModuleAlreadyPresentAsDynamicModule() {
        final PluginLoader pluginLoader = mock(PluginLoader.class);
        final PluginInternal plugin = mock(PluginInternal.class);
        final Element module = mock(Element.class);
        final ModuleDescriptor moduleDescriptor = mock(ModuleDescriptor.class, withSettings().extraInterfaces(StateAware.class));

        newDefaultPluginManager(pluginLoader)
                .addPlugins(pluginLoader, ImmutableList.<Plugin>of(plugin));

        when(pluginLoader.createModule(plugin, module, moduleDescriptorFactory)).thenReturn(moduleDescriptor);
        when(plugin.addDynamicModuleDescriptor(moduleDescriptor)).thenReturn(false);
        when(plugin.toString()).thenReturn("lawsonPlugin");
        when(moduleDescriptor.getKey()).thenReturn("moduleKey");

        expectedException.expect(PluginException.class);
        expectedException.expectMessage(containsString("lawsonPlugin"));
        expectedException.expectMessage(containsString("moduleKey"));

        manager.addDynamicModule(plugin, module);

        verify((StateAware) moduleDescriptor, never()).enabled();
    }

    private static class CannotEnablePlugin extends StaticPlugin {
        public CannotEnablePlugin() {
            setKey("foo");
        }

        @Override
        protected PluginState enableInternal() {
            throw new RuntimeException("boo");
        }

        public void disabled() {
        }
    }

    @Test
    public void testAddPluginsThatThrowExceptionOnEnabled() throws Exception {
        final Plugin plugin = new CannotEnablePlugin();

        newDefaultPluginManager()
                .addPlugins(null, Arrays.asList(plugin));

        assertFalse(plugin.getPluginState() == PluginState.ENABLED);
    }
}
