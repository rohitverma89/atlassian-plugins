package com.atlassian.plugin.manager;

import com.atlassian.plugin.ModuleDescriptor;
import com.atlassian.plugin.Plugin;
import com.atlassian.plugin.impl.StaticPlugin;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Mock;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.runners.MockitoJUnitRunner;
import org.mockito.stubbing.Answer;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import static org.mockito.Matchers.isA;
import static org.mockito.Mockito.doAnswer;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class TestPluginPersistentStateModifier {
    Plugin plugin;
    PluginPersistentStateModifier persistentStateModifier;
    ArgumentCaptor<PluginPersistentState> argument;

    @Mock
    ModuleDescriptor descriptor;

    @Mock
    PluginPersistentStateStore customStore;

    @Before
    public void setUp() throws Exception {
        plugin = new StaticPlugin();
        plugin.setKey("foo");
        plugin.setEnabledByDefault(true);

        PluginPersistentState.Builder builder = PluginPersistentState.Builder.create();
        builder.setEnabled(plugin, false);
        when(customStore.load()).thenReturn(builder.toState());
        doAnswer(new Answer<Void>() {
            @Override
            public Void answer(InvocationOnMock invocationOnMock) throws Throwable {
                final PluginPersistentState pluginState = (PluginPersistentState) invocationOnMock.getArguments()[0];
                when(customStore.load()).thenReturn(pluginState);
                return null;
            }
        }).when(customStore).save(isA(PluginPersistentState.class));

        when(descriptor.getCompleteKey()).thenReturn("foo:bar");

        persistentStateModifier = new PluginPersistentStateModifier(customStore);
        argument = ArgumentCaptor.forClass(PluginPersistentState.class);
    }

    @Test
    public void testDisablePlugin() throws Exception {
        plugin.setEnabledByDefault(true);
        persistentStateModifier.disable(plugin);

        verify(customStore).save(argument.capture());
        assertThat(argument.getValue().isEnabled(plugin), is(false));
    }

    @Test
    public void testEnablePlugin() throws Exception {
        plugin.setEnabledByDefault(false);
        persistentStateModifier.enable(plugin);

        verify(customStore).save(argument.capture());
        assertThat(argument.getValue().isEnabled(plugin), is(true));
    }

    @Test
    public void testDisableModule() throws Exception {
        when(descriptor.isEnabledByDefault()).thenReturn(true);
        persistentStateModifier.disable(descriptor);

        verify(customStore).save(argument.capture());
        assertThat(argument.getValue().isEnabled(descriptor), is(false));
    }

    @Test
    public void testEnableModule() throws Exception {
        when(descriptor.isEnabledByDefault()).thenReturn(false);
        persistentStateModifier.enable(descriptor);

        verify(customStore).save(argument.capture());
        assertThat(argument.getValue().isEnabled(descriptor), is(true));
    }
}