package com.atlassian.plugin.manager;

import com.atlassian.plugin.Plugin;
import com.atlassian.plugin.PluginArtifact;
import com.atlassian.plugin.PluginInformation;
import com.atlassian.plugin.jmx.PluginManagerMXBean;
import com.google.common.collect.ImmutableList;
import org.hamcrest.Description;
import org.hamcrest.Matcher;
import org.hamcrest.TypeSafeMatcher;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.io.File;
import java.util.Date;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.arrayContaining;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class TestDefaultPluginManagerJmxBridge {
    @Mock
    private DefaultPluginManager defaultPluginManager;

    // Hold this via the interface class so we provide usages of its methods
    private PluginManagerMXBean defaultPluginManagerJmxBridge;

    @Before
    public void setUp() throws Exception {
        defaultPluginManagerJmxBridge = new DefaultPluginManagerJmxBridge(defaultPluginManager);
    }

    @Test
    public void getPluginsReturnsPlugins() {
        final String pluginKey = "alpha";
        final Date dateLoaded = new Date(123L);
        final Date dateInstalled = new Date(456L);
        final boolean enabledByDefault = true;
        final boolean isBundledPlugin = true;
        final Plugin plugin = mock(Plugin.class);
        when(plugin.getKey()).thenReturn(pluginKey);
        when(plugin.isEnabledByDefault()).thenReturn(enabledByDefault);
        when(plugin.getDateInstalled()).thenReturn(dateInstalled);
        when(plugin.getDateLoaded()).thenReturn(dateLoaded);
        when(plugin.isBundledPlugin()).thenReturn(isBundledPlugin);

        final String version = "1.2.3";

        final PluginInformation pluginInformation = mock(PluginInformation.class);

        when(pluginInformation.getVersion()).thenReturn(version);
        when(plugin.getPluginInformation()).thenReturn(pluginInformation);
        when(defaultPluginManager.isPluginEnabled(pluginKey)).thenReturn(true);

        final Plugin nullPlugin = mock(Plugin.class);

        when(defaultPluginManager.getPlugins()).thenReturn(ImmutableList.of(plugin, nullPlugin));
        final PluginManagerMXBean.PluginData[] pluginDatas = defaultPluginManagerJmxBridge.getPlugins();
        assertThat(pluginDatas, arrayContaining(
                pluginData(pluginKey, version, true, true, null, dateInstalled.getTime(), dateLoaded.getTime(), isBundledPlugin),
                pluginData(null, null, false, false, null, null, null, false)
        ));
    }

    @Test
    public void getPluginsForArtifactBasedPluginReturnsLocation() {
        // Construct the Plugin object
        final String pluginKey = "alpha";
        final Date dateLoaded = new Date(123L);
        final Date dateInstalled = new Date(456L);
        final boolean enabledByDefault = true;
        final boolean isBundledPlugin = true;
        final Plugin plugin = mock(Plugin.class);

        when(plugin.getKey()).thenReturn(pluginKey);
        when(plugin.isEnabledByDefault()).thenReturn(enabledByDefault);
        when(plugin.getDateInstalled()).thenReturn(dateInstalled);
        when(plugin.getDateLoaded()).thenReturn(dateLoaded);
        when(plugin.isBundledPlugin()).thenReturn(isBundledPlugin);

        // Construct the PluginArtifact inside the PluginArtifactBackedPlugin
        final PluginArtifact pluginArtifact = mock(PluginArtifact.class);
        final String location = "/sw/ondemand/plugin/plugin-5.3.jar";
        when(pluginArtifact.toFile()).thenReturn(new File(location));
        when(plugin.getPluginArtifact()).thenReturn(pluginArtifact);

        // Construct the PluginInformation inside the Plugin
        final String version = "1.2.3";
        final PluginInformation pluginInformation = mock(PluginInformation.class);
        when(pluginInformation.getVersion()).thenReturn(version);
        when(plugin.getPluginInformation()).thenReturn(pluginInformation);

        // Set the DefaultPluginManager values
        when(defaultPluginManager.isPluginEnabled(pluginKey)).thenReturn(true);
        when(defaultPluginManager.getPlugins()).thenReturn(ImmutableList.<Plugin>of(plugin));
        final PluginManagerMXBean.PluginData[] pluginDatas = defaultPluginManagerJmxBridge.getPlugins();

        assertThat(pluginDatas, arrayContaining(
                pluginData(pluginKey, version, true, enabledByDefault, location, dateInstalled.getTime(), dateLoaded.getTime(), isBundledPlugin)
        ));
    }

    private Matcher<PluginManagerMXBean.PluginData> pluginData(final String key, final String version, final boolean enabled,
                                                               final boolean enabledByDefault, final String location, final Long dateInstalled, final Long dateLoaded,
                                                               final boolean isBundledPlugin) {
        final Matcher<String> keyMatcher = equalTo(key);
        final Matcher<String> versionMatcher = equalTo(version);
        final Matcher<Boolean> enabledMatcher = equalTo(enabled);
        final Matcher<String> locationMatcher = equalTo(location);
        final Matcher<Long> dateInstalledMatcher = equalTo(dateInstalled);
        final Matcher<Long> dateLoadedMatcher = equalTo(dateLoaded);
        final Matcher<Boolean> enabledByDefaultMatcher = equalTo(enabledByDefault);
        final Matcher<Boolean> isBundledPluginMatcher = equalTo(isBundledPlugin);

        return new TypeSafeMatcher<PluginManagerMXBean.PluginData>() {
            @Override
            protected boolean matchesSafely(final PluginManagerMXBean.PluginData item) {
                return keyMatcher.matches(item.getKey())
                        && versionMatcher.matches(item.getVersion())
                        && enabledMatcher.matches(item.isEnabled())
                        && locationMatcher.matches(item.getLocation())
                        && dateInstalledMatcher.matches(item.getDateInstalled())
                        && dateLoadedMatcher.matches(item.getDateLoaded())
                        && enabledByDefaultMatcher.matches(item.isEnabledByDefault())
                        && isBundledPluginMatcher.matches(item.isBundledPlugin());
            }

            @Override
            public void describeTo(final Description description) {
                description.appendText("PluginData with key ");
                keyMatcher.describeTo(description);
                description.appendText(" and version ");
                versionMatcher.describeTo(description);
                description.appendText(" and enabled ");
                enabledMatcher.describeTo(description);
                description.appendText(" and location ");
                locationMatcher.describeTo(description);
                description.appendText(" and dateInstalled ");
                dateInstalledMatcher.describeTo(description);
                description.appendText(" and dateLoaded ");
                dateLoadedMatcher.describeTo(description);
                description.appendText(" and enabledByDefault ");
                enabledByDefaultMatcher.describeTo(description);
                description.appendText(" and isBundledPlugin ");
                isBundledPluginMatcher.describeTo(description);
            }
        };
    }

    @Test
    public void scanForNewPluginsDoesScanForNewPlugins() {
        final int expected = 1;
        when(defaultPluginManager.scanForNewPlugins()).thenReturn(expected);
        final int actual = defaultPluginManagerJmxBridge.scanForNewPlugins();
        assertThat(actual, is(expected));
    }
}
