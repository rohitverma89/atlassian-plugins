package com.atlassian.plugin.manager;

import com.atlassian.plugin.ModuleDescriptorFactory;
import com.atlassian.plugin.PluginAccessor;
import com.atlassian.plugin.PluginRegistry;
import com.atlassian.plugin.event.PluginEventManager;
import com.atlassian.plugin.scope.ScopeManager;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.util.Arrays;
import java.util.Collection;

import static org.mockito.Mockito.mock;

@RunWith(Parameterized.class)
public class TestDefaultPluginManagerWithProductPluginAccessor extends TestDefaultPluginManager {

    private interface PluginAccessorSupplier {
        PluginAccessor supply(final PluginRegistry.ReadOnly pluginRegistry,
                              final PluginPersistentStateStore store,
                              final ModuleDescriptorFactory moduleDescriptorFactory,
                              final PluginEventManager pluginEventManager,
                              final ScopeManager scopeManager);
    }

    @Parameterized.Parameters(name = "{0}")
    public static Collection<Object[]> data() {
        return Arrays.asList(new Object[][]{
                {"ProductPluginAccessor", new PluginAccessorSupplier() {
                    @Override
                    public PluginAccessor supply(final PluginRegistry.ReadOnly pluginRegistry,
                                                 final PluginPersistentStateStore store,
                                                 final ModuleDescriptorFactory moduleDescriptorFactory,
                                                 final PluginEventManager pluginEventManager,
                                                 final ScopeManager scopeManager) {
                        return new ProductPluginAccessor(pluginRegistry, store, moduleDescriptorFactory, pluginEventManager, scopeManager);
                    }
                }},
                {"ProductPluginAccessorBase", new PluginAccessorSupplier() {
                    @Override
                    public PluginAccessor supply(final PluginRegistry.ReadOnly pluginRegistry,
                                                 final PluginPersistentStateStore store,
                                                 final ModuleDescriptorFactory moduleDescriptorFactory,
                                                 final PluginEventManager pluginEventManager,
                                                 final ScopeManager scopeManager) {
                        return new ProductPluginAccessorBase(pluginRegistry, store, moduleDescriptorFactory, pluginEventManager, scopeManager);
                    }
                }}
        });
    }

    @Parameterized.Parameter
    public String testName;

    @Parameterized.Parameter(1)
    public PluginAccessorSupplier pluginAccessorSupplier;

    private PluginAccessor pluginAccessor;

    private ScopeManager scopeManager = mock(ScopeManager.class);

    /**
     * Whenever the plugin manager is recreated, set up the plugin accessor used by the base class tests to use the
     * product accessor
     */
    @Override
    protected DefaultPluginManager newDefaultPluginManager(DefaultPluginManager.Builder builder) {
        final PluginRegistry.ReadWrite pluginRegistry = new PluginRegistryImpl();

        pluginAccessor = pluginAccessorSupplier.supply(pluginRegistry, pluginStateStore, moduleDescriptorFactory, pluginEventManager, scopeManager);

        builder
                .withPluginRegistry(pluginRegistry)
                .withPluginAccessor(pluginAccessor);
        manager = super.newDefaultPluginManager(builder);

        return manager;
    }

    /**
     * Make base class tests use our product plugin accessor
     */
    @Override
    protected PluginAccessor getPluginAccessor() {
        return pluginAccessor;
    }
}
