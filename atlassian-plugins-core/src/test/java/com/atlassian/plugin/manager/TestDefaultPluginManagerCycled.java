package com.atlassian.plugin.manager;

import com.atlassian.plugin.ModuleDescriptorFactory;
import com.atlassian.plugin.Plugin;
import com.atlassian.plugin.PluginRestartState;
import com.atlassian.plugin.PluginState;
import com.atlassian.plugin.event.PluginEventManager;
import com.atlassian.plugin.loaders.PluginLoader;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.ImmutableSet;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;

import java.util.Arrays;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.notNullValue;
import static org.junit.Assert.fail;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.when;

@RunWith(Parameterized.class)
public class TestDefaultPluginManagerCycled extends TestDependentPlugins {
    enum Result {
        DISABLED(PluginState.DISABLED),
        BOUNCED(PluginState.ENABLED),
        UNTOUCHED(PluginState.ENABLED);

        final PluginState finalState;

        Result(final PluginState finalState) {
            this.finalState = finalState;
        }

        public PluginState getFinalState() {
            return finalState;
        }
    }

    @Parameterized.Parameters(name = "{index}: {0}({1}): {2}")
    public static Collection<Object[]> data() {
        return Arrays.asList(new Object[][]{
                {PluginState.DISABLED, ImmutableList.of("G"), ImmutableMap.<Result, Set<String>>builder()
                        .put(Result.DISABLED, ImmutableSet.of("C", "E", "H"))
                        .put(Result.BOUNCED, ImmutableSet.of("D", "F", "A"))
                        .put(Result.UNTOUCHED, ImmutableSet.of("B"))
                        .build()
                },
                {PluginState.UNINSTALLED, ImmutableList.of("G"), ImmutableMap.<Result, Set<String>>builder()
                        .put(Result.DISABLED, ImmutableSet.of("C", "E", "H"))
                        .put(Result.BOUNCED, ImmutableSet.of("B", "D", "F", "A"))
                        .build()
                },
                // circular deps
                {PluginState.DISABLED, ImmutableList.of("a"), ImmutableMap.<Result, Set<String>>builder()
                        .put(Result.DISABLED, ImmutableSet.of("b", "c"))
                        .build()
                },
                {PluginState.UNINSTALLED, ImmutableList.of("a"), ImmutableMap.<Result, Set<String>>builder()
                        .put(Result.DISABLED, ImmutableSet.of("b", "c"))
                        .build()
                },

                // multiple plugins

                {PluginState.UNINSTALLED, ImmutableList.of("G", "D", "E"), ImmutableMap.<Result, Set<String>>builder()
                        .put(Result.DISABLED, ImmutableSet.of("C", "H", "F"))
                        .put(Result.BOUNCED, ImmutableSet.of("A", "B"))
                        .build()
                },

                {PluginState.UNINSTALLED, ImmutableList.of("b", "c"), ImmutableMap.<Result, Set<String>>builder()
                        .put(Result.DISABLED, ImmutableSet.of("a"))
                        .build()
                },

                {PluginState.UNINSTALLED, ImmutableList.of("mC", "mD"), ImmutableMap.<Result, Set<String>>builder()
                        .put(Result.DISABLED, ImmutableSet.of("mX", "mA", "mE"))
                        .put(Result.BOUNCED, ImmutableSet.of("mB"))
                        .build()
                },
        });
    }

    @Parameterized.Parameter
    public PluginState action;

    @Parameterized.Parameter(1)
    public List<String> target;

    @Parameterized.Parameter(2)
    public Map<Result, Set<String>> expectedResults;

    @Rule
    public MockitoRule mockito = MockitoJUnit.rule();

    @Mock
    private PluginPersistentStateStore pluginPersistentStateStore;
    @Mock
    private ModuleDescriptorFactory moduleDescriptorFactory;
    @Mock
    private PluginEventManager pluginEventManager;
    @Mock
    private PluginPersistentState pluginPersistentState;
    @Mock
    private PluginLoader pluginLoader;

    private DefaultPluginManager pluginManager;

    @Before
    public void setUp() throws Exception {
        super.setUp();

        when(pluginLoader.loadAllPlugins(any(ModuleDescriptorFactory.class))).thenReturn(allPlugins);
        when(pluginLoader.supportsRemoval()).thenReturn(true);

        pluginManager = new DefaultPluginManager(
                pluginPersistentStateStore,
                Arrays.<PluginLoader>asList(pluginLoader),
                moduleDescriptorFactory,
                pluginEventManager);
        when(pluginPersistentStateStore.load()).thenReturn(pluginPersistentState);
        when(pluginPersistentState.getPluginRestartState(any(String.class))).thenReturn(PluginRestartState.NONE);
        pluginManager.init();

        pluginManager.enablePlugins(getKeys(allPlugins).toArray(new String[allPlugins.size()]));
    }

    @Test
    public void testAllOtherPluginsNotTouched() throws Exception {
        final Set<Plugin> changed = new HashSet<>();

        run((p, result) -> changed.add(p));

        final Set<Plugin> allPlugins = new HashSet<>(pluginManager.getPlugins());
        allPlugins.removeAll(changed);
        target.stream().forEach(key -> allPlugins.remove(pluginManager.getPlugin(key)));

        allPlugins.stream().forEach(gp -> {
            PluginWithDeps p = (PluginWithDeps) gp;
            assertThat(p.getKey(), p.getTimesEnabled(), is(1));
            assertThat(p.getKey(), p.getTimesDisabled(), is(0));
        });
    }

    @Test
    public void testPluginsInDesiredStateAfterAction() throws Exception {
        run((p, result) -> assertThat("Plugin '" + p + "' is not " + result.getFinalState(), p.getPluginState(), is(result.getFinalState())));
    }

    @Test
    public void testBouncedDisabledThenEnabled() throws Exception {
        run((p, result) -> {
            if (result == Result.BOUNCED) {
                assertThat(p.getKey(), p.getTimesEnabled(), is(2));
                assertThat(p.getKey(), p.getTimesDisabled(), is(1));
            }
        });
    }

    @Test
    public void testNotTouchedNotBounced() throws Exception {
        run((p, result) -> {
            if (result == Result.UNTOUCHED) {
                assertThat(p.getKey(), p.getTimesEnabled(), is(1));
                assertThat(p.getKey(), p.getTimesDisabled(), is(0));
            }
        });
    }

    private void run(final AssertResult assertion) {
        switch (action) {
            case DISABLED:
                pluginManager.disablePlugin(target.get(0));
                break;
            case UNINSTALLED:
                if (target.size()==1) {
                    pluginManager.uninstall(pluginManager.getPlugin(target.get(0)));
                } else {
                    pluginManager.uninstallPlugins(target.stream().map(pluginManager::getPlugin).collect(Collectors.toList()));
                }
                break;
            default:
                fail("Unsupported action " + action);
        }

        for (final Result result : expectedResults.keySet()) {
            for (final String key : expectedResults.get(result)) {
                final Plugin p = pluginManager.getPlugin(key);
                assertThat("Plugin with key ='" + key + "' not found", p, is(notNullValue()));

                assertion.assertResult((PluginWithDeps) p, result);
            }
        }
    }

    private interface AssertResult {
        void assertResult(final PluginWithDeps p, final Result result);
    }
}
