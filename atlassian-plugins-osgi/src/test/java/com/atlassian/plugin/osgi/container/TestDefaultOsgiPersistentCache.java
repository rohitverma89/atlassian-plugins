package com.atlassian.plugin.osgi.container;

import com.atlassian.plugin.osgi.container.impl.DefaultOsgiPersistentCache;
import com.atlassian.plugin.test.PluginTestUtils;
import junit.framework.TestCase;
import org.apache.commons.io.FileUtils;

import java.io.File;
import java.io.IOException;

public class TestDefaultOsgiPersistentCache extends TestCase {
    private File tmpDir;

    @Override
    protected void setUp() throws Exception {
        super.setUp();
        tmpDir = PluginTestUtils.createTempDirectory(TestDefaultOsgiPersistentCache.class);
    }

    public void testRecordLastVersion() throws IOException {
        DefaultOsgiPersistentCache cache = new DefaultOsgiPersistentCache(tmpDir);
        File versionFile = new File(new File(tmpDir, "transformed-plugins"), "cache.key");
        cache.validate("1.0");
        assertTrue(versionFile.exists());
        String txt = FileUtils.readFileToString(versionFile);

        //"e8dc057d3346e56aed7cf252185dbe1fa6454411" == SHA1("1.0").
        assertEquals(String.valueOf("e8dc057d3346e56aed7cf252185dbe1fa6454411"), txt);
    }

    public void testCleanOnUpgrade() throws IOException {
        DefaultOsgiPersistentCache cache = new DefaultOsgiPersistentCache(tmpDir);
        File tmp = File.createTempFile("foo", ".txt", new File(tmpDir, "transformed-plugins"));
        cache.validate("1.0");
        assertTrue(tmp.exists());
        cache.validate("2.0");
        assertFalse(tmp.exists());
    }

    public void testNullVersion() throws IOException {
        DefaultOsgiPersistentCache cache = new DefaultOsgiPersistentCache(tmpDir);
        cache.validate(null);
        File tmp = File.createTempFile("foo", ".txt", new File(tmpDir, "transformed-plugins"));
        assertTrue(tmp.exists());
        cache.validate(null);
        assertTrue(tmp.exists());
    }

    @Override
    protected void tearDown() throws Exception {
        super.tearDown();
        FileUtils.cleanDirectory(tmpDir);
    }
}
