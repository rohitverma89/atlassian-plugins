package com.atlassian.plugin.osgi.util;

import com.atlassian.plugin.JarPluginArtifact;
import com.atlassian.plugin.PluginArtifact;
import com.atlassian.plugin.PluginParseException;
import com.atlassian.plugin.osgi.factory.OsgiPlugin;
import com.atlassian.plugin.osgi.factory.transform.StubHostComponentRegistration;
import com.atlassian.plugin.osgi.hostcomponents.HostComponentRegistration;
import com.atlassian.plugin.osgi.hostcomponents.impl.MockRegistration;
import com.atlassian.plugin.test.CapturedLogging;
import com.atlassian.plugin.test.PluginJarBuilder;
import com.google.common.collect.Sets;
import org.junit.Assert;
import org.junit.Rule;
import org.junit.Test;
import org.mockito.Mockito;
import org.osgi.framework.Bundle;
import org.osgi.framework.Constants;
import org.osgi.framework.Version;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Dictionary;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.jar.JarFile;
import java.util.jar.Manifest;

import static com.atlassian.plugin.test.CapturedLogging.didLogWarn;
import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.assertFalse;
import static junit.framework.Assert.assertTrue;
import static org.hamcrest.Matchers.notNullValue;
import static org.hamcrest.Matchers.nullValue;
import static org.hamcrest.core.Is.is;
import static org.hamcrest.core.IsEqual.equalTo;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class TestOsgiHeaderUtil {

    @Rule
    public CapturedLogging capturedLogging = new CapturedLogging(OsgiHeaderUtil.class);

    @Test(expected = NullPointerException.class)
    public void shouldAssertValueShouldNotBeNull() {
        Manifest manifest = new Manifest();
        OsgiHeaderUtil.getValidatedAttribute(manifest, "some-random-key");
    }

    @Test(expected = IllegalArgumentException.class)
    public void shouldAssertValueShouldNotBeEmpty() throws IOException {
        Manifest manifest = generateManifest();
        OsgiHeaderUtil.getValidatedAttribute(manifest, "Some-other");
    }

    @Test(expected = IllegalArgumentException.class)
    public void shouldRaiseAnExceptionWhenValueIsMissing() throws IOException {
        Manifest manifest = generateManifest();
        OsgiHeaderUtil.getNonEmptyAttribute(manifest, "Some-other");
    }

    @Test
    public void shouldGetValidatedAttribute() throws IOException {
        Manifest manifestFile = generateManifest();
        assertThat(OsgiHeaderUtil.getValidatedAttribute(manifestFile, "Import-Package"), equalTo("javax.swing"));
    }

    @Test
    public void shouldGetEmptyAttributeWithoutValidation() throws IOException {
        Manifest manifest = generateManifest();
        assertThat(OsgiHeaderUtil.getAttributeWithoutValidation(manifest, "Some-other"), is(""));
    }

    @Test
    public void shouldGetNullAttributeWithoutValidation() throws IOException {
        Manifest manifest = generateManifest();
        Assert.assertNull(OsgiHeaderUtil.getAttributeWithoutValidation(manifest, "some-random"));
    }

    private Manifest generateManifest() throws IOException {
        File bundle = new PluginJarBuilder("someplugin")
                .addResource("META-INF/MANIFEST.MF", "Manifest-Version: 1.0\n" +
                        "Import-Package: javax.swing\n" +
                        "Bundle-SymbolicName: my.foo.symbolicName\n" +
                        "Some-other: \n" +
                        "Bundle-Version: 1.0\n")
                .build();
        JarPluginArtifact jarPluginArtifact = new JarPluginArtifact(bundle);
        InputStream descriptorClassStream = jarPluginArtifact.getResourceAsStream("META-INF/MANIFEST.MF");
        return new Manifest(descriptorClassStream);
    }

    @Test
    public void testFindReferredPackages() throws IOException {
        Set<String> foundPackages = OsgiHeaderUtil.findReferredPackageNames(new ArrayList<HostComponentRegistration>() {{
            add(new StubHostComponentRegistration(OsgiHeaderUtil.class));
        }});

        assertTrue(foundPackages.contains(HostComponentRegistration.class.getPackage().getName()));
    }

    @Test
    public void testFindReferredPackagesMustNotReturnJavaPackages() throws IOException {
        List<HostComponentRegistration> regs = Arrays.<HostComponentRegistration>asList(new MockRegistration(Mockito.mock(DummyClass.class), DummyClass.class));

        Set<String> foundPackages = OsgiHeaderUtil.findReferredPackageNames(regs);

        assertFalse(foundPackages.contains("java.lang"));
    }

    @Test
    public void testFindReferredPackagesWithVersion() throws IOException {
        Map<String, String> foundPackages = OsgiHeaderUtil.findReferredPackageVersions(new ArrayList<HostComponentRegistration>() {{
            add(new StubHostComponentRegistration(OsgiHeaderUtil.class));
        }}, Collections.singletonMap(HostComponentRegistration.class.getPackage().getName(), "1.0.45"));

        assertTrue(foundPackages.containsKey(HostComponentRegistration.class.getPackage().getName()));
        assertEquals(foundPackages.get(HostComponentRegistration.class.getPackage().getName()), "1.0.45");
    }

    @Test
    public void testGetPluginKeyBundle() {
        Dictionary headers = new Hashtable();
        headers.put(Constants.BUNDLE_VERSION, "1.0");
        headers.put(Constants.BUNDLE_SYMBOLICNAME, "foo");

        Bundle bundle = mock(Bundle.class);
        when(bundle.getSymbolicName()).thenReturn("foo");
        when(bundle.getHeaders()).thenReturn(headers);

        assertEquals("foo-1.0", OsgiHeaderUtil.getPluginKey(bundle));

        headers.put(OsgiPlugin.ATLASSIAN_PLUGIN_KEY, "bar");
        assertEquals("bar", OsgiHeaderUtil.getPluginKey(bundle));
    }

    @Test
    public void testGetPluginKeyBundleWithDirectives() {
        Dictionary headers = new Hashtable();
        headers.put(Constants.BUNDLE_VERSION, "1.0");
        headers.put(Constants.BUNDLE_SYMBOLICNAME, "foo;singleton:=true");

        Bundle bundle = mock(Bundle.class);
        when(bundle.getSymbolicName()).thenReturn("foo;singleton:=true");
        when(bundle.getHeaders()).thenReturn(headers);

        assertThat("foo-1.0", equalTo(OsgiHeaderUtil.getPluginKey(bundle)));

        headers.put(OsgiPlugin.ATLASSIAN_PLUGIN_KEY, "bar");
        assertThat("bar", equalTo(OsgiHeaderUtil.getPluginKey(bundle)));
    }

    @Test
    public void testGetPluginKeyManifest() {
        Manifest mf = new Manifest();
        mf.getMainAttributes().putValue(Constants.BUNDLE_VERSION, "1.0");
        mf.getMainAttributes().putValue(Constants.BUNDLE_SYMBOLICNAME, "foo");

        assertEquals("foo-1.0", OsgiHeaderUtil.getPluginKey(mf));

        mf.getMainAttributes().putValue(OsgiPlugin.ATLASSIAN_PLUGIN_KEY, "bar");
        assertEquals("bar", OsgiHeaderUtil.getPluginKey(mf));
    }

    @Test
    public void testGetPluginKeyManifestWithDirectives() {
        Manifest mf = new Manifest();
        mf.getMainAttributes().putValue(Constants.BUNDLE_VERSION, "1.0");
        mf.getMainAttributes().putValue(Constants.BUNDLE_SYMBOLICNAME, "foo;singleton:=true");

        assertThat("foo-1.0", equalTo(OsgiHeaderUtil.getPluginKey(mf)));

        mf.getMainAttributes().putValue(OsgiPlugin.ATLASSIAN_PLUGIN_KEY, "bar");
        assertThat("bar", equalTo(OsgiHeaderUtil.getPluginKey(mf)));
    }

    @Test
    public void testGeneratePackageVersionString() {
        Map<String, String> input = new HashMap<String, String>();
        input.put("foo.bar", "1.2");
        input.put("foo.baz", Version.emptyVersion.toString());

        String output = OsgiHeaderUtil.generatePackageVersionString(input);

        Set<String> set = Sets.newHashSet(output.split("[,]"));

        assertTrue(set.contains("foo.bar;version=1.2"));
        assertTrue(set.contains("foo.baz"));
        assertEquals(2, set.size());
    }

    @Test
    public void getManifestGetsManifest() {
        final String manifestString = "Manifest-Version: 1.0\r\nSome-Header: some value\r\n";
        final PluginArtifact pluginArtifact = getPluginArtifactWithManifest(manifestString);
        final Manifest manifest = OsgiHeaderUtil.getManifest(pluginArtifact);
        assertThat(manifest, notNullValue());
        assertThat(manifest.getMainAttributes().getValue("Some-Header"), is("some value"));
    }

    @Test
    public void getManifestReturnsNullIfManifestIsMissing() {
        final PluginArtifact pluginArtifact = mock(PluginArtifact.class);
        // Mockito defaults return null, which is correct for missing resource
        final Manifest manifest = OsgiHeaderUtil.getManifest(pluginArtifact);
        assertThat(manifest, nullValue());
    }

    @Test
    public void getManifestReturnsNullIfGetResourceAsStreamFails() {
        final PluginArtifact pluginArtifact = mock(PluginArtifact.class);
        when(pluginArtifact.getResourceAsStream(JarFile.MANIFEST_NAME))
                .thenThrow(new PluginParseException("Intentional test fail"));
        final Manifest manifest = OsgiHeaderUtil.getManifest(pluginArtifact);
        assertThat(manifest, nullValue());
    }

    @Test
    public void getManifestReturnsNullIfResourceStreamThrowsWhenRead() throws Exception {
        final InputStream inputStream = new InputStream() {
            @Override
            public int read() throws IOException {
                throw new IOException("Intentional test fail");
            }
        };
        final PluginArtifact pluginArtifact = mock(PluginArtifact.class);
        when(pluginArtifact.getResourceAsStream(JarFile.MANIFEST_NAME)).thenReturn(inputStream);
        final Manifest manifest = OsgiHeaderUtil.getManifest(pluginArtifact);
        assertThat(manifest, nullValue());
    }

    @Test
    public void getManifestReturnsNullIfManifestIsCorrupt() {
        // Malformed, because you need a ' ' after the header ':'
        final String manifestString = "Manifest-Version: 1.0\r\nSome-Header:some value\r\n";
        final PluginArtifact pluginArtifact = getPluginArtifactWithManifest(manifestString);
        final Manifest manifest = OsgiHeaderUtil.getManifest(pluginArtifact);
        assertThat(manifest, nullValue());
    }

    @Test
    public void moveStarPackagesToEndComplex() {

        final Map<String, Map<String, String>> input = new HashMap<String, Map<String, String>>();
        input.put("javax.servlet*", new HashMap<String, String>());
        input.put("javax.servlet*~", new HashMap<String, String>());
        input.put("*", new HashMap<String, String>());
        input.put("*~", new HashMap<String, String>());
        input.put("javax.servlet", new HashMap<String, String>());
        input.put("javax.servlet~", new HashMap<String, String>());
        input.put("org.apache.lucene.search", new HashMap<String, String>());

        final Map<String, Map<String, String>> output = new HashMap<String, Map<String, String>>();
        output.put("javax.servlet", new HashMap<String, String>());
        output.put("javax.servlet~", new HashMap<String, String>());
        output.put("org.apache.lucene.search", new HashMap<String, String>());
        output.put("javax.servlet*", new HashMap<String, String>());
        output.put("javax.servlet*~", new HashMap<String, String>());
        output.put("*", new HashMap<String, String>());
        output.put("*~", new HashMap<String, String>());

        assertThat(OsgiHeaderUtil.moveStarPackageToEnd(input, "myplugin"), is(output));
    }

    @Test
    public void moveStarPackagesToEndEmpty() {
        final Map<String, Map<String, String>> input = new HashMap<String, Map<String, String>>();
        final Map<String, Map<String, String>> output = new HashMap<String, Map<String, String>>();
        assertThat(OsgiHeaderUtil.moveStarPackageToEnd(input, "myplugin"), is(output));
    }

    @Test
    public void stripDuplicatePackagesWithDuplicates() {

        final Map<String, Map<String, String>> input = new HashMap<String, Map<String, String>>();
        input.put("some.package", new HashMap<String, String>());
        input.put("some.package~", new HashMap<String, String>());
        input.put("some.other.package~", new HashMap<String, String>());

        final Map<String, Map<String, String>> output = new HashMap<String, Map<String, String>>();
        output.put("some.package", new HashMap<String, String>());

        assertThat(OsgiHeaderUtil.stripDuplicatePackages(input, "myplugin", "myaction"), is(output));

        assertThat(capturedLogging, didLogWarn("removing duplicate", "myplugin", "myaction", "some.package~"));
    }

    @Test
    public void stripDuplicatePackagesEmpty() {
        final Map<String, Map<String, String>> input = new HashMap<String, Map<String, String>>();
        final Map<String, Map<String, String>> output = new HashMap<String, Map<String, String>>();
        assertThat(OsgiHeaderUtil.stripDuplicatePackages(input, "myplugin", "myaction"), is(output));
    }

    private PluginArtifact getPluginArtifactWithManifest(final String manifestString) {
        final PluginArtifact pluginArtifact = mock(PluginArtifact.class);
        final ByteArrayInputStream manifestStream = new ByteArrayInputStream(manifestString.getBytes());
        when(pluginArtifact.getResourceAsStream(JarFile.MANIFEST_NAME)).thenReturn(manifestStream);
        return pluginArtifact;
    }

    private static interface DummyClass {
        void doSomething(Object obj);
    }

}
