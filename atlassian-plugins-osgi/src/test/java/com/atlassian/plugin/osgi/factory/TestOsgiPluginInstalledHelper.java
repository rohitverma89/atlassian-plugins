package com.atlassian.plugin.osgi.factory;

import com.atlassian.fugue.Pair;
import com.atlassian.plugin.module.ContainerAccessor;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableMap;
import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.mockito.Mockito;
import org.osgi.framework.Bundle;
import org.osgi.framework.BundleContext;
import org.osgi.framework.Constants;
import org.osgi.framework.wiring.BundleRequirement;
import org.osgi.framework.wiring.BundleRevision;
import org.osgi.framework.wiring.BundleRevisions;
import org.osgi.framework.wiring.BundleWire;
import org.osgi.framework.wiring.BundleWiring;
import org.osgi.service.packageadmin.PackageAdmin;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.beans.factory.support.BeanDefinitionBuilder;
import org.springframework.beans.factory.support.DefaultListableBeanFactory;
import org.springframework.context.support.GenericApplicationContext;

import java.util.ArrayList;
import java.util.Dictionary;
import java.util.Hashtable;
import java.util.List;
import java.util.Map;
import java.util.Set;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.arrayContaining;
import static org.hamcrest.Matchers.empty;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.notNullValue;
import static org.hamcrest.Matchers.nullValue;
import static org.mockito.Matchers.argThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class TestOsgiPluginInstalledHelper {
    @Rule
    public ExpectedException expectedException = ExpectedException.none();

    /**
     * Similar to Felix's (private) org.apache.felix.framework.BundleImpl
     */
    interface BundleImpl extends Bundle, BundleRevisions {
    }

    private BundleImpl bundle;
    private BundleContext bundleContext;
    private Dictionary<String, String> dict;
    private PackageAdmin packageAdmin;

    private OsgiPluginInstalledHelper helper;

    @Before
    public void setUp() {
        bundle = mock(BundleImpl.class);
        dict = new Hashtable<String, String>();
        dict.put(Constants.BUNDLE_DESCRIPTION, "desc");
        dict.put(Constants.BUNDLE_VERSION, "1.0");
        when(bundle.getHeaders()).thenReturn(dict);
        bundleContext = mock(BundleContext.class);
        when(bundle.getBundleContext()).thenReturn(bundleContext);

        packageAdmin = mock(PackageAdmin.class);

        helper = new OsgiPluginInstalledHelper(bundle, packageAdmin);
    }

    @After
    public void tearDown() {
        bundle = null;
        packageAdmin = null;
        helper = null;
        dict = null;
        bundleContext = null;
    }

    @Test
    public void canSetContainerAccessorToSpringGenericApplicationContext() {
        final DefaultListableBeanFactory beanFactory = new DefaultListableBeanFactory();
        final BeanDefinition beanDefinition = BeanDefinitionBuilder.rootBeanDefinition(ChildBean.class).getBeanDefinition();
        beanFactory.registerBeanDefinition("child", beanDefinition);

        when(bundle.getSymbolicName()).thenReturn("foo");
        GenericApplicationContext applicationContext = new GenericApplicationContext(beanFactory);
        applicationContext.refresh();
        helper.setPluginContainer(applicationContext);
        final SetterInjectedBean bean = new SetterInjectedBean();
        helper.getContainerAccessor().injectBean(bean);
        assertThat(bean.getChild(), notNullValue());
    }

    @Test
    public void canSetContainerAccessorToCustomContainerAccessor() {
        final ContainerAccessor containerAccessor = mock(ContainerAccessor.class);
        final String expectedBean = "foo";
        when(containerAccessor.createBean(String.class)).thenReturn(expectedBean);

        helper.setPluginContainer(containerAccessor);
        // We don't require the helper return our ContainerAccessor, we require that our accessor is used
        final String actualBean = helper.getContainerAccessor().createBean(String.class);
        assertThat(actualBean, is(expectedBean));
    }

    @Test
    public void getRequiredContainerAccessorReturnsContainerAccessor() {
        final ContainerAccessor containerAccessor = mock(ContainerAccessor.class);
        helper.setPluginContainer(containerAccessor);
        // We don't require our ContainerAccessor come back, but optional and required should return the same thing
        assertThat(helper.getRequiredContainerAccessor(), is(helper.getContainerAccessor()));
    }

    @Test
    public void beforeSetGetContainerAccessorIsNull() {
        assertThat(helper.getContainerAccessor(), nullValue());
    }

    @Test
    public void beforeSetGetRequiredContainerAccessorThrows() {
        final String bundleSymbolicName = "test-bundle-symbolicName";
        when(bundle.getSymbolicName()).thenReturn(bundleSymbolicName);
        expectedException.expect(IllegalStateException.class);
        expectedException.expectMessage(bundleSymbolicName);
        helper.getRequiredContainerAccessor();
    }

    @Test
    public void disableWithoutEnablingDoesNotThrow() {
        helper.onDisable();
    }

    @Test
    public void installedBundlesAreResolvedByGetRequiredPlugins() {
        Mockito.when(bundle.getState()).thenReturn(Bundle.INSTALLED);
        Mockito.when(bundle.getRevisions()).thenReturn(ImmutableList.<BundleRevision>of());
        when(packageAdmin.resolveBundles(argThat(arrayContaining(bundle)))).thenReturn(true);
        helper.getRequiredPlugins();
        verify(packageAdmin).resolveBundles(new Bundle[]{bundle});
    }


    private BundleImpl createBundleWithWiring(final Pair<String, String>... wires) {
        List<BundleWire> requiredWires = new ArrayList<>();

        for (Pair<String, String> wire : wires) {
            Bundle requiredBundle = mock(Bundle.class);
            Mockito.when(requiredBundle.getSymbolicName()).thenReturn(wire.left());
            Mockito.when(requiredBundle.getHeaders()).thenReturn(new Hashtable<String, String>());

            BundleWiring provider = mock(BundleWiring.class);
            Mockito.when(provider.getBundle()).thenReturn(requiredBundle);

            BundleRequirement requirement = mock(BundleRequirement.class);
            Map<String, String> directives;
            if ("mandatory".equals(wire.right())) {
                directives = ImmutableMap.of();
            } else {
                directives = ImmutableMap.of("resolution", wire.right());
            }
            Mockito.when(requirement.getDirectives()).thenReturn(directives);

            BundleWire requiredWire = mock(BundleWire.class);
            Mockito.when(requiredWire.getProviderWiring()).thenReturn(provider);
            Mockito.when(requiredWire.getRequirement()).thenReturn(requirement);

            requiredWires.add(requiredWire);
        }

        BundleWiring wiring = mock(BundleWiring.class);
        Mockito.when(wiring.getRequiredWires(null)).thenReturn(requiredWires);

        BundleRevision bundleRevision = mock(BundleRevision.class);
        List<BundleRevision> bundleRevisions = new ArrayList<>();
        bundleRevisions.add(bundleRevision);
        Mockito.when(bundleRevision.getWiring()).thenReturn(wiring);

        BundleImpl bundle = mock(BundleImpl.class);
        Mockito.when(bundle.getRevisions()).thenReturn(bundleRevisions);

        return bundle;
    }

    @Test
    public void getRequiredPluginsReturnsEmptySetIfBundleCannotBeResolved() {
        final String symbolicName = "test-bundle-sybolicName";
        when(bundle.getSymbolicName()).thenReturn(symbolicName);
        when(bundle.getState()).thenReturn(Bundle.INSTALLED);
        when(packageAdmin.resolveBundles(argThat(arrayContaining(bundle)))).thenReturn(false);
        final Set<String> requiredPlugins = helper.getRequiredPlugins();
        verify(packageAdmin).resolveBundles(new Bundle[]{bundle});
        assertThat(requiredPlugins, empty());
    }

    public static class ChildBean {
    }

    public static class SetterInjectedBean {
        private ChildBean child;

        public ChildBean getChild() {
            return child;
        }

        @SuppressWarnings("UnusedDeclaration")
        public void setChild(final ChildBean child) {
            this.child = child;
        }
    }
}
