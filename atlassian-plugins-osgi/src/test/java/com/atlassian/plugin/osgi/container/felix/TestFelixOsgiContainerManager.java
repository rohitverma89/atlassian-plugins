package com.atlassian.plugin.osgi.container.felix;

import com.atlassian.plugin.event.impl.DefaultPluginEventManager;
import com.atlassian.plugin.osgi.container.OsgiContainerException;
import com.atlassian.plugin.osgi.container.OsgiContainerManager;
import com.atlassian.plugin.osgi.container.OsgiPersistentCache;
import com.atlassian.plugin.osgi.container.impl.DefaultOsgiPersistentCache;
import com.atlassian.plugin.osgi.container.impl.DefaultPackageScannerConfiguration;
import com.atlassian.plugin.osgi.hostcomponents.HostComponentRegistration;
import com.atlassian.plugin.test.PluginJarBuilder;
import com.atlassian.plugin.test.PluginTestUtils;
import com.google.common.base.Charsets;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableMap;
import com.google.common.hash.Hashing;
import org.apache.commons.io.FileUtils;
import org.apache.felix.framework.Logger;
import org.apache.felix.framework.cache.BundleArchive;
import org.hamcrest.FeatureMatcher;
import org.hamcrest.Matcher;
import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.contrib.java.lang.system.RestoreSystemProperties;
import org.osgi.framework.Bundle;
import org.osgi.framework.BundleException;
import org.osgi.framework.Constants;
import org.osgi.framework.ServiceReference;
import org.osgi.service.packageadmin.PackageAdmin;
import org.osgi.util.tracker.ServiceTracker;
import org.osgi.util.tracker.ServiceTrackerCustomizer;

import java.io.File;
import java.io.FilenameFilter;
import java.io.IOException;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;

import static com.atlassian.plugin.ReferenceMode.PERMIT_REFERENCE;
import static com.atlassian.plugin.osgi.container.felix.FelixOsgiContainerManager.ATLASSIAN_BOOTDELEGATION;
import static com.atlassian.plugin.osgi.container.felix.FelixOsgiContainerManager.ATLASSIAN_BOOTDELEGATION_EXTRA;
import static com.atlassian.plugin.osgi.container.felix.FelixOsgiContainerManager.ATLASSIAN_DISABLE_REFERENCE_PROTOCOL;
import static com.atlassian.plugin.test.Matchers.fileNamed;
import static com.atlassian.plugin.util.PluginUtils.ATLASSIAN_PLUGINS_ENABLE_WAIT;
import static org.hamcrest.Matchers.allOf;
import static org.hamcrest.Matchers.arrayContaining;
import static org.hamcrest.Matchers.arrayWithSize;
import static org.hamcrest.Matchers.containsString;
import static org.hamcrest.Matchers.empty;
import static org.hamcrest.Matchers.emptyArray;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.hasItemInArray;
import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.not;
import static org.hamcrest.Matchers.notNullValue;
import static org.hamcrest.Matchers.nullValue;
import static org.hamcrest.Matchers.startsWith;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.fail;
import static org.osgi.framework.Constants.BUNDLE_NAME;
import static org.osgi.framework.Constants.BUNDLE_SYMBOLICNAME;

public class TestFelixOsgiContainerManager {
    @Rule
    public RestoreSystemProperties restoreSystemProperties = new RestoreSystemProperties(
            ATLASSIAN_BOOTDELEGATION,
            ATLASSIAN_BOOTDELEGATION_EXTRA,
            ATLASSIAN_DISABLE_REFERENCE_PROTOCOL,
            ATLASSIAN_PLUGINS_ENABLE_WAIT);

    private File tmpdir;
    private OsgiPersistentCache osgiPersistentCache;
    private FelixOsgiContainerManager felix;
    private URL frameworkBundlesUrl = getClass().getResource("/nothing.zip");

    @Before
    public void setUp() throws Exception {
        tmpdir = PluginTestUtils.createTempDirectory(TestFelixOsgiContainerManager.class);
        osgiPersistentCache = new DefaultOsgiPersistentCache(tmpdir);
        felix = new FelixOsgiContainerManager(
                frameworkBundlesUrl,
                osgiPersistentCache,
                new DefaultPackageScannerConfiguration(),
                null,
                new DefaultPluginEventManager());
    }

    @After
    public void tearDown() throws Exception {
        if (felix != null && felix.isRunning()) {
            for (final Bundle bundle : felix.getBundles()) {
                try {
                    bundle.uninstall();
                } catch (final BundleException ignored) {
                }
            }
        }
        if (felix != null) {
            felix.stop();
            felix.clearExportCache(); // prevent export cache from being reused across tests
        }
        felix = null;
        osgiPersistentCache = null;
        tmpdir = null;
    }

    @Test
    public void testDetectXercesOverride() {
        felix.detectXercesOverride("foo.bar,baz.jim");
        felix.detectXercesOverride("foo.bar,org.apache.xerces.util;version=\"1.0\",baz.jim");
        felix.detectXercesOverride("foo.bar,org.apache.xerces.util;version=\"1.0\"");
        felix.detectXercesOverride("foo.bar,repackaged.org.apache.xerces.util,bar.baz");


        try {
            felix.detectXercesOverride("foo.bar,org.apache.xerces.util");
            fail("Should fail validation");
        } catch (final OsgiContainerException ex) {
            // should fail
        }

        try {
            felix.detectXercesOverride("org.apache.xerces.util");
            fail("Should fail validation");
        } catch (final OsgiContainerException ex) {
            // should fail
        }

        try {
            felix.detectXercesOverride("org.apache.xerces.util,bar.baz");
            fail("Should fail validation");
        } catch (final OsgiContainerException ex) {
            // should fail
        }

    }

    @Test
    public void testStartStop() {
        final FilenameFilter filter = new FilenameFilter() {
            public boolean accept(final File file, final String s) {
                return s.startsWith("felix");
            }
        };
        final int filesNamedFelix = tmpdir.listFiles(filter).length;
        felix.start();
        assertThat(felix.isRunning(), is(true));
        assertThat(felix.getBundles(), arrayWithSize(1));
        felix.stop();
        assertThat(tmpdir.listFiles(filter), arrayWithSize(filesNamedFelix));
    }

    @Test
    public void testInstallBundle() throws URISyntaxException {
        felix.start();
        assertThat(felix.getBundles(), arrayWithSize(1));
        final File jar = new File(getClass().getResource("/myapp-1.0.jar").toURI());
        felix.installBundle(jar);
        assertThat(felix.getBundles(), arrayWithSize(2));
        assertThat(felix.getBundles()[1], not(bundleThatIsReference()));
        // There should a single jar in the felix cache now
        final String[] justJar = {"jar"};
        final Collection<File> cachedJars = FileUtils.listFiles(osgiPersistentCache.getOsgiBundleCache(), justJar, true);
        assertThat(cachedJars, hasSize(1));
    }

    @Test
    public void testInstallBundleAllowReference() throws URISyntaxException {
        felix.start();
        assertThat(felix.getBundles(), arrayWithSize(1));
        final File jar = new File(getClass().getResource("/myapp-1.0.jar").toURI());
        felix.installBundle(jar, PERMIT_REFERENCE);
        assertThat(felix.getBundles(), arrayWithSize(2));
        assertThat(felix.getBundles()[1], bundleThatIsReference());
        // There should be no jars in the felix cache
        final String[] justJar = {"jar"};
        final Collection<File> cachedJars = FileUtils.listFiles(osgiPersistentCache.getOsgiBundleCache(), justJar, true);
        assertThat(cachedJars, empty());
    }

    @Test
    public void testAllowsReferenceInstallDefaultForwardsRequest() throws URISyntaxException {
        felix.start();
        assertThat(felix.getBundles(), arrayWithSize(1));
        final File jar = new File(getClass().getResource("/myapp-1.0.jar").toURI());
        OsgiContainerManager.AllowsReferenceInstall.Default.installBundle(felix, jar, true);
        assertThat(felix.getBundles(), arrayWithSize(2));
        assertThat(felix.getBundles()[1], bundleThatIsReference());
    }

    @Test
    public void testInstallFileBundleWithReferenceProtocolDisabled() throws URISyntaxException {
        felix.start();
        assertThat(felix.getBundles(), arrayWithSize(1));
        System.setProperty(ATLASSIAN_DISABLE_REFERENCE_PROTOCOL, "true");
        final File jar = new File(getClass().getResource("/myapp-1.0.jar").toURI());
        felix.installBundle(jar, true);
        assertThat(felix.getBundles(), arrayWithSize(2));
        assertThat(felix.getBundles()[1], not(bundleThatIsReference()));
    }

    @Test
    public void checkDefaultBootDelegation() throws Exception {
        felix.start();
        final Bundle clientBundle = installBootDelegationBundles();
        try {
            // junit is not boot delegated by default
            clientBundle.loadClass("my.client.ClientClass").newInstance();
            fail("Expected exception: NoClassDefFoundError");
        } catch (final NoClassDefFoundError expected) {
            assertThat(expected.getMessage(), containsString("junit/framework/TestCase"));
        }
        // but com.sun.xml.internal.bind.v2 is
        clientBundle.loadClass("my.client.OtherClientClass").newInstance();
        felix.stop();
    }

    @Test
    public void checkReplacingBootDelegation() throws Exception {
        // This system property replaces the boot delegation
        System.setProperty(ATLASSIAN_BOOTDELEGATION, "junit.framework,junit.framework.*");
        felix.start();
        // The replacement exposed the JUnit TestCase class from the parent classloader to the bundle ...
        final Bundle clientBundle = installBootDelegationBundles();
        clientBundle.loadClass("my.client.ClientClass").newInstance();
        // ... but does not include the previously default com.sun.xml.internal.bind.v2...
        try {
            clientBundle.loadClass("my.client.OtherClientClass").newInstance();
            fail("Expected exception: NoClassDefFoundError");
        } catch (final NoClassDefFoundError expected) {
            assertThat(expected.getMessage(), containsString("com/sun/xml/internal/bind/v2/ContextFactory"));
        }
        felix.stop();
    }

    @Test
    public void checkExtraBootDelegation() throws Exception {
        // This system property augments the boot delegation
        System.setProperty(ATLASSIAN_BOOTDELEGATION_EXTRA, "junit.framework,junit.framework.*");
        felix.start();
        // We've now exposed the JUnit TestCase class from the parent classloader to the bundle ...
        final Bundle clientBundle = installBootDelegationBundles();
        clientBundle.loadClass("my.client.ClientClass").newInstance();
        // ... and not hidden the previously default com.sun.xml.internal.bind.v2...
        clientBundle.loadClass("my.client.OtherClientClass").newInstance();
        felix.stop();
    }

    private Bundle installBootDelegationBundles() throws Exception {
        // Server class extends JUnit TestCase class, which is not available to the bundle
        final File pluginServer = new PluginJarBuilder("plugin")
                .addResource("META-INF/MANIFEST.MF", "Manifest-Version: 1.0\n" +
                        "Bundle-Version: 1.0\n" +
                        "Bundle-SymbolicName: my.server\n" +
                        "Bundle-ManifestVersion: 2\n" +
                        "Export-Package: my.server\n")
                .addJava("my.server.ServerClass", "package my.server; public class ServerClass extends junit.framework.TestCase {}")
                .addJava("my.server.OtherServerClass", "package my.server; public class OtherServerClass " +
                        "extends com.sun.xml.internal.bind.v2.ContextFactory " +
                        "{}")
                .build();

        // Client is necessary to load the server class in a Felix ContentClassLoader, to avoid the hack in Felix's
        // R4SearchPolicyCore (approx. line 591) which will use parent delegation if a class cannot be found
        // and the calling classloader is not a ContentClassLoader.
        final File pluginClient = new PluginJarBuilder("plugin")
                .addResource("META-INF/MANIFEST.MF", "Manifest-Version: 1.0\n" +
                        "Bundle-Version: 1.0\n" +
                        "Bundle-SymbolicName: my.client\n" +
                        "Bundle-ManifestVersion: 2\n" +
                        "Import-Package: my.server\n")
                .addJava("my.client.ClientClass", "package my.client; public class ClientClass {" +
                        "public ClientClass() throws ClassNotFoundException {" +
                        "getClass().getClassLoader().loadClass(\"my.server.ServerClass\");" +
                        "}}")
                .addJava("my.client.OtherClientClass", "package my.client; public class OtherClientClass {" +
                        "public OtherClientClass() throws ClassNotFoundException {" +
                        "getClass().getClassLoader().loadClass(\"my.server.OtherServerClass\");" +
                        "}}")
                .build();

        final Bundle serverBundle = felix.installBundle(pluginServer);
        serverBundle.start();
        final Bundle clientBundle = felix.installBundle(pluginClient);
        clientBundle.start();
        return clientBundle;
    }


    @Test
    public void testInstallBundleTwice() throws URISyntaxException, IOException, BundleException {
        final File plugin = new PluginJarBuilder("plugin")
                .addResource("META-INF/MANIFEST.MF", "Manifest-Version: 1.0\n" +
                        "Import-Package: javax.swing\n" +
                        "Bundle-Version: 1.0\n" +
                        "Bundle-SymbolicName: my.foo.symbolicName\n" +
                        "Bundle-ManifestVersion: 2\n")
                .addResource("foo.txt", "foo")
                .build();

        final File pluginUpdate = new PluginJarBuilder("plugin")
                .addResource("META-INF/MANIFEST.MF", "Manifest-Version: 1.0\n" +
                        "Import-Package: javax.swing\n" +
                        "Bundle-Version: 1.0\n" +
                        "Bundle-SymbolicName: my.foo.symbolicName\n" +
                        "Bundle-ManifestVersion: 2\n")
                .addResource("bar.txt", "bar")
                .build();

        felix.start();
        assertThat(felix.getBundles(), arrayWithSize(1));
        final Bundle bundle = felix.installBundle(plugin);
        assertThat(felix.getBundles(), arrayWithSize(2));
        assertThat(bundle, bundleWithSymbolicName("my.foo.symbolicName"));
        assertThat(bundle.getHeaders().get(Constants.BUNDLE_VERSION), is((Object) "1.0"));
        assertThat(bundle, bundleWithState(Bundle.INSTALLED));
        assertThat(bundle.getResource("foo.txt"), notNullValue());
        assertThat(bundle.getResource("bar.txt"), nullValue());
        bundle.start();
        assertThat(bundle, bundleWithState(Bundle.ACTIVE));
        final Bundle bundleUpdate = felix.installBundle(pluginUpdate);
        assertThat(felix.getBundles(), arrayWithSize(2));
        assertThat(bundleUpdate, bundleWithState(Bundle.INSTALLED));
        bundleUpdate.start();
        assertThat(bundleUpdate, bundleWithState(Bundle.ACTIVE));
        assertThat(bundleUpdate.getResource("foo.txt"), nullValue());
        assertThat(bundleUpdate.getResource("bar.txt"), notNullValue());
    }

    @Test
    public void testInstallBundleTwiceDifferentSymbolicNames() throws URISyntaxException, IOException, BundleException {
        final File plugin = new PluginJarBuilder("plugin")
                .addResource("META-INF/MANIFEST.MF", "Manifest-Version: 1.0\n" +
                        "Import-Package: javax.swing\n" +
                        "Bundle-Version: 1.0\n" +
                        "Bundle-SymbolicName: my.foo\n" +
                        "Atlassian-Plugin-Key: my.foo.symbolicName\n" +
                        "Bundle-ManifestVersion: 2\n")
                .addResource("foo.txt", "foo")
                .build();

        final File pluginUpdate = new PluginJarBuilder("plugin")
                .addResource("META-INF/MANIFEST.MF", "Manifest-Version: 1.0\n" +
                        "Import-Package: javax.swing\n" +
                        "Bundle-Version: 1.0\n" +
                        "Atlassian-Plugin-Key: my.foo.symbolicName\n" +
                        "Bundle-SymbolicName: my.bar\n" +
                        "Bundle-ManifestVersion: 2\n")
                .addResource("bar.txt", "bar")
                .build();

        felix.start();
        assertThat(felix.getBundles(), arrayWithSize(1));
        final Bundle bundle = felix.installBundle(plugin);
        assertThat(felix.getBundles(), arrayWithSize(2));
        assertThat(bundle.getHeaders().get(Constants.BUNDLE_VERSION), is((Object) "1.0"));
        assertThat(bundle, bundleWithState(Bundle.INSTALLED));
        assertThat(bundle.getResource("foo.txt"), notNullValue());
        assertThat(bundle.getResource("bar.txt"), nullValue());
        bundle.start();
        assertThat(bundle, bundleWithState(Bundle.ACTIVE));
        final Bundle bundleUpdate = felix.installBundle(pluginUpdate);
        assertThat(felix.getBundles(), arrayWithSize(2));
        assertThat(bundleUpdate, bundleWithState(Bundle.INSTALLED));
        bundleUpdate.start();
        assertThat(bundleUpdate, bundleWithState(Bundle.ACTIVE));
        assertThat(bundleUpdate.getResource("foo.txt"), nullValue());
        assertThat(bundleUpdate.getResource("bar.txt"), notNullValue());
    }

    @Test
    public void testInstallFailure() throws Exception {
        final File plugin = new PluginJarBuilder("plugin")
                .addResource("META-INF/MANIFEST.MF", "Manifest-Version: 1.0\n" +
                        "Bundle-Version: 1.0\n" +
                        "Import-Package: foo.missing.package\n" +
                        "Bundle-SymbolicName: my.foo.symbolicName\n" +
                        "Bundle-ManifestVersion: 2\n")
                .build();
        felix.start();

        final Bundle bundle = felix.installBundle(plugin);
        try {
            bundle.loadClass("foo.bar");
            fail("Should have thrown exception");
        } catch (final ClassNotFoundException ex) {
            // no worries
        }
    }

    @Test
    public void testServiceTrackerIsClosed() throws URISyntaxException, IOException, BundleException {
        felix.start();
        // PackageAdmin is an example of service which is loaded by default in Felix
        final ServiceTracker tracker = felix.getServiceTracker(PackageAdmin.class.getCanonicalName());
        Object[] trackedServices = tracker.getServices();
        assertThat(trackedServices, notNullValue());

        tracker.close();

        trackedServices = tracker.getServices();
        assertThat(trackedServices, nullValue());
    }

    @Test
    public void testServiceTrackerActuallyTracksStuff() throws Exception {
        startFelixWithListAsOSGiService();

        final ServiceTracker tracker = felix.getServiceTracker(List.class.getName());
        final Object[] trackedServices = tracker.getServices();

        // Combining these into a single match is nontrivial on account of needing to manage the types.
        assertThat(trackedServices, arrayWithSize(1));
        assertThat(trackedServices[0], is((Object) ImmutableList.of("blah")));
    }

    @Test
    public void testServiceTrackerCutomizerIsInPlace() throws Exception {
        startFelixWithListAsOSGiService();
        final CountDownLatch latch = new CountDownLatch(1);

        felix.getServiceTracker(List.class.getName(), new ServiceTrackerCustomizer() {
            public Object addingService(final ServiceReference reference) {
                latch.countDown();
                return reference;
            }

            public void modifiedService(final ServiceReference reference, final Object service) {
            }

            public void removedService(final ServiceReference reference, final Object service) {
            }
        });
        assertThat(latch.await(10, TimeUnit.SECONDS), is(true));
    }

    private void startFelixWithListAsOSGiService() throws Exception {
        final File plugin = new PluginJarBuilder("plugin")
                .addResource("META-INF/MANIFEST.MF", "Manifest-Version: 1.0\n" +
                        "Bundle-Version: 1.0\n" +
                        "Atlassian-Plugin-Key: my.foo.symbolicName\n" +
                        "Bundle-Name: my.bar\n" +
                        "Bundle-SymbolicName: my.bar\n" +
                        "Bundle-ManifestVersion: 2\n" +
                        "Bundle-Activator: my.MyActivator\n" +
                        "Import-Package: org.osgi.framework\n")
                .addJava("my.MyActivator", "package my;" +
                        "import org.osgi.framework.ServiceRegistration;\n" +
                        "import org.osgi.framework.BundleActivator;\n" +
                        "import org.osgi.framework.BundleContext;\n" +
                        "import java.util.*;\n" +
                        "public class MyActivator implements BundleActivator {\n" +
                        "    private ServiceRegistration registration;\n" +
                        "    public void start(BundleContext context) throws Exception\n" +
                        "    {\n" +
                        "        context.registerService(List.class.getName(), Arrays.asList(new Object[]{ \"blah\" }), new Properties());\n" +
                        "    }\n" +
                        "    public void stop(BundleContext context) throws Exception {}\n" +
                        "}")
                .build();
        felix.start();
        final Bundle bundle = felix.installBundle(plugin);
        bundle.start();
    }

    @Test
    public void testRuntimeEnvironment() {
        final int timeout = 3883;
        setPluginTimeout(timeout);
        final String expectedRuntimeEnvironment = String.format(
                "java.version=%s,plugin.enable.timeout=%d",
                System.getProperty("java.version"), timeout);

        assertThat(felix.getRuntimeEnvironment(), is(expectedRuntimeEnvironment));
    }

    @Test
    public void testRuntimeEnvironmentIntegration() throws Exception {
        final int timeout = 678;
        setPluginTimeout(timeout);

        startFelixWithListAsOSGiService();

        final File versionFile = new File(new File(tmpdir, "transformed-plugins"), "cache.key");
        assertThat(versionFile.exists(), is(true));
        final String txt = FileUtils.readFileToString(versionFile);

        final ExportsBuilder eBuilder = new ExportsBuilder();
        final String systemExports = eBuilder.getExports(new ArrayList<HostComponentRegistration>(),
                new DefaultPackageScannerConfiguration());

        // cache key should include the current JVM version
        final String expectedKey = String.format(
                "java.version=%s,plugin.enable.timeout=%d,%s",
                System.getProperty("java.version"),
                timeout,
                systemExports);
        assertThat(txt, is(Hashing.sha1().hashString(expectedKey, Charsets.UTF_8).toString()));
    }

    @Test
    public void testFrameworkBundlesUnzipped() {
        // Check that nothing.zip, at frameworkBundlesUrl is unpacked when the framework is started
        // Should start empty or the test is broken
        assertThat(osgiPersistentCache.getFrameworkBundleCache().listFiles(), emptyArray());
        felix.start();
        assertThat(osgiPersistentCache.getFrameworkBundleCache().listFiles(), arrayContaining(fileNamed("nothing.txt")));
    }

    @Test
    public void testFrameworkBundleDirectorySkipsUnzip() throws Exception {
        final File frameworkBundles = new File(tmpdir, "provided-framework-bundles");
        FileUtils.forceMkdir(frameworkBundles);
        final String symbolicName = "my.foo.frameworkbundle";
        final Map<String, String> manifest = ImmutableMap.<String, String>builder()
                .put("Manifest-Version", "1.0")
                .put(BUNDLE_NAME, "Framework Bundle")
                .put(BUNDLE_SYMBOLICNAME, symbolicName)
                .build();
        new PluginJarBuilder("framework-bundle")
                .manifest(manifest)
                .build(frameworkBundles);
        felix = new FelixOsgiContainerManager(
                frameworkBundles,
                osgiPersistentCache,
                new DefaultPackageScannerConfiguration(),
                null,
                new DefaultPluginEventManager());

        felix.start();
        // Even after start, the framework bundle cache should be empty
        assertThat(osgiPersistentCache.getFrameworkBundleCache().listFiles(), emptyArray());
        // And we should have found the bundle we put in the provided directory
        assertThat(felix.getBundles(), hasItemInArray(bundleWithSymbolicName(symbolicName)));
    }

    @Test
    public void testFrameworkBundleDirectoryDoesNotExistThrowsNiceException() throws Exception {
        final File frameworkBundles = new File(tmpdir, "framework-bundles-dir-does-not-exist");
        felix = new FelixOsgiContainerManager(
                frameworkBundles,
                osgiPersistentCache,
                new DefaultPackageScannerConfiguration(),
                null,
                new DefaultPluginEventManager());

        TestFelixLogger logger = new TestFelixLogger();
        felix.setFelixLogger(logger);

        assertThat(frameworkBundles.exists(), is(false));
        felix.start();

        assertThat(logger.getExceptions(), hasSize(1));
        //noinspection ThrowableResultOfMethodCallIgnored
        Throwable exception = logger.getExceptions().get(0);
        Matcher<String> matchesException = allOf(containsString("BundleException"), containsString(frameworkBundles.toString()));
        assertThat(exception.toString(), matchesException);
    }

    private void setPluginTimeout(final int timeout) {
        System.setProperty(ATLASSIAN_PLUGINS_ENABLE_WAIT, String.valueOf(timeout));
    }

    /**
     * Matches a Bundle whose location indicates that it is a reference bundle.
     *
     * @return a matcher for bundles whose location is a reference: location.
     */
    private static Matcher<Bundle> bundleThatIsReference() {
        return bundleWithLocation(startsWith(BundleArchive.REFERENCE_PROTOCOL));
    }

    /**
     * Matches a Bundle whose location indicates that it is a reference bundle.
     *
     * @return a matcher for bundles whose location is a reference: location.
     */
    private static Matcher<Bundle> bundleWithLocation(final Matcher<String> locationMatcher) {
        return new FeatureMatcher<Bundle, String>(locationMatcher, "bundle with location", "bundle location") {
            @Override
            protected String featureValueOf(final Bundle bundle) {
                return bundle.getLocation();
            }
        };
    }

    /**
     * Matches a Bundle whose symbolic name is as given.
     *
     * @param symbolicName the expected symbolic name for the bundle.
     * @return a matcher for bundles whose symbolic name is {@code symbolicName}.
     */
    private static Matcher<Bundle> bundleWithSymbolicName(final String symbolicName) {
        return new FeatureMatcher<Bundle, String>(equalTo(symbolicName), "bundle with symbolic name", "symbolic name") {
            @Override
            protected String featureValueOf(final Bundle bundle) {
                return bundle.getSymbolicName();
            }
        };
    }

    /**
     * Matches a Bundle whose state is as given.
     *
     * @param bundleState the expected state for the bundle.
     * @return a matcher for bundles whose state is {@code bundleState}.
     */
    private static Matcher<Bundle> bundleWithState(final Integer bundleState) {
        return new FeatureMatcher<Bundle, Integer>(equalTo(bundleState), "a bundle with state", "bundle state") {
            @Override
            protected Integer featureValueOf(final Bundle bundle) {
                return bundle.getState();
            }
        };
    }

    private static class TestFelixLogger extends Logger {

        private final List<Throwable> exceptions = new ArrayList<>();

        @Override
        protected void doLog(Bundle bundle, ServiceReference sr, int level, String msg, Throwable throwable) {
            super.doLog(bundle, sr, level, msg, throwable);
            exceptions.add(throwable);
        }

        public List<Throwable> getExceptions() {
            return exceptions;
        }
    }
}
