package com.atlassian.plugin.osgi.spring;

import com.atlassian.plugin.JarPluginArtifact;
import com.atlassian.plugin.osgi.PluginInContainerTestBase;
import com.atlassian.plugin.test.PluginJarBuilder;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

/**
 * When an optional OSGi service is unavailable at runtime, Spring DM's proxy class will throw a specific exception.
 * The exception is different in OSGi Blueprint. This test is to demonstrate a plugin that relies on the Spring DM
 * behaviour so we can consider other approaches.
 * <ul>
 * <li>Catch <code>RuntimeException</code> and check for both class names
 * <li>Fail; don't try to cope with missing services
 * <li>Rewrite the bytecode for the catch at runtime
 * </li>
 */
public class TestServiceUnavailable extends PluginInContainerTestBase {
    /* Spring definition of a bean that takes an optional reference to an OSGi service */
    static final String beans = "<beans xmlns=\"http://www.springframework.org/schema/beans\"\n" +
            "             xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\"\n" +
            "             xmlns:osgi=\"http://www.eclipse.org/gemini/blueprint/schema/blueprint\"\n" +
            "             xsi:schemaLocation=\"" +
            "                 http://www.eclipse.org/gemini/blueprint/schema/blueprint http://www.eclipse.org/gemini/blueprint/schema/blueprint/gemini-blueprint.xsd\n" +
            "                 http://www.springframework.org/schema/beans http://www.springframework.org/schema/beans/spring-beans-2.5.xsd\">\n" +
            "\n" +
            " <osgi:reference id='a' interface='test.X' cardinality='0..1' timeout='1'/>" +
            " <bean class='test.FooConsumer'>" +
            "  <constructor-arg ref='a' />" +
            " </bean>" +

            "</beans>\n";

    /* Communicate with the plugin through a static field */
    public static String mutable = null;

    @Test
    public void testUnavailableServicesCanBeDetected() throws Exception {
        TestServiceUnavailable.mutable = null;

        initPluginManager();

        PluginJarBuilder plugin = new PluginJarBuilder("testServiceUnavailable")
                .addFormattedResource("atlassian-plugin.xml",
                        "<atlassian-plugin name='Test' key='test.serviceunavailable.plugin' pluginsVersion='2'>",
                        "    <plugin-info>",
                        "        <version>1.0</version>",
                        "    </plugin-info>",
                        "</atlassian-plugin>")

                .addFormattedJava("test.X",
                        "package test;",
                        "public interface X {}"
                )

                .addFormattedJava("test.FooConsumer",
                        "package test;",
                        "import com.atlassian.plugin.osgi.spring.TestServiceUnavailable;",
                        /* A constructor that tries to use an OSGi service stub and cope with it being unavailable */
                        "public class FooConsumer {",
                        "    FooConsumer (Object foo)",
                        "    {",
                        "        TestServiceUnavailable.mutable = \"invoked\";",
                        "        try",
                        "        {",
                        "            foo.toString();",
                        "            TestServiceUnavailable.mutable = \"succeeded\";",
                        "        }",
                        "        catch (RuntimeException e)",
                        "        {",
                        "            if (e.getClass().getSimpleName().equals(\"ServiceUnavailableException\"))",
                        "            {",
                        "                TestServiceUnavailable.mutable = \"caught\";",
                        "            }",
                        "            else",
                        "            {",
                        "                throw e;",
                        "            }",
                        "        }",
                        "        catch (Exception e)",
                        "        {",
                        "            TestServiceUnavailable.mutable = \"unexpected - \" + e.getClass().getName();",
                        "        }",
                        "    }",
                        "}"
                )

                .addFormattedResource("META-INF/spring/beans.xml", beans);

        plugin.build(pluginsDir);

        pluginController.installPlugin(new JarPluginArtifact(plugin.build()));

        osgiContainerManager.stop();

        assertEquals("The specific ServiceUnavailableException should be caught",
                "caught", mutable);
    }
}
