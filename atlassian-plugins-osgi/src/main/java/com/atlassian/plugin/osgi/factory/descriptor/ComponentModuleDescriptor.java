package com.atlassian.plugin.osgi.factory.descriptor;

import com.atlassian.plugin.Permissions;
import com.atlassian.plugin.Plugin;
import com.atlassian.plugin.PluginParseException;
import com.atlassian.plugin.RequirePermission;
import com.atlassian.plugin.descriptors.AbstractModuleDescriptor;
import com.atlassian.plugin.descriptors.CannotDisable;
import com.atlassian.plugin.module.ModuleFactory;
import com.atlassian.plugin.osgi.module.BeanPrefixModuleFactory;
import org.dom4j.Element;

import javax.annotation.Nonnull;

/**
 * Module descriptor for components.  Shouldn't be directly used outside providing read-only information.
 *
 * @since 2.2.0
 */
@CannotDisable
@RequirePermission(Permissions.EXECUTE_JAVA)
public class ComponentModuleDescriptor extends AbstractModuleDescriptor<Object> {
    public ComponentModuleDescriptor() {
        super(ModuleFactory.LEGACY_MODULE_FACTORY);
    }

    @Override
    public void init(@Nonnull Plugin plugin, @Nonnull Element element) throws PluginParseException {
        super.init(plugin, element);
        checkPermissions();
    }

    @Override
    protected void loadClass(Plugin plugin, String clazz) throws PluginParseException {
        try {
            // this should have been done once by the plugin container so should never throw exception.
            this.moduleClass = plugin.loadClass(clazz, null);
        } catch (ClassNotFoundException e) {
            throw new PluginParseException("cannot load component class", e);
        }
    }

    @Override
    public Object getModule() {
        return new BeanPrefixModuleFactory().createModule(getKey(), this);
    }

    /**
     * @return Module Class Name
     * @since 2.3.0
     * @deprecated - BEWARE that this is a temporary method that will not exist for long. Deprecated since 2.3.0
     */
    public String getModuleClassName() {
        return moduleClassName;
    }
}
