package com.atlassian.plugin.osgi.container;

import com.atlassian.plugin.ReferenceMode;
import com.atlassian.plugin.osgi.hostcomponents.HostComponentRegistration;
import org.osgi.framework.Bundle;
import org.osgi.framework.BundleListener;
import org.osgi.framework.ServiceReference;
import org.osgi.util.tracker.ServiceTracker;
import org.osgi.util.tracker.ServiceTrackerCustomizer;

import java.io.File;
import java.util.List;

import static com.atlassian.plugin.ReferenceMode.FORBID_REFERENCE;
import static com.atlassian.plugin.ReferenceMode.PERMIT_REFERENCE;

/**
 * Manages the OSGi container and handles any interactions with it
 */
public interface OsgiContainerManager {
    /**
     * Starts the OSGi container
     *
     * @throws OsgiContainerException If the container cannot be started
     */
    void start() throws OsgiContainerException;

    /**
     * Stops the OSGi container
     *
     * @throws OsgiContainerException If the container cannot be stopped
     */
    void stop() throws OsgiContainerException;

    /**
     * Installs a bundle into a running OSGI container
     *
     * @param file The bundle file to install
     * @return The created bundle
     * @throws OsgiContainerException If the bundle cannot be loaded
     * @deprecated since 4.0.0, to be removed in 5.0.0: Use {@link #installBundle(File, ReferenceMode)} and explicitly specify
     * reference semantics.
     */
    @Deprecated
    Bundle installBundle(File file) throws OsgiContainerException;

    /**
     * Installs a bundle into a running OSGI container.
     *
     * @param file          The bundle file to install.
     * @param referenceMode specifies whether the container may install a reference to, rather than copy, the bundle file.
     * @return The installed bundle.
     * @throws OsgiContainerException If the bundle cannot be loaded
     * @since 4.0.0
     */
    Bundle installBundle(File file, ReferenceMode referenceMode) throws OsgiContainerException;

    /**
     * @return If the container is running or not
     */
    boolean isRunning();

    /**
     * Gets a list of installed bundles
     *
     * @return An array of bundles
     */
    Bundle[] getBundles();

    /**
     * Gets a list of service references
     *
     * @return An array of service references
     */
    ServiceReference[] getRegisteredServices();

    /**
     * Gets a list of host component registrations
     *
     * @return A list of host component registrations
     */
    List<HostComponentRegistration> getHostComponentRegistrations();

    /**
     * Gets a service tracker to follow a service registered under a certain interface.  Will return a new
     * {@link ServiceTracker} instance for every call, so don't call more than necessary.  Any provided
     * {@link ServiceTracker} instances will be opened before returning and automatically closed on shutdown.
     *
     * @param interfaceClassName The interface class as a String
     * @return A service tracker to follow all instances of that interface
     * @throws IllegalStateException If the OSGi container is not running
     * @since 2.1
     */
    ServiceTracker getServiceTracker(String interfaceClassName);

    /**
     * Gets a service tracker to follow a service registered under a certain interface with a
     * {@link ServiceTrackerCustomizer} attached for customizing tracked service objects. Will return a new
     * {@link ServiceTracker} instance for every call, so don't call more than necessary.  Any provided
     * {@link ServiceTracker} instances will be opened before returning and automatically closed on shutdown.
     *
     * @param interfaceClassName       The interface class as a String
     * @param serviceTrackerCustomizer service tracker customizer for the created service tracker
     * @return A service tracker to follow all instances of that interface
     * @throws IllegalStateException If the OSGi container is not running
     * @since 2.13
     */
    ServiceTracker getServiceTracker(String interfaceClassName, ServiceTrackerCustomizer serviceTrackerCustomizer);

    /**
     * That is shortcut to access SystemBundle.
     */
    void addBundleListener(BundleListener listener);

    /**
     * That is shortcut to access SystemBundle.
     */
    void removeBundleListener(BundleListener listener);

    /**
     * Additional interface for implementations which support reference installation.
     *
     * @deprecated since 4.0.0, to be removed in 5.0.0: Methods in this interface are now provided directly by
     * {@link OsgiContainerManager}.
     */
    @Deprecated
    interface AllowsReferenceInstall {
        /**
         * @see OsgiContainerManager#installBundle(File, ReferenceMode)
         */
        Bundle installBundle(File file, boolean allowReference) throws OsgiContainerException;

        /**
         * Host to default implementation of {@link AllowsReferenceInstall}.
         */
        class Default {
            /**
             * Forwards to {@link OsgiContainerManager#installBundle(File, ReferenceMode)}.
             *
             * @param osgiContainerManager the OsgiContainerManager to install into.
             * @param file                 The bundle file to install.
             * @param allowReference       true iff the file need not be copied, and the container may install a reference to it.
             * @return {@code osgiContainerManager.installBundle(file, allowReference ? PERMIT_REFERENCE : FORBID_REFERENCE)}
             */
            public static Bundle installBundle(
                    final OsgiContainerManager osgiContainerManager, final File file, final boolean allowReference)
                    throws OsgiContainerException {
                return osgiContainerManager.installBundle(file, allowReference ? PERMIT_REFERENCE : FORBID_REFERENCE);
            }
        }
    }
}
