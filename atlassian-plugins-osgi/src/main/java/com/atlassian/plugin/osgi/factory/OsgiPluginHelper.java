package com.atlassian.plugin.osgi.factory;

import com.atlassian.plugin.PluginDependencies;
import com.atlassian.plugin.module.ContainerAccessor;
import org.osgi.framework.Bundle;
import org.osgi.util.tracker.ServiceTracker;

import javax.annotation.Nonnull;
import java.io.InputStream;
import java.net.URL;
import java.util.Set;

/**
 * Helper for the {@link OsgiPlugin} to abstract how key operations are handled in different states, represented
 * by implementations of this interface.
 *
 * @since 2.2.0
 */
interface OsgiPluginHelper {
    /**
     * @return the OSGi bundle
     */
    Bundle getBundle();

    /**
     * Loads a class from the bundle
     *
     * @param clazz        The class name to load
     * @param callingClass The calling class
     * @param <T>          The type of class to load
     * @return An instance of the class
     * @throws ClassNotFoundException If the class cannot be found
     */
    <T> Class<T> loadClass(String clazz, Class<?> callingClass) throws ClassNotFoundException;

    /**
     * Gets a resource from the bundle
     *
     * @param name The resource name
     * @return The resource
     */
    URL getResource(final String name);

    /**
     * Gets a resource as a stream from the bundle
     *
     * @param name The resource name
     * @return The input stream
     */
    InputStream getResourceAsStream(final String name);

    /**
     * Gets the classloader for this bundle
     *
     * @return The class loader instance
     */
    ClassLoader getClassLoader();

    /**
     * Installs the bundle
     *
     * @return The created bundle
     */
    Bundle install();

    /**
     * Notification the bundle has been enabled
     *
     * @param serviceTrackers The service trackers to associate with the bundle
     */
    void onEnable(ServiceTracker... serviceTrackers);

    /**
     * Notification that the plugin has been disabled
     */
    void onDisable();

    /**
     * Notification the bundle has been uninstalled
     */
    void onUninstall();

    /**
     * @return a list of required plugins
     * @deprecated Use {@link #getDependencies()} instead. Since 4.0
     */
    @Deprecated
    Set<String> getRequiredPlugins();

    /**
     * @see OsgiPlugin#getDependencies()
     */
    @Nonnull
    PluginDependencies getDependencies();

    /**
     * @param container the plugin container (likely a spring context)
     */
    void setPluginContainer(Object container);

    ContainerAccessor getContainerAccessor();

    ContainerAccessor getRequiredContainerAccessor();

    /**
     * Tells whether this plugin is a remote plugin or not, by looking up the manifest header.
     *
     * @return {@code true} if it is a remote plugin
     * @since 3.0
     */
    boolean isRemotePlugin();
}
