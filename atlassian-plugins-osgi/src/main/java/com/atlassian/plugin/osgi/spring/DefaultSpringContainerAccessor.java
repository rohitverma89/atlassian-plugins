package com.atlassian.plugin.osgi.spring;

import com.atlassian.plugin.PluginException;
import com.atlassian.plugin.module.ContainerAccessor;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Collection;
import java.util.Map;

import static com.google.common.base.Preconditions.checkState;
import static com.google.common.base.Predicates.isNull;
import static com.google.common.collect.Iterables.any;
import static com.google.common.collect.Lists.newArrayList;

/**
 * Manages spring context access, including autowiring.
 *
 * @since 2.2.0
 */
public class DefaultSpringContainerAccessor implements ContainerAccessor {
    private final Object nativeBeanFactory;
    private final Method nativeCreateBeanMethod;
    private final Method nativeAutowireBeanMethod;
    private final Method nativeGetBeanMethod;
    private final Method nativeGetBeansOfTypeMethod;

    /**
     * The autowire strategy to use when creating and wiring a bean
     */
    public enum AutowireStrategy {
        AUTOWIRE_NO,
        /**
         * Performs setter-based injection by name
         */
        AUTOWIRE_BY_NAME,

        /**
         * Performs setter-based injection by type
         */
        AUTOWIRE_BY_TYPE,

        /**
         * Performs construction-based injection by type
         */
        AUTOWIRE_BY_CONSTRUCTOR,

        /**
         * Autodetects appropriate injection by first seeing if any no-arg constructors exist.  If not, performs constructor
         * injection, and if so, autowires by type then name
         */
        AUTOWIRE_AUTODETECT
    }

    public DefaultSpringContainerAccessor(final Object applicationContext) {
        Object beanFactory = null;
        try {
            final Method m = applicationContext.getClass().getMethod("getAutowireCapableBeanFactory");
            beanFactory = m.invoke(applicationContext);
        } catch (final NoSuchMethodException e) {
            // Should never happen
            throw new PluginException("Cannot find createBean method on registered bean factory: " + beanFactory, e);
        } catch (final IllegalAccessException e) {
            // Should never happen
            throw new PluginException("Cannot access createBean method", e);
        } catch (final InvocationTargetException e) {
            handleSpringMethodInvocationError(e);
        }

        nativeBeanFactory = beanFactory;
        try {
            nativeCreateBeanMethod = beanFactory.getClass().getMethod("createBean", Class.class, int.class, boolean.class);
            nativeAutowireBeanMethod = beanFactory.getClass().getMethod("autowireBeanProperties", Object.class, int.class, boolean.class);
            nativeGetBeanMethod = beanFactory.getClass().getMethod("getBean", String.class);
            nativeGetBeansOfTypeMethod = beanFactory.getClass().getMethod("getBeansOfType", Class.class);

            checkState(!any(newArrayList(nativeGetBeansOfTypeMethod, nativeAutowireBeanMethod, nativeCreateBeanMethod, nativeGetBeanMethod), isNull()));
        } catch (final NoSuchMethodException e) {
            // Should never happen
            throw new PluginException("Cannot find one or more methods on registered bean factory: " + nativeBeanFactory, e);
        }
    }

    private void handleSpringMethodInvocationError(final InvocationTargetException e) {
        if (e.getCause() instanceof Error) {
            throw (Error) e.getCause();
        } else if (e.getCause() instanceof RuntimeException) {
            throw (RuntimeException) e.getCause();
        } else {
            // Should never happen as Spring methods only throw runtime exceptions
            throw new PluginException("Unable to invoke createBean", e.getCause());
        }
    }

    public <T> T createBean(final Class<T> clazz) {
        try {
            return clazz.cast(nativeCreateBeanMethod.invoke(nativeBeanFactory, clazz, AutowireStrategy.AUTOWIRE_AUTODETECT.ordinal(), false));
        } catch (final IllegalAccessException e) {
            // Should never happen
            throw new PluginException("Unable to access createBean method", e);
        } catch (final InvocationTargetException e) {
            handleSpringMethodInvocationError(e);
            return null;
        }
    }

    @Override
    public <T> T injectBean(T bean) {
        try {
            nativeAutowireBeanMethod.invoke(nativeBeanFactory, bean, AutowireStrategy.AUTOWIRE_AUTODETECT.ordinal(), false);
        } catch (final IllegalAccessException e) {
            // Should never happen
            throw new PluginException("Unable to access createBean method", e);
        } catch (final InvocationTargetException e) {
            handleSpringMethodInvocationError(e);
        }
        return bean;
    }

    public <T> Collection<T> getBeansOfType(Class<T> interfaceClass) {
        try {
            Map<String, T> beans = (Map<String, T>) nativeGetBeansOfTypeMethod.invoke(nativeBeanFactory, interfaceClass);
            return beans.values();
        } catch (final IllegalAccessException e) {
            // Should never happen
            throw new PluginException("Unable to access getBeansOfType method", e);
        } catch (final InvocationTargetException e) {
            handleSpringMethodInvocationError(e);
            return null;
        }
    }

    @Override
    public <T> T getBean(final String id) {
        try {
            return (T) nativeGetBeanMethod.invoke(nativeBeanFactory, id);
        } catch (final IllegalAccessException e) {
            // Should never happen
            throw new PluginException("Unable to access getBean method", e);
        } catch (final InvocationTargetException e) {
            handleSpringMethodInvocationError(e);
            return null;
        }
    }
}