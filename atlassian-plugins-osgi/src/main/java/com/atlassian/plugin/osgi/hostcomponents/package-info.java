/**
 * This package provides a way for host applications to register their host components.  See
 * {@link com.atlassian.plugin.osgi.hostcomponents.HostComponentProvider} for more information.
 */
package com.atlassian.plugin.osgi.hostcomponents;