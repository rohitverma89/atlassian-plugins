package com.atlassian.plugin.osgi.factory;

import com.atlassian.plugin.Application;
import com.atlassian.plugin.ModuleDescriptor;
import com.atlassian.plugin.ModuleDescriptorFactory;
import com.atlassian.plugin.Plugin;
import com.atlassian.plugin.PluginParseException;
import com.atlassian.plugin.parsers.XmlDescriptorParser;
import org.dom4j.Element;

import java.io.InputStream;
import java.util.Set;

import static com.google.common.base.Preconditions.checkNotNull;

/**
 * Descriptor parser that handles special tasks for osgi plugins such as recording the
 * originating module descriptor elements.  Must only be used with {@link OsgiPlugin} instances.
 *
 * @since 2.1.2
 */
public class OsgiPluginXmlDescriptorParser extends XmlDescriptorParser {
    /**
     * @param source       The XML descriptor source
     * @param applications The application keys to limit modules to, null for only unspecified
     * @throws com.atlassian.plugin.PluginParseException if there is a problem reading the descriptor from the XML {@link java.io.InputStream}.
     */
    public OsgiPluginXmlDescriptorParser(final InputStream source, final Set<Application> applications) throws PluginParseException {
        super(checkNotNull(source, "The descriptor source must not be null"), applications);
    }

    /**
     * Creates a DescriptorParser that can read multiple XML files
     *
     * @param source              The descriptor stream
     * @param supplementalSources streams that contain additional module descriptors
     * @param applications        the application key to filter modules with, null for all unspecified
     * @throws PluginParseException if there is a problem reading the descriptor from the XML {@link InputStream}.
     * @since 3.2.16
     */
    public OsgiPluginXmlDescriptorParser(final InputStream source, final Iterable<InputStream> supplementalSources, final Set<Application> applications) throws PluginParseException {
        super(source, supplementalSources, applications);
    }


    /**
     * Passes module descriptor elements back to the {@link OsgiPlugin}
     *
     * @param plugin                  The plugin
     * @param element                 The module element
     * @param moduleDescriptorFactory The module descriptor factory
     * @return The module, or null if the module cannot be found
     * @throws PluginParseException
     */
    @Override
    protected ModuleDescriptor<?> createModuleDescriptor(final Plugin plugin, final Element element, final ModuleDescriptorFactory moduleDescriptorFactory) throws PluginParseException {
        final ModuleDescriptor<?> descriptor = super.createModuleDescriptor(plugin, element, moduleDescriptorFactory);

        passModuleDescriptorToPlugin(plugin, element, descriptor);

        return descriptor;
    }

    /**
     * Passes module descriptor elements back to the {@link OsgiPlugin}
     */
    @Override
    public ModuleDescriptor<?> addModule(final ModuleDescriptorFactory moduleDescriptorFactory, final Plugin plugin, final Element module) {
        final ModuleDescriptor<?> descriptor = super.addModule(moduleDescriptorFactory, plugin, module);

        passModuleDescriptorToPlugin(plugin, module, descriptor);

        return descriptor;
    }

    /**
     * Passes module descriptor elements back to the {@link OsgiPlugin}
     */
    private void passModuleDescriptorToPlugin(final Plugin plugin, final Element element, final ModuleDescriptor<?> descriptor) {
        if (plugin instanceof OsgiPlugin) {
            final String key;
            if (descriptor == null) {
                key = element.attributeValue("key");
            } else {
                key = descriptor.getKey();
            }

            ((OsgiPlugin) plugin).addModuleDescriptorElement(key, element);
        }
    }
}
