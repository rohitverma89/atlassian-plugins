package com.atlassian.plugin.osgi.factory;

import com.atlassian.plugin.Application;
import com.atlassian.plugin.JarPluginArtifact;
import com.atlassian.plugin.ModuleDescriptor;
import com.atlassian.plugin.ModuleDescriptorFactory;
import com.atlassian.plugin.Plugin;
import com.atlassian.plugin.PluginArtifact;
import com.atlassian.plugin.PluginParseException;
import com.atlassian.plugin.event.PluginEventManager;
import com.atlassian.plugin.factories.AbstractPluginFactory;
import com.atlassian.plugin.impl.UnloadablePlugin;
import com.atlassian.plugin.osgi.container.OsgiContainerManager;
import com.atlassian.plugin.osgi.container.OsgiPersistentCache;
import com.atlassian.plugin.osgi.factory.transform.DefaultPluginTransformer;
import com.atlassian.plugin.osgi.factory.transform.PluginTransformationException;
import com.atlassian.plugin.osgi.factory.transform.PluginTransformer;
import com.atlassian.plugin.osgi.factory.transform.model.SystemExports;
import com.atlassian.plugin.parsers.CompositeDescriptorParserFactory;
import com.atlassian.plugin.parsers.DescriptorParser;
import com.google.common.base.Function;
import com.google.common.base.Predicate;
import com.google.common.collect.Sets;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.StringUtils;
import org.dom4j.Element;
import org.osgi.framework.Constants;
import org.osgi.util.tracker.ServiceTracker;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Nullable;
import java.io.File;
import java.io.InputStream;
import java.util.Arrays;
import java.util.Set;
import java.util.jar.Manifest;

import static com.atlassian.plugin.ReferenceMode.PERMIT_REFERENCE;
import static com.atlassian.plugin.osgi.util.OsgiHeaderUtil.extractOsgiPluginInformation;
import static com.atlassian.plugin.osgi.util.OsgiHeaderUtil.getAttributeWithoutValidation;
import static com.atlassian.plugin.osgi.util.OsgiHeaderUtil.getManifest;
import static com.atlassian.plugin.osgi.util.OsgiHeaderUtil.getNonEmptyAttribute;
import static com.atlassian.plugin.parsers.XmlDescriptorParserUtils.addModule;
import static com.google.common.base.Preconditions.checkNotNull;
import static com.google.common.collect.Iterables.transform;

/**
 * Plugin loader that starts an OSGi container and loads plugins into it, wrapped as OSGi bundles.  Supports
 * <ul>
 * <li>Dynamic loading of module descriptors via OSGi services</li>
 * <li>Delayed enabling until the plugin container is active</li>
 * <li>XML or Jar manifest configuration</li>
 * </ul>
 */
public final class OsgiPluginFactory extends AbstractPluginFactory {
    private static final Logger log = LoggerFactory.getLogger(OsgiPluginFactory.class);

    public interface PluginTransformerFactory {
        PluginTransformer newPluginTransformer(OsgiPersistentCache cache, SystemExports systemExports, Set<Application> applicationKeys, String pluginDescriptorPath, OsgiContainerManager osgi);
    }

    public static class DefaultPluginTransformerFactory implements PluginTransformerFactory {
        public PluginTransformer newPluginTransformer(
                final OsgiPersistentCache cache,
                final SystemExports systemExports,
                final Set<Application> applicationKeys,
                final String pluginDescriptorPath,
                final OsgiContainerManager osgi) {
            return new DefaultPluginTransformer(cache, systemExports, applicationKeys, pluginDescriptorPath, osgi);
        }
    }

    private final OsgiContainerManager osgi;
    private final String pluginDescriptorFileName;
    private final PluginEventManager pluginEventManager;
    private final Set<Application> applications;
    private final OsgiPersistentCache persistentCache;
    private final PluginTransformerFactory pluginTransformerFactory;

    private volatile PluginTransformer pluginTransformer;

    private final OsgiChainedModuleDescriptorFactoryCreator osgiChainedModuleDescriptorFactoryCreator;

    /**
     * Default constructor
     */
    public OsgiPluginFactory(
            final String pluginDescriptorFileName,
            final Set<Application> applications,
            final OsgiPersistentCache persistentCache,
            final OsgiContainerManager osgi,
            final PluginEventManager pluginEventManager) {
        this(pluginDescriptorFileName, applications, persistentCache, osgi, pluginEventManager, new DefaultPluginTransformerFactory());
    }

    /**
     * Constructor for implementations that want to override the DefaultPluginTransformer with a custom implementation
     */
    public OsgiPluginFactory(
            final String pluginDescriptorFileName,
            final Set<Application> applications,
            final OsgiPersistentCache persistentCache,
            final OsgiContainerManager osgi,
            final PluginEventManager pluginEventManager,
            final PluginTransformerFactory pluginTransformerFactory) {
        super(new OsgiPluginXmlDescriptorParserFactory(), applications);
        this.pluginDescriptorFileName = checkNotNull(pluginDescriptorFileName, "Plugin descriptor is required");
        this.osgi = checkNotNull(osgi, "The OSGi container is required");
        this.applications = checkNotNull(applications, "Applications is required!");
        this.persistentCache = checkNotNull(persistentCache, "The osgi persistent cache is required");
        this.pluginEventManager = checkNotNull(pluginEventManager, "The plugin event manager is required");
        this.pluginTransformerFactory = checkNotNull(pluginTransformerFactory, "The plugin transformer factory is required");
        this.osgiChainedModuleDescriptorFactoryCreator = new OsgiChainedModuleDescriptorFactoryCreator(new OsgiChainedModuleDescriptorFactoryCreator.ServiceTrackerFactory() {
            public ServiceTracker create(final String className) {
                return osgi.getServiceTracker(className);
            }
        });
    }

    private PluginTransformer getPluginTransformer() {
        if (pluginTransformer == null) {
            final String exportString = osgi.getBundles()[0].getHeaders()
                    .get(Constants.EXPORT_PACKAGE);
            final SystemExports exports = new SystemExports(exportString);
            pluginTransformer = pluginTransformerFactory.newPluginTransformer(persistentCache, exports, applications, pluginDescriptorFileName, osgi);
        }
        return pluginTransformer;
    }

    public String canCreate(final PluginArtifact pluginArtifact) throws PluginParseException {
        // see TestPluginFactorySelection comments for selection logic
        // explanation
        boolean isPlugin = hasDescriptor(checkNotNull(pluginArtifact));
        boolean hasSpring = pluginArtifact.containsSpringContext();
        boolean isTransformless = getPluginKeyFromManifest(checkNotNull(pluginArtifact)) != null;

        String key = null;
        if ((isPlugin && !isTransformless) || (isTransformless && hasSpring)) {
            key = isPlugin ? getPluginKeyFromDescriptor(checkNotNull(pluginArtifact)) : getPluginKeyFromManifest(pluginArtifact);
        }

        return key;
    }

    @Override
    protected InputStream getDescriptorInputStream(final PluginArtifact pluginArtifact) {
        return pluginArtifact.getResourceAsStream(pluginDescriptorFileName);
    }

    @Override
    protected Predicate<Integer> isValidPluginsVersion() {
        return new Predicate<Integer>() {
            @Override
            public boolean apply(final Integer input) {
                return input == Plugin.VERSION_2;
            }
        };
    }

    /**
     * @param pluginArtifact The plugin artifact
     * @return The plugin key if a manifest is present and contains {@link OsgiPlugin#ATLASSIAN_PLUGIN_KEY} and
     * {@link Constants#BUNDLE_VERSION}
     */
    private String getPluginKeyFromManifest(final PluginArtifact pluginArtifact) {
        final Manifest mf = getManifest(pluginArtifact);
        if (mf != null) {
            final String key = mf.getMainAttributes().getValue(OsgiPlugin.ATLASSIAN_PLUGIN_KEY);
            final String version = mf.getMainAttributes().getValue(Constants.BUNDLE_VERSION);
            if (key != null) {
                if (version != null) {
                    return key;
                } else {
                    log.warn("Found plugin key '" + key + "' in the manifest but no bundle version, so it can't be loaded as an OsgiPlugin");
                }
            }
        }
        return null;
    }

    /**
     * @param pluginArtifact The plugin artifact
     * @return The scan folders  if a manifest is present and contains an {@link OsgiPlugin#ATLASSIAN_PLUGIN_KEY}
     * @since 3.2.13
     */
    private Iterable<String> getScanFoldersFromManifest(PluginArtifact pluginArtifact) {
        final Set<String> scanFolders = Sets.newHashSet();
        final Manifest mf = getManifest(pluginArtifact);
        if (mf != null) {
            final String sf = mf.getMainAttributes().getValue(OsgiPlugin.ATLASSIAN_SCAN_FOLDERS);
            if (StringUtils.isNotBlank(sf)) {
                String[] folders = sf.split(",");
                scanFolders.addAll(Arrays.asList(folders));
            }
        }
        return scanFolders;
    }

    /**
     * Deploys the plugin artifact.  The artifact will only undergo transformation if a plugin descriptor can be found
     * and the "Atlassian-Plugin-Key" value is not already defined in the manifest.
     *
     * @param pluginArtifact          the plugin artifact to deploy
     * @param moduleDescriptorFactory The factory for plugin modules
     * @return The instantiated and populated plugin
     * @throws PluginParseException     If the descriptor cannot be parsed
     * @throws IllegalArgumentException If the plugin descriptor isn't found, and the plugin key and bundle version aren't
     *                                  specified in the manifest
     * @since 2.2.0
     */
    public Plugin create(final PluginArtifact pluginArtifact, final ModuleDescriptorFactory moduleDescriptorFactory)
            throws PluginParseException {
        checkNotNull(pluginArtifact, "The plugin deployment unit is required");
        checkNotNull(moduleDescriptorFactory, "The module descriptor factory is required");

        Plugin plugin = null;
        InputStream pluginDescriptor = null;
        try {
            pluginDescriptor = pluginArtifact.getResourceAsStream(pluginDescriptorFileName);
            if (pluginDescriptor != null) {
                final ModuleDescriptorFactory combinedFactory = getChainedModuleDescriptorFactory(
                        moduleDescriptorFactory, pluginArtifact);

                final PluginArtifact artifactToInstall;

                // only transform the artifact if no plugin key is found in the manifest
                final String pluginKeyFromManifest = getPluginKeyFromManifest(pluginArtifact);
                if (pluginKeyFromManifest == null) {
                    log.debug("Plugin key NOT found in manifest at entry {}, undergoing transformation", OsgiPlugin.ATLASSIAN_PLUGIN_KEY);
                    artifactToInstall = createOsgiPluginJar(pluginArtifact);
                } else {
                    log.debug("Plugin key found in manifest at entry {}, skipping transformation for '{}'", OsgiPlugin.ATLASSIAN_PLUGIN_KEY, pluginKeyFromManifest);
                    artifactToInstall = pluginArtifact;
                }
                final DescriptorParser parser = createDescriptorParser(artifactToInstall, pluginDescriptor);
                final Plugin osgiPlugin = new OsgiPlugin(parser.getKey(), osgi, artifactToInstall, pluginArtifact, pluginEventManager);

                // Temporarily configure plugin until it can be properly installed
                plugin = parser.configurePlugin(combinedFactory, osgiPlugin);
            } else {
                final Manifest manifest = getManifest(pluginArtifact);
                if (manifest != null) {
                    plugin = extractOsgiPlugin(pluginArtifact, manifest, osgi, pluginEventManager);
                    // The next true is because we require a Bundle-Version in our canCreate
                    plugin.setPluginInformation(extractOsgiPluginInformation(manifest, true));
                } else {
                    log.warn("Unable to load plugin from '{}', no manifest", pluginArtifact);

                    return new UnloadablePlugin("No manifest in PluginArtifact '" + pluginArtifact + "'");
                }
            }
        } catch (final PluginTransformationException ex) {
            return reportUnloadablePlugin(pluginArtifact.toFile(), ex);
        } finally {
            IOUtils.closeQuietly(pluginDescriptor);
        }
        return plugin;
    }

    @Override
    public ModuleDescriptor<?> createModule(final Plugin plugin, final Element module, final ModuleDescriptorFactory moduleDescriptorFactory) {
        if (plugin instanceof OsgiPlugin) {
            final ModuleDescriptorFactory combinedFactory = osgiChainedModuleDescriptorFactoryCreator.create(new OsgiChainedModuleDescriptorFactoryCreator.ResourceLocator() {
                @Override
                public boolean doesResourceExist(final String name) {
                    // This returns true to indicate that the listable module descriptor class is present in this plugin.
                    // Those module descriptors are skipped when first installing the plugin, however no need for us to skip
                    // here, as the plugin has been loaded/installed.
                    return false;
                }
            }, moduleDescriptorFactory);

            return addModule(combinedFactory, plugin, module);
        } else {
            return null;
        }
    }

    private DescriptorParser createDescriptorParser(final PluginArtifact pluginArtifact, final InputStream pluginDescriptor) {
        final Set<String> xmlPaths = Sets.newHashSet();
        for (String path : getScanFoldersFromManifest(pluginArtifact)) {
            xmlPaths.addAll(((PluginArtifact.HasExtraModuleDescriptors) pluginArtifact).extraModuleDescriptorFiles(path));
        }

        final Iterable<InputStream> sources = transform(xmlPaths
                , new Function<String, InputStream>() {
            final Iterable<InputStream> streams = Sets.newHashSet();

            @Override
            public InputStream apply(@Nullable String source) {
                return pluginArtifact.getResourceAsStream(source);
            }
        });
        return ((CompositeDescriptorParserFactory) descriptorParserFactory).getInstance(pluginDescriptor, sources, applications);
    }

    private static Plugin extractOsgiPlugin(
            final PluginArtifact pluginArtifact,
            final Manifest mf,
            final OsgiContainerManager osgi,
            final PluginEventManager pluginEventManager) {
        final String pluginKey = getNonEmptyAttribute(mf, OsgiPlugin.ATLASSIAN_PLUGIN_KEY);
        final String bundleName = getAttributeWithoutValidation(mf, Constants.BUNDLE_NAME);

        final Plugin plugin = new OsgiPlugin(pluginKey, osgi, pluginArtifact, pluginArtifact, pluginEventManager);
        plugin.setPluginsVersion(2);
        plugin.setName(bundleName);
        return plugin;
    }

    /**
     * Get a chained module descriptor factory that includes any dynamically available descriptor factories
     *
     * @param originalFactory The factory provided by the host application
     * @param pluginArtifact
     * @return The composite factory
     */
    private ModuleDescriptorFactory getChainedModuleDescriptorFactory(
            final ModuleDescriptorFactory originalFactory, final PluginArtifact pluginArtifact) {
        return osgiChainedModuleDescriptorFactoryCreator.create(new OsgiChainedModuleDescriptorFactoryCreator.ResourceLocator() {
            public boolean doesResourceExist(final String name) {
                return pluginArtifact.doesResourceExist(name);
            }
        }, originalFactory);
    }

    private PluginArtifact createOsgiPluginJar(final PluginArtifact pluginArtifact) {
        final File transformedFile = getPluginTransformer().transform(pluginArtifact, osgi.getHostComponentRegistrations());
        // A transformed jar is always allows reference, because the jar is sitting in a transform cache somewhere.
        // A PluginTransformer implementation could conceivably break this, but it would be broken to do so.
        return new JarPluginArtifact(transformedFile, PERMIT_REFERENCE);
    }

    private Plugin reportUnloadablePlugin(final File file, final Exception e) {
        log.error("Unable to load plugin: " + file, e);

        final UnloadablePlugin plugin = new UnloadablePlugin();
        plugin.setErrorText("Unable to load plugin: " + e.getMessage());
        return plugin;
    }
}
