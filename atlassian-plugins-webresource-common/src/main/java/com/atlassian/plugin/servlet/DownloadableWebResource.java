package com.atlassian.plugin.servlet;

import com.atlassian.plugin.Plugin;
import com.atlassian.plugin.elements.ResourceLocation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.ServletContext;
import java.io.InputStream;

/**
 * A {@link DownloadableResource} that will serve the resource via the web application's {@link ServletContext}.
 */
public class DownloadableWebResource extends AbstractDownloadableResource {
    private static final Logger log = LoggerFactory.getLogger(DownloadableWebResource.class);

    private final ServletContext servletContext;

    public DownloadableWebResource(Plugin plugin, ResourceLocation resourceLocation, String extraPath, ServletContext servletContext, boolean disableMinification) {
        super(plugin, resourceLocation, extraPath, disableMinification);
        this.servletContext = servletContext;
    }

    @Override
    protected InputStream getResourceAsStream(final String resourceLocation) {
        String fixedResourceLocation = fixResourceLocation(resourceLocation);
        return servletContext.getResourceAsStream(fixedResourceLocation);
    }

    private String fixResourceLocation(final String resourceLocation) {
        if (!resourceLocation.startsWith("/")) {
            final String resourceLocationWithSlash = "/" + resourceLocation;
            log.debug("ResourceLocation: {}, does not start with slash. Location was modified to: {}",
                    resourceLocation, resourceLocationWithSlash);
            return resourceLocationWithSlash;
        }
        return resourceLocation;
    }
}
