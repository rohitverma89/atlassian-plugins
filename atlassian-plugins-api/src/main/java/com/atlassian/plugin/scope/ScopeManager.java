package com.atlassian.plugin.scope;

import com.atlassian.plugin.ScopeAware;

/**
 * Entry point for scope checks.
 * <p>
 * Scope key is accessible from all {@link com.atlassian.plugin.ScopeAware} classes.
 * <p>
 * Typical users of this class are module descriptor resolvers which will expose/hide
 * module functionality based on whenever given scope is enabled for current tenant.
 * <p>
 * For example serving JIRA Service Desk rest request would need to pass `jira-service-desk` license scope verification
 * and if product happened not to be licensed for current tenant then rest module descriptor resolver will intercept
 * request and return 404 instead of serving normal response by underlying plugin's rest endpoint.
 * <p>
 * Return values of this class must not be cached and underlying implementation will guarantee
 * acceptable performance for multiple per-request invocations.
 *
 * @see com.atlassian.plugin.ScopeAware
 * @since 4.1
 */
public interface ScopeManager {
    /**
     * @param scopeKey the scope key
     * @return true if scope identified by scope key is active
     */
    boolean isScopeActive(String scopeKey);

    static boolean isActive(ScopeManager scopeManager, ScopeAware scopeAware) {
        return scopeAware.getScopeKey().map(s -> scopeManager.isScopeActive(s)).orElse(true);
    }
}
