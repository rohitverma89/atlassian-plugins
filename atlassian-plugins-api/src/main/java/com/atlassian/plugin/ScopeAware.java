package com.atlassian.plugin;

import java.util.Optional;

import static java.util.Optional.empty;

/**
 * Makes class scope aware.
 * <p>
 * Intention of scope is to mark plugin or set of plugins as belonging to subset of product functionality
 * which could be turned on/off in one go.
 * <p>
 * Given interface could be used in conjunction with {@link com.atlassian.plugin.scope.ScopeManager}
 * which will take care about validating scope for current tenant.
 * <p>
 * Making plugin scope aware is done via adding {@code <scope>} element:
 * <pre>
 * {@code <plugin-info><scope key = "/license/jira-service-desk"/></plugin-info>}
 * </pre>
 * once configured plugin itself and all modules will have that scope assigned.
 * <p>
 * Modules can opt out from scope by specifying `scope` attribute:
 * <pre>
 * {@code<rest key="key" name="name" scoped="false" ... />"}
 * </pre>
 *
 * @see com.atlassian.plugin.scope.ScopeManager
 * @since 4.1
 */
public interface ScopeAware {
    /**
     * @return scope set or {@link Optional#empty()} if not defined
     */
    default Optional<String> getScopeKey() {
        return empty();
    }
}
