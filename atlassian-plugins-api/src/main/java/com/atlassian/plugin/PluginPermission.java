package com.atlassian.plugin;

import com.atlassian.fugue.Option;
import com.google.common.base.Objects;

import static com.atlassian.fugue.Option.some;
import static com.google.common.base.Preconditions.checkNotNull;

/**
 * <p>Represents a plugin permission as parsed from the plugin descriptor.
 * <p>A plugin permission here is:
 * <ul>
 * <li>A <strong>name</strong> which denotes the permission itself.</li>
 * <li>An <strong>{@link InstallationMode installation mode}</strong> which tells whether the permission is required
 * for a given type of installation of the plugin. No installation mode defined means that the permission is always
 * required.</li>
 * </ul>
 *
 * @since 3.0
 */
public final class PluginPermission {
    public final static PluginPermission ALL = new PluginPermission(Permissions.ALL_PERMISSIONS);
    public final static PluginPermission EXECUTE_JAVA = new PluginPermission(Permissions.EXECUTE_JAVA);

    private final String name;
    private final Option<InstallationMode> installationMode;

    public PluginPermission(String name) {
        this(checkNotNull(name), Option.<InstallationMode>none());
    }

    public PluginPermission(String name, InstallationMode installationMode) {
        this(checkNotNull(name), some(checkNotNull(installationMode)));
    }

    public PluginPermission(String name, Option<InstallationMode> installationMode) {
        this.name = checkNotNull(name);
        this.installationMode = checkNotNull(installationMode);
    }

    public String getName() {
        return name;
    }

    /**
     * The installation mode for that permission.
     *
     * @return the installation mode as an {@link Option}. If the option is {@link Option#none()}
     * then this means this permission is always valid, however when it is {@link Option#defined() defined}
     * it will only be valid for the given installation mode.
     */
    public Option<InstallationMode> getInstallationMode() {
        return installationMode;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }

        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        final PluginPermission that = (PluginPermission) o;
        return Objects.equal(this.name, that.name);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(this.name);
    }
}
