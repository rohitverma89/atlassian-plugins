package com.atlassian.plugin;

/**
 * A named boolean for specifying whether to allow reference installation of plugins.
 *
 * @since 4.0.0
 */
public enum ReferenceMode {
    /**
     * {@link ReferenceMode#allowsReference} returns false.
     */
    FORBID_REFERENCE(false),

    /**
     * {@link ReferenceMode#allowsReference} returns true.
     */
    PERMIT_REFERENCE(true);

    private boolean allowsReference;

    private ReferenceMode(boolean allowsReference) {
        this.allowsReference = allowsReference;
    }

    public boolean allowsReference() {
        return allowsReference;
    }
}
