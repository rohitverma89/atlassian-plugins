package com.atlassian.plugin.event.events;

import com.atlassian.annotations.PublicApi;
import com.atlassian.plugin.ModuleDescriptor;

import static com.google.common.base.Preconditions.checkNotNull;

/**
 * Base class for events with ModuleDescriptor context.
 *
 * @see com.atlassian.plugin.event.events
 * @since 4.0.0
 */
@PublicApi
public class PluginModuleEvent {
    private final ModuleDescriptor<?> module;

    public PluginModuleEvent(final ModuleDescriptor<?> module) {
        this.module = checkNotNull(module);
    }

    public ModuleDescriptor<?> getModule() {
        return module;
    }

    @Override
    public String toString() {
        return getClass().getName() + " for " + module;
    }
}
