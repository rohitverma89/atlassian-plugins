package com.atlassian.plugin.event.events;

import com.atlassian.annotations.PublicApi;
import com.atlassian.plugin.Plugin;
import com.atlassian.plugin.PluginState;

import javax.annotation.Nonnull;
import java.util.Set;

import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkNotNull;

/**
 * Event fired after dependent plugins have changed their state in response to a change in this plugin's state i.e.
 * install, enable, uninstall, disable.
 *
 * @see com.atlassian.plugin.event.events
 * @since 4.0.0
 */
@PublicApi
public class PluginDependentsChangedEvent extends PluginEvent {
    final PluginState state;
    final Set<Plugin> disabled;
    final Set<Plugin> cycled;

    public PluginDependentsChangedEvent(final Plugin plugin, @Nonnull final PluginState state, @Nonnull final Set<Plugin> disabled, @Nonnull final Set<Plugin> cycled) {
        super(plugin);
        this.state = checkNotNull(state);
        checkArgument(state == PluginState.INSTALLED || state == PluginState.ENABLED || state == PluginState.UNINSTALLED || state == PluginState.DISABLED, "state must be one of INSTALLED, ENABLED, UNINSTALLED, DISABLED");
        this.disabled = checkNotNull(disabled);
        this.cycled = checkNotNull(cycled);
    }

    /**
     * End state of plugin that caused this event.
     *
     * @return one of {@link PluginState#INSTALLED}, {@link PluginState#ENABLED}, {@link PluginState#UNINSTALLED}, {@link PluginState#DISABLED}
     */
    public PluginState getState() {
        return state;
    }

    /**
     * Plugins which had their state changed from enabled to disabled
     *
     * @return possibly empty set
     */
    public Set<Plugin> getDisabled() {
        return disabled;
    }

    /**
     * Plugins which had their state changed from enabled to disabled to enabled
     *
     * @return possibly empty set
     */
    public Set<Plugin> getCycled() {
        return cycled;
    }

    @Override
    public String toString() {
        return super.toString() + ", disabled=" + disabled + ", cycled=" + cycled;
    }
}
