package com.atlassian.plugin.event.events;

import com.atlassian.annotations.PublicApi;
import com.atlassian.plugin.PluginAccessor;
import com.atlassian.plugin.PluginController;

/**
 * Signals a warm restart of the plugin framework has been completed.
 *
 * @see com.atlassian.plugin.event.events
 * @since 2.3.0
 */
@PublicApi
public class PluginFrameworkWarmRestartedEvent extends PluginFrameworkEvent {
    public PluginFrameworkWarmRestartedEvent(final PluginController pluginController, final PluginAccessor pluginAccessor) {
        super(pluginController, pluginAccessor);
    }
}
