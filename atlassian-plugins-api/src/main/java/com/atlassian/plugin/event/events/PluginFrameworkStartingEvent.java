package com.atlassian.plugin.event.events;

import com.atlassian.annotations.PublicApi;
import com.atlassian.plugin.PluginAccessor;
import com.atlassian.plugin.PluginController;

/**
 * Event that signifies the plugin framework is being started.
 *
 * @see com.atlassian.plugin.event.events
 * @since 2.0.0
 */
@PublicApi
public class PluginFrameworkStartingEvent extends PluginFrameworkEvent {
    public PluginFrameworkStartingEvent(final PluginController pluginController, final PluginAccessor pluginAccessor) {
        super(pluginController, pluginAccessor);
    }
}
