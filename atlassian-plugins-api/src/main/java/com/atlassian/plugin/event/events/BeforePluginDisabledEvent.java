package com.atlassian.plugin.event.events;

import com.atlassian.plugin.Plugin;

/**
 * Event that signifies a plugin is about to be disabled, uninstalled, or updated.
 *
 * @see com.atlassian.plugin.event.events
 * @since 3.0.5
 * @deprecated since 4.0.0. Use {@link PluginDisablingEvent} instead.
 */
@Deprecated
public class BeforePluginDisabledEvent extends PluginEvent {
    public BeforePluginDisabledEvent(final Plugin plugin) {
        super(plugin);
    }
}
