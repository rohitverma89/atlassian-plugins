package com.atlassian.plugin;

import com.atlassian.fugue.Option;
import com.google.common.collect.ImmutableSet;

import java.util.Set;

import static com.atlassian.fugue.Option.option;
import static com.google.common.base.Preconditions.checkNotNull;

/**
 * @since 3.0
 */
public final class Permissions {
    /**
     * <p>Represents all the permissions in one. This is useful for plugins before version 3 which by default require all
     * permissions.
     * <p>It is not a good idea to use this as a plugin developer, as this is a way internally to define permissions for
     * legacy plugins.
     */
    public static final String ALL_PERMISSIONS = "all_permissions";

    /**
     * A permission to execute java code. You will need this permission if as a plugin developers you write your own
     * java components for your plugin, be it a simple service, a servlet, etc.
     */
    public static final String EXECUTE_JAVA = "execute_java";

    /**
     * A permission to create system modules.
     */
    public static final String CREATE_SYSTEM_MODULES = "create_system_modules";

    /**
     * <p>A permission to generate arbitrary HTML.
     * <p>Generating arbitrary HTML can be a security threat hence why it would require such permission.
     */
    public static final String GENERATE_ANY_HTML = "generate_any_html";

    private Permissions() {
    }

    public static Set<String> getRequiredPermissions(Class<?> type) {
        final Class<RequirePermission> annotation = RequirePermission.class;
        if (type != null && type.isAnnotationPresent(annotation)) {
            return ImmutableSet.copyOf(type.getAnnotation(annotation).value());
        }
        return ImmutableSet.of();
    }

    public static Plugin addPermission(Plugin plugin, String permission, InstallationMode mode) {
        checkNotNull(plugin);

        getPluginInformation(plugin).setPermissions(
                ImmutableSet.<PluginPermission>builder()
                        .addAll(getPluginInformation(plugin).getPermissions())
                        .add(new PluginPermission(permission, Option.<InstallationMode>option(mode)))
                        .build());
        return plugin;
    }

    private static PluginInformation getPluginInformation(Plugin plugin) {
        return option(plugin.getPluginInformation()).getOrElse(new PluginInformation());
    }
}
