# Atlassian Plugins

## Description

The plugin framework supports several types of plugins, including OSGi-based plugins. [OSGi](http://www.osgi.org/) is a dynamic module system for Java that the framework uses to enable plugins to depend on each other and more easily share services. Because the plugin framework supports multiple versions of plugins simultaneously, legacy plugins or those built into the application can exist side-by-side with newer dynamic plugins that leverage the plugin-sharing benefits of OSGi.

## Atlassian Developer?

### Committing Guidelines

Please see the [Platform Rules Of Engagement (go/proe)](http://go.atlassian.com/proe) for committing to this module.
The main active branches are

- [atlassian-plugins-3.0.x](https://bitbucket.org/atlassian/atlassian-plugins/commits/branch/atlassian-plugins-3.0.x):
  Bug fixes for legacy product usage of plugin.
  Topic branches here should be prefixed with `issue-3.0/`.
- [atlassian-plugins-3.2.x](https://bitbucket.org/atlassian/atlassian-plugins/commits/branch/atlassian-plugins-3.2.x):
  Bug fixes for current product usage of plugin.
  Topic branches here should be prefixed with `issue-3.2/`.
- [atlassian-plugins-4.0.x](https://bitbucket.org/atlassian/atlassian-plugins/commits/branch/atlassian-plugins-4.0.x):
  Bug fixes for current product usage of plugin.
  Topic branches here should be prefixed with `issue-4.0/`.
- [master](https://bitbucket.org/atlassian/atlassian-plugins/commits/branch/master):
  New features for the next backwards compatible release of plugins.
  Topic branches here should be prefixed with `issue/`.

### Builds

The Bamboo builds for this project are on [EcoBAC](https://ecosystem-bamboo.internal.atlassian.com/browse/PLUG)

### Integration Testing

Any tests under the it package must be run in the `integration-test` maven phase.

### Releasing

Releasing is done with a release stage in the bamboo CI build.
See the [Platform Rules Of Engagement](http://go.atlassian.com/proe) for details.

## External User?

### Issues

Please raise any issues you find with this module in [JIRA](https://ecosystem.atlassian.net/browse/PLUG)

### Documentation

[Plugin Framework Documentation](https://developer.atlassian.com/display/PLUGINFRAMEWORK)