package com.atlassian.plugin.servlet.descriptors;

import com.atlassian.plugin.ModulePermissionException;
import com.atlassian.plugin.Permissions;
import com.atlassian.plugin.Plugin;
import com.atlassian.plugin.PluginParseException;
import com.atlassian.plugin.impl.StaticPlugin;
import com.atlassian.plugin.module.ModuleFactory;
import com.atlassian.plugin.servlet.ServletModuleManager;
import org.dom4j.Element;
import org.dom4j.dom.DOMElement;
import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import javax.servlet.http.HttpServlet;

@RunWith(MockitoJUnitRunner.class)
public class TestServletModuleDescriptor {
    @Rule
    public ExpectedException expectedException = ExpectedException.none();

    private ServletModuleDescriptor descriptor;

    @Mock
    private ServletModuleManager servletModuleManager;

    @Before
    public void setUp() {
        descriptor = new ServletModuleDescriptor(ModuleFactory.LEGACY_MODULE_FACTORY, servletModuleManager);
    }

    @After
    public void tearDown() {
        descriptor = null;
    }

    @Test
    public void initWithExecuteJavaPermission() {
        Plugin plugin = new StaticPlugin();
        plugin.setKey("somekey");
        Permissions.addPermission(plugin, Permissions.EXECUTE_JAVA, null);

        Element e = getValidConfig();
        e.addAttribute("class", SomeServlet.class.getName());

        // test that this does not throw
        descriptor.init(plugin, e);
    }

    @Test
    public void initWithAllPermission() {
        Plugin plugin = new StaticPlugin();
        plugin.setKey("somekey");
        Permissions.addPermission(plugin, Permissions.ALL_PERMISSIONS, null);

        Element e = getValidConfig();
        e.addAttribute("class", SomeServlet.class.getName());

        // test that this does not throw
        descriptor.init(plugin, e);
    }

    @Test
    public void initWithoutExecuteJavaPermission() {
        Plugin plugin = new StaticPlugin();
        plugin.setKey("somekey");
        Element e = getValidConfig();
        e.addAttribute("class", SomeServlet.class.getName());

        expectedException.expect(ModulePermissionException.class);

        descriptor.init(plugin, e);
    }

    private Element getValidConfig() {
        Element e = new DOMElement("servlet");
        e.addAttribute("key", "key2");
        Element url = new DOMElement("url-pattern");
        url.setText("/foo");
        e.add(url);
        return e;
    }

    @Test
    public void initWithNoUrlPattern() {
        Plugin plugin = new StaticPlugin();
        plugin.setKey("somekey");
        Element e = new DOMElement("servlet");
        e.addAttribute("key", "key2");
        e.addAttribute("class", SomeServlet.class.getName());

        expectedException.expect(PluginParseException.class);

        descriptor.init(plugin, e);
    }

    @Test
    public void initWithMissingParamValue() {
        Plugin plugin = new StaticPlugin();
        plugin.setKey("somekey");
        Element e = new DOMElement("servlet");
        e.addAttribute("key", "key2");
        e.addAttribute("class", SomeServlet.class.getName());
        Element url = new DOMElement("url-pattern");
        url.setText("/foo");
        e.add(url);
        Element param = new DOMElement("init-param");
        e.add(param);

        expectedException.expect(PluginParseException.class);

        descriptor.init(plugin, e);
    }

    @Test
    public void initWithNoClass() {
        Plugin plugin = new StaticPlugin();
        plugin.setKey("somekey");
        Permissions.addPermission(plugin, Permissions.ALL_PERMISSIONS, null);

        Element e = getValidConfig();

        // test that this does not throw
        descriptor.init(plugin, e);
    }

    static class SomeServlet extends HttpServlet {
    }
}
