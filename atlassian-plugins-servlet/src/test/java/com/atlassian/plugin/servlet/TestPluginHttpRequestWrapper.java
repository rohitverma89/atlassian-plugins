package com.atlassian.plugin.servlet;

import com.atlassian.plugin.servlet.descriptors.ServletModuleDescriptor;
import com.atlassian.plugin.servlet.descriptors.ServletModuleDescriptorBuilder;
import org.junit.Test;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class TestPluginHttpRequestWrapper {

    @Test
    public void testForwardScenario() throws Exception {
        final HttpServletRequest mockInnerRequest = mock(HttpServletRequest.class);

        // Initially mocking it to "/hello" (so that the forward request's basePath is resolved to "/hello". This is replicating the specific issue
        when(mockInnerRequest.getPathInfo()).thenReturn("/forwardSource");

        when(mockInnerRequest.getServletPath()).thenReturn("/plugin/servlet");

        PluginHttpRequestWrapper forwardRequest = new PluginHttpRequestWrapper(mockInnerRequest,
                new ServletModuleDescriptorBuilder().withPath("/forwardSource").build());

        // Mocking it to its actual value "/world"
        when(mockInnerRequest.getPathInfo()).thenReturn("/forwardDestination");

        assertEquals("/forwardDestination", forwardRequest.getPathInfo());
    }

    @Test
    public void testWildcardMatching() {
        PluginHttpRequestWrapper request = getWrappedRequest("/context/plugins", "/plugin/servlet/path/to/resource",
                new ServletModuleDescriptorBuilder().withPath("/plugin/servlet/*").build());

        assertEquals("/path/to/resource", request.getPathInfo());
        assertEquals("/context/plugins/plugin/servlet", request.getServletPath());
    }

    @Test
    public void testExactPathMatching() {
        PluginHttpRequestWrapper request = getWrappedRequest("/context/plugins", "/plugin/servlet",
                new ServletModuleDescriptorBuilder().withPath("/plugin/servlet").build());

        assertNull(request.getPathInfo());
        assertEquals("/context/plugins/plugin/servlet", request.getServletPath());
    }

    private PluginHttpRequestWrapper getWrappedRequest(String servletPath, String pathInfo,
                                                       ServletModuleDescriptor servletModuleDescriptor) {
        HttpServletRequest mockWrappedRequest = mock(HttpServletRequest.class);
        when(mockWrappedRequest.getServletPath()).thenReturn(servletPath);
        when(mockWrappedRequest.getPathInfo()).thenReturn(pathInfo);

        return new PluginHttpRequestWrapper(mockWrappedRequest, servletModuleDescriptor);
    }

    @Test
    public void testGetSessionFalse() throws Exception {
        HttpServletRequest mockWrappedRequest = mock(HttpServletRequest.class);
        when(mockWrappedRequest.getPathInfo()).thenReturn(null);
        when(mockWrappedRequest.getSession(false)).thenReturn(null);

        PluginHttpRequestWrapper request = new PluginHttpRequestWrapper(mockWrappedRequest, null);

        assertNull(request.getSession(false));
    }

    @Test
    public void testGetSession() throws Exception {
        // Mock the Session
        HttpSession mockSession = mock(HttpSession.class);
        when(mockSession.getAttribute("foo")).thenReturn("bar");

        // Mock the Request
        HttpServletRequest mockWrappedRequest = mock(HttpServletRequest.class);
        // getPathInfo(0 gets called in constructor
        when(mockWrappedRequest.getPathInfo()).thenReturn(null);
        // delegate will have getSession(true) called and return null.
        when(mockWrappedRequest.getSession(true)).thenReturn(mockSession);

        PluginHttpRequestWrapper request = new PluginHttpRequestWrapper(mockWrappedRequest, null);

        HttpSession wrappedSession = request.getSession();
        assertTrue(wrappedSession instanceof PluginHttpSessionWrapper);
        assertEquals("bar", wrappedSession.getAttribute("foo"));
    }

    @Test
    public void testGetSessionTrue() throws Exception {
        // Mock the Session
        HttpSession mockSession = mock(HttpSession.class);
        when(mockSession.getAttribute("foo")).thenReturn("bar");

        // Mock the Request
        HttpServletRequest mockWrappedRequest = mock(HttpServletRequest.class);
        // getPathInfo(0 gets called in constructor
        when(mockWrappedRequest.getPathInfo()).thenReturn(null);
        // delegate will have getSession(true) called and return null.
        when(mockWrappedRequest.getSession(true)).thenReturn(mockSession);
        PluginHttpRequestWrapper request = new PluginHttpRequestWrapper(mockWrappedRequest, null);

        HttpSession wrappedSession = request.getSession(true);
        assertTrue(wrappedSession instanceof PluginHttpSessionWrapper);
        assertEquals("bar", wrappedSession.getAttribute("foo"));
    }

    @Test
    public void testPrefixingWildcardsMatching() {
        PluginHttpRequestWrapper request = getWrappedRequest("/context/plugins", "/plugin/servlet-two/path/to/resource",
                new ServletModuleDescriptorBuilder()
                        .withPath("/plugin/servlet/*")
                        .withPath("/plugin/servlet-two/*")
                        .build());

        // should match the second mapping.
        assertEquals("/context/plugins/plugin/servlet-two", request.getServletPath());
        assertEquals("/path/to/resource", request.getPathInfo());
    }
}
