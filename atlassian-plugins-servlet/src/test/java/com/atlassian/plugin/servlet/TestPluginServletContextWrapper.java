package com.atlassian.plugin.servlet;

import com.atlassian.plugin.Plugin;
import org.dom4j.Element;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import javax.servlet.Servlet;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServlet;
import java.util.HashMap;
import java.util.concurrent.ConcurrentHashMap;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class TestPluginServletContextWrapper {
    @Rule
    public ExpectedException expectedException = ExpectedException.none();

    @Mock
    private ServletContext servletContext;
    @Mock
    private ServletModuleManager servletModuleManager;
    @Mock
    private Plugin plugin;

    @Captor
    private ArgumentCaptor<Element> elementCaptor;

    private ServletContext contextWrapper;

    @Before
    public void before() {
        when(servletContext.getAttribute("wrapped")).thenReturn("wrapped value");

        contextWrapper = new PluginServletContextWrapper(servletModuleManager, plugin, servletContext, new ConcurrentHashMap<String, Object>(), new HashMap<String, String>());
    }

    @Test
    public void putAttribute() {
        // if set attribute is called on the wrapped context it will throw an 
        // exception since it is not expecting it
        contextWrapper.setAttribute("attr", "value");
        assertThat(contextWrapper.getAttribute("attr"), is((Object) "value"));
    }

    @Test
    public void getAttributeDelegatesToWrappedContext() {
        assertThat(contextWrapper.getAttribute("wrapped"), is((Object) "wrapped value"));
    }

    @Test
    public void putAttributeOverridesWrapperContextAttribute() {
        // if set attribute is called on the wrapped context it will throw an 
        // exception since it is not expecting it
        contextWrapper.setAttribute("wrapped", "value");
        assertThat(contextWrapper.getAttribute("wrapped"), is((Object) "value"));
    }

    @Test
    public void addServletByClassName() {
        contextWrapper.addServlet("roger", RogerServlet.class.getName());

        verify(servletModuleManager).addServlet(plugin, "roger", RogerServlet.class.getName());
        verifyNoMoreInteractions(servletModuleManager);
    }

    @Test
    public void addServletByClass() {
        contextWrapper.addServlet("roger", RogerServlet.class);

        verify(servletModuleManager).addServlet(plugin, "roger", RogerServlet.class.getName());
        verifyNoMoreInteractions(servletModuleManager);
    }

    @Test
    public void addServletByExistingServlet() {
        final RogerServlet rogerServlet = new RogerServlet();
        contextWrapper.addServlet("roger", rogerServlet);

        verify(servletModuleManager).addServlet(plugin, "roger", rogerServlet, contextWrapper);
        verifyNoMoreInteractions(servletModuleManager);
    }

    @Test
    public void addServletWithNonHttpServletVerboten() {
        expectedException.expect(IllegalArgumentException.class);
        expectedException.expectMessage("javax.servlet.http.HttpServlet");

        contextWrapper.addServlet("klaus", mock(Servlet.class));
    }

    public class RogerServlet extends HttpServlet {
    }
}
