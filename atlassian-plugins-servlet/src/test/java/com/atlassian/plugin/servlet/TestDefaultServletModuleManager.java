package com.atlassian.plugin.servlet;

import com.atlassian.plugin.ModuleDescriptor;
import com.atlassian.plugin.Plugin;
import com.atlassian.plugin.PluginAccessor;
import com.atlassian.plugin.PluginController;
import com.atlassian.plugin.PluginException;
import com.atlassian.plugin.event.PluginEventManager;
import com.atlassian.plugin.event.events.PluginFrameworkShutdownEvent;
import com.atlassian.plugin.event.events.PluginFrameworkShuttingDownEvent;
import com.atlassian.plugin.event.events.PluginFrameworkStartedEvent;
import com.atlassian.plugin.scope.ScopeManager;
import com.atlassian.plugin.servlet.DefaultServletModuleManager.LazyLoadedServletReference;
import com.atlassian.plugin.servlet.descriptors.ServletContextListenerModuleDescriptor;
import com.atlassian.plugin.servlet.descriptors.ServletContextListenerModuleDescriptorBuilder;
import com.atlassian.plugin.servlet.descriptors.ServletContextParamDescriptorBuilder;
import com.atlassian.plugin.servlet.descriptors.ServletFilterModuleDescriptor;
import com.atlassian.plugin.servlet.descriptors.ServletFilterModuleDescriptorBuilder;
import com.atlassian.plugin.servlet.descriptors.ServletModuleDescriptor;
import com.atlassian.plugin.servlet.descriptors.ServletModuleDescriptorBuilder;
import com.atlassian.plugin.servlet.filter.DelegatingPluginFilter;
import com.atlassian.plugin.servlet.filter.FilterDispatcherCondition;
import com.atlassian.plugin.servlet.filter.FilterLocation;
import com.atlassian.plugin.servlet.filter.FilterTestUtils.FilterAdapter;
import com.atlassian.plugin.servlet.filter.FilterTestUtils.SoundOffFilter;
import com.atlassian.plugin.servlet.filter.IteratingFilterChain;
import com.atlassian.plugin.servlet.util.DefaultPathMapper;
import com.atlassian.plugin.servlet.util.PathMapper;
import com.atlassian.plugin.test.CapturedLogging;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.Iterables;
import com.google.common.collect.Iterators;
import org.dom4j.Element;
import org.dom4j.dom.DOMElement;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.runners.MockitoJUnitRunner;
import org.mockito.stubbing.Answer;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.Vector;
import java.util.concurrent.atomic.AtomicReference;

import static com.atlassian.plugin.servlet.filter.FilterDispatcherCondition.ERROR;
import static com.atlassian.plugin.servlet.filter.FilterDispatcherCondition.FORWARD;
import static com.atlassian.plugin.servlet.filter.FilterDispatcherCondition.INCLUDE;
import static com.atlassian.plugin.servlet.filter.FilterDispatcherCondition.REQUEST;
import static com.atlassian.plugin.servlet.filter.FilterLocation.BEFORE_DISPATCH;
import static com.atlassian.plugin.servlet.filter.FilterTestUtils.emptyChain;
import static com.atlassian.plugin.test.CapturedLogging.didLogWarn;
import static com.atlassian.plugin.test.Matchers.isElement;
import static java.util.Collections.emptyList;
import static java.util.Collections.enumeration;
import static org.hamcrest.Matchers.contains;
import static org.hamcrest.Matchers.instanceOf;
import static org.hamcrest.Matchers.not;
import static org.hamcrest.collection.IsMapContaining.hasKey;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertSame;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyString;
import static org.mockito.Matchers.same;
import static org.mockito.Mockito.doAnswer;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class TestDefaultServletModuleManager {
    @Rule
    public CapturedLogging capturedLogging = new CapturedLogging(DefaultServletModuleManager.class);
    @Rule
    public ExpectedException expectedException = ExpectedException.none();

    @Mock
    private PluginEventManager mockPluginEventManager;
    @Mock
    private PathMapper mockServletMapper;
    @Mock
    private PathMapper mockFilterMapper;
    @Mock
    private FilterFactory mockFilterFactory;
    @Mock
    private PluginController pluginController;
    @Mock
    private ScopeManager scopeManager;

    private DefaultServletModuleManager servletModuleManager;

    @Before
    public void setUp() {
        servletModuleManager = new DefaultServletModuleManager(mockPluginEventManager,
                new DefaultPathMapper(),
                new DefaultPathMapper(),
                new FilterFactory(),
                scopeManager);
        when(scopeManager.isScopeActive(anyString())).thenReturn(true);
        final PluginFrameworkStartedEvent pluginFrameworkStartedEvent = new PluginFrameworkStartedEvent(pluginController, mock(PluginAccessor.class));
        servletModuleManager.onPluginFrameworkStartingEvent(pluginFrameworkStartedEvent);
    }

    @Test
    public void testGettingServletWithSimplePath() throws Exception {
        final ServletContext mockServletContext = mock(ServletContext.class);
        when(mockServletContext.getInitParameterNames()).thenReturn(Iterators.asEnumeration(Iterators.<String>emptyIterator()));
        final ServletConfig mockServletConfig = mock(ServletConfig.class);
        when(mockServletConfig.getServletContext()).thenReturn(mockServletContext);

        final HttpServletRequest mockHttpServletRequest = mock(HttpServletRequest.class);
        when(mockHttpServletRequest.getPathInfo()).thenReturn("/servlet");
        final HttpServletResponse mockHttpServletResponse = mock(HttpServletResponse.class);

        TestHttpServlet servlet = new TestHttpServlet();
        ServletModuleDescriptor descriptor = new ServletModuleDescriptorBuilder()
                .with(servlet)
                .withPath("/servlet")
                .with(servletModuleManager)
                .build();

        servletModuleManager.addServletModule(descriptor);

        HttpServlet wrappedServlet = servletModuleManager.getServlet("/servlet", mockServletConfig);
        wrappedServlet.service(mockHttpServletRequest, mockHttpServletResponse);
        assertTrue(servlet.serviceCalled);
    }

    @Test
    public void testGettingServlet() {
        getServletTwice(false);
    }

    private void getServletTwice(boolean expectNewServletEachCall) {
        DefaultServletModuleManager mgr = new DefaultServletModuleManager(mockPluginEventManager);

        AtomicReference<HttpServlet> servletRef = new AtomicReference<HttpServlet>();
        TestHttpServlet firstServlet = new TestHttpServlet();
        servletRef.set(firstServlet);
        ServletModuleDescriptor descriptor = new ServletModuleDescriptorBuilder()
                .withFactory(ObjectFactories.createMutable(servletRef))
                .withPath("/servlet")
                .with(mgr)
                .build();

        final ServletConfig mockServletConfig = mock(ServletConfig.class);
        final ServletContext mockServletContext = mock(ServletContext.class);
        when(mockServletContext.getInitParameterNames()).thenReturn(enumeration(emptyList()));
        when(mockServletConfig.getServletContext()).thenReturn(mockServletContext);

        assertTrue(firstServlet == ((DelegatingPluginServlet) mgr.getServlet(descriptor, mockServletConfig)).getDelegatingServlet());

        TestHttpServlet secondServlet = new TestHttpServlet();
        servletRef.set(secondServlet);
        HttpServlet expectedServlet = (expectNewServletEachCall ? secondServlet : firstServlet);
        assertTrue(expectedServlet == ((DelegatingPluginServlet) mgr.getServlet(descriptor, mockServletConfig)).getDelegatingServlet());
    }

    @Test
    public void testGettingFilter() {
        getFilterTwice(false);
    }

    private void getFilterTwice(boolean expectNewFilterEachCall) {
        DefaultServletModuleManager mgr = new DefaultServletModuleManager(mockPluginEventManager);

        AtomicReference<Filter> filterRef = new AtomicReference<Filter>();
        TestHttpFilter firstFilter = new TestHttpFilter();
        filterRef.set(firstFilter);
        ServletFilterModuleDescriptor descriptor = new ServletFilterModuleDescriptorBuilder()
                .withFactory(ObjectFactories.createMutable(filterRef))
                .withPath("/servlet")
                .with(mgr)
                .build();

        final FilterConfig mockFilterConfig = mock(FilterConfig.class);
        final ServletContext mockServletContext = mock(ServletContext.class);
        when(mockServletContext.getInitParameterNames()).thenReturn(enumeration(emptyList()));
        when(mockFilterConfig.getServletContext()).thenReturn(mockServletContext);

        assertTrue(firstFilter == ((DelegatingPluginFilter) mgr.getFilter(descriptor, mockFilterConfig)).getDelegatingFilter());

        TestHttpFilter secondFilter = new TestHttpFilter();
        filterRef.set(secondFilter);
        Filter expectedFilter = (expectNewFilterEachCall ? secondFilter : firstFilter);
        assertTrue(expectedFilter == ((DelegatingPluginFilter) mgr.getFilter(descriptor, mockFilterConfig)).getDelegatingFilter());
    }

    @Test
    public void testGettingServletWithException() throws Exception {
        ServletContext mockServletContext = mock(ServletContext.class);
        when(mockServletContext.getInitParameterNames()).thenReturn(enumeration(emptyList()));
        ServletConfig mockServletConfig = mock(ServletConfig.class);
        when(mockServletConfig.getServletContext()).thenReturn(mockServletContext);

        HttpServletRequest mockHttpServletRequest = mock(HttpServletRequest.class);
        when(mockHttpServletRequest.getPathInfo()).thenReturn("/servlet");

        TestHttpServletWithException servlet = new TestHttpServletWithException();
        ServletModuleDescriptor descriptor = new ServletModuleDescriptorBuilder()
                .with(servlet)
                .withPath("/servlet")
                .with(servletModuleManager)
                .build();

        servletModuleManager.addServletModule(descriptor);

        assertNull(servletModuleManager.getServlet("/servlet", mockServletConfig));
    }

    @Test
    public void testOnlyActiveServletsAreReturned() throws Exception {
        ServletContext mockServletContext = mock(ServletContext.class);
        when(mockServletContext.getInitParameterNames()).thenReturn(enumeration(emptyList()));
        ServletConfig mockServletConfig = mock(ServletConfig.class);
        when(mockServletConfig.getServletContext()).thenReturn(mockServletContext);

        HttpServletRequest mockHttpServletRequest = mock(HttpServletRequest.class);
        when(mockHttpServletRequest.getPathInfo()).thenReturn("/servlet");
        HttpServletResponse mockHttpServletResponse = mock(HttpServletResponse.class);

        TestHttpServlet servlet = new TestHttpServlet();
        ServletModuleDescriptor descriptor = new ServletModuleDescriptorBuilder()
                .with(servlet)
                .withScopeKey(Optional.of("servetatron"))
                .withPath("/servlet/*")
                .with(servletModuleManager)
                .build();

        servletModuleManager.addServletModule(descriptor);

        when(scopeManager.isScopeActive("servetatron")).thenReturn(true);
        assertNotNull(servletModuleManager.getServlet("/servlet/servetatron", mockServletConfig));

        when(scopeManager.isScopeActive("servetatron")).thenReturn(false);
        assertNull(servletModuleManager.getServlet("/servlet/servetatron", mockServletConfig));
    }

    @Test
    public void testGettingFilterWithException() throws Exception {
        ServletContext mockServletContext = mock(ServletContext.class);
        when(mockServletContext.getInitParameterNames()).thenReturn(enumeration(emptyList()));
        FilterConfig mockFilterConfig = mock(FilterConfig.class);
        when(mockFilterConfig.getServletContext()).thenReturn(mockServletContext);

        HttpServletRequest mockHttpServletRequest = mock(HttpServletRequest.class);
        when(mockHttpServletRequest.getPathInfo()).thenReturn("/servlet");

        TestFilterWithException servlet = new TestFilterWithException();
        ServletFilterModuleDescriptor descriptor = new ServletFilterModuleDescriptorBuilder()
                .with(servlet)
                .withPath("/servlet")
                .with(servletModuleManager)
                .at(FilterLocation.AFTER_ENCODING)
                .build();

        servletModuleManager.addFilterModule(descriptor);

        assertEquals(false, servletModuleManager.getFilters(FilterLocation.AFTER_ENCODING, "/servlet", mockFilterConfig).iterator().hasNext());
    }

    @Test
    public void testGettingServletWithComplexPath() throws Exception {
        ServletContext mockServletContext = mock(ServletContext.class);
        when(mockServletContext.getInitParameterNames()).thenReturn(enumeration(emptyList()));
        ServletConfig mockServletConfig = mock(ServletConfig.class);
        when(mockServletConfig.getServletContext()).thenReturn(mockServletContext);

        HttpServletRequest mockHttpServletRequest = mock(HttpServletRequest.class);
        when(mockHttpServletRequest.getPathInfo()).thenReturn("/servlet");
        HttpServletResponse mockHttpServletResponse = mock(HttpServletResponse.class);

        TestHttpServlet servlet = new TestHttpServlet();
        ServletModuleDescriptor descriptor = new ServletModuleDescriptorBuilder()
                .with(servlet)
                .withPath("/servlet/*")
                .with(servletModuleManager)
                .build();

        servletModuleManager.addServletModule(descriptor);

        HttpServlet wrappedServlet = servletModuleManager.getServlet("/servlet/this/is/a/test", mockServletConfig);
        wrappedServlet.service(mockHttpServletRequest, mockHttpServletResponse);
        assertTrue(servlet.serviceCalled);
    }

    @Test
    public void testMultipleFiltersWithTheSameComplexPath() throws ServletException {
        ServletContext mockServletContext = mock(ServletContext.class);
        FilterConfig mockFilterConfig = mock(FilterConfig.class);
        when(mockFilterConfig.getServletContext()).thenReturn(mockServletContext);
        when(mockServletContext.getInitParameterNames()).thenReturn(new Vector().elements());
        Plugin plugin = new PluginBuilder().build();
        ServletFilterModuleDescriptor filterDescriptor = new ServletFilterModuleDescriptorBuilder()
                .with(plugin)
                .withKey("foo")
                .with(new FilterAdapter())
                .withPath("/foo/*")
                .with(servletModuleManager)
                .build();

        ServletFilterModuleDescriptor filterDescriptor2 = new ServletFilterModuleDescriptorBuilder()
                .with(plugin)
                .withKey("bar")
                .with(new FilterAdapter())
                .withPath("/foo/*")
                .with(servletModuleManager)
                .build();
        servletModuleManager.addFilterModule(filterDescriptor);
        servletModuleManager.addFilterModule(filterDescriptor2);

        servletModuleManager.removeFilterModule(filterDescriptor);
        assertTrue(servletModuleManager.getFilters(FilterLocation.BEFORE_DISPATCH, "/foo/jim", mockFilterConfig).iterator().hasNext());
    }

    @Test
    public void testMultipleFiltersWithTheSameSimplePath() throws ServletException {
        ServletContext mockServletContext = mock(ServletContext.class);
        FilterConfig mockFilterConfig = mock(FilterConfig.class);
        when(mockFilterConfig.getServletContext()).thenReturn(mockServletContext);
        when(mockServletContext.getInitParameterNames()).thenReturn(new Vector().elements());
        Plugin plugin = new PluginBuilder().build();
        ServletFilterModuleDescriptor filterDescriptor = new ServletFilterModuleDescriptorBuilder()
                .with(plugin)
                .withKey("foo")
                .with(new FilterAdapter())
                .withPath("/foo")
                .with(servletModuleManager)
                .build();

        ServletFilterModuleDescriptor filterDescriptor2 = new ServletFilterModuleDescriptorBuilder()
                .with(plugin)
                .withKey("bar")
                .with(new FilterAdapter())
                .withPath("/foo")
                .with(servletModuleManager)
                .build();
        servletModuleManager.addFilterModule(filterDescriptor);
        servletModuleManager.addFilterModule(filterDescriptor2);

        servletModuleManager.removeFilterModule(filterDescriptor);
        assertTrue(servletModuleManager.getFilters(FilterLocation.BEFORE_DISPATCH, "/foo", mockFilterConfig).iterator().hasNext());
    }

    @Test
    public void testPluginContextInitParamsGetMerged() throws Exception {
        ServletContext mockServletContext = mock(ServletContext.class);
        when(mockServletContext.getInitParameterNames()).thenReturn(enumeration(emptyList()));
        ServletConfig mockServletConfig = mock(ServletConfig.class);
        when(mockServletConfig.getServletContext()).thenReturn(mockServletContext);

        Plugin plugin = new PluginBuilder().build();

        new ServletContextParamDescriptorBuilder()
                .with(plugin)
                .withParam("param.name", "param.value")
                .build();

        // a servlet that will check for param.name to be in the servlet context
        ServletModuleDescriptor servletDescriptor = new ServletModuleDescriptorBuilder()
                .with(plugin)
                .with(new TestHttpServlet() {
                    @Override
                    public void init(ServletConfig servletConfig) {
                        assertEquals("param.value", servletConfig.getServletContext().getInitParameter("param.name"));
                    }
                })
                .withPath("/servlet")
                .with(servletModuleManager)
                .build();
        servletModuleManager.addServletModule(servletDescriptor);

        servletModuleManager.getServlet("/servlet", mockServletConfig);
    }

    @Test
    public void testServletListenerContextInitializedIsCalled() throws Exception {
        ServletContext mockServletContext = mock(ServletContext.class);
        when(mockServletContext.getInitParameterNames()).thenReturn(enumeration(emptyList()));
        ServletConfig mockServletConfig = mock(ServletConfig.class);
        when(mockServletConfig.getServletContext()).thenReturn(mockServletContext);

        final TestServletContextListener listener = new TestServletContextListener();

        Plugin plugin = new PluginBuilder().build();

        new ServletContextListenerModuleDescriptorBuilder()
                .with(plugin)
                .with(listener)
                .build();

        ServletModuleDescriptor servletDescriptor = new ServletModuleDescriptorBuilder()
                .with(plugin)
                .with(new TestHttpServlet())
                .withPath("/servlet")
                .with(servletModuleManager)
                .build();

        servletModuleManager.addServletModule(servletDescriptor);
        servletModuleManager.getServlet("/servlet", mockServletConfig);
        assertTrue(listener.initCalled);
    }

    @Test
    public void testServletListenerContextFilterAndServletUseTheSameServletContext() throws Exception {
        Plugin plugin = new PluginBuilder().build();

        final AtomicReference<ServletContext> contextRef = new AtomicReference<ServletContext>();
        // setup a context listener to capture the context
        new ServletContextListenerModuleDescriptorBuilder()
                .with(plugin)
                .with(new TestServletContextListener() {
                    @Override
                    public void contextInitialized(ServletContextEvent event) {
                        contextRef.set(event.getServletContext());
                    }
                })
                .build();

        // a servlet that checks that the context is the same for it as it was for the context listener
        ServletModuleDescriptor servletDescriptor = new ServletModuleDescriptorBuilder()
                .with(plugin)
                .with(new TestHttpServlet() {
                    @Override
                    public void init(ServletConfig mockServletConfig) {
                        assertSame(contextRef.get(), mockServletConfig.getServletContext());
                    }
                })
                .withPath("/servlet")
                .with(servletModuleManager)
                .build();
        servletModuleManager.addServletModule(servletDescriptor);

        // a filter that checks that the context is the same for it as it was for the context listener
        ServletFilterModuleDescriptor filterDescriptor = new ServletFilterModuleDescriptorBuilder()
                .with(plugin)
                .with(new FilterAdapter() {
                    @Override
                    public void init(FilterConfig mockFilterConfig) {
                        assertSame(contextRef.get(), mockFilterConfig.getServletContext());
                    }
                })
                .withPath("/*")
                .with(servletModuleManager)
                .build();
        servletModuleManager.addFilterModule(filterDescriptor);

        ServletContext mockServletContext = mock(ServletContext.class);
        when(mockServletContext.getInitParameterNames()).thenReturn(enumeration(emptyList()));

        // get a servlet, this will initialize the servlet context for the first time in addition to the servlet itself.
        // if the servlet doesn't get the same context as the context listener did, the assert will fail
        ServletConfig mockServletConfig = mock(ServletConfig.class);
        when(mockServletConfig.getServletContext()).thenReturn(mockServletContext);
        servletModuleManager.getServlet("/servlet", mockServletConfig);

        // get the filters, if the filter doesn't get the same context as the context listener did, the assert will fail
        FilterConfig mockFilterConfig = mock(FilterConfig.class);
        when(mockFilterConfig.getServletContext()).thenReturn(mockServletContext);
        servletModuleManager.getFilters(FilterLocation.BEFORE_DISPATCH, "/servlet", mockFilterConfig);
    }

    @Test
    public void testFiltersWithSameLocationAndWeightInTheSamePluginAppearInTheOrderTheyAreDeclared() throws Exception {
        ServletContext mockServletContext = mock(ServletContext.class);
        when(mockServletContext.getInitParameterNames()).thenReturn(enumeration(emptyList()));
        FilterConfig mockFilterConfig = mock(FilterConfig.class);
        when(mockFilterConfig.getServletContext()).thenReturn(mockServletContext);

        Plugin plugin = new PluginBuilder().build();

        List<Integer> filterCallOrder = new LinkedList<Integer>();
        ServletFilterModuleDescriptor d1 = new ServletFilterModuleDescriptorBuilder()
                .with(plugin)
                .withKey("filter-1")
                .with(new SoundOffFilter(filterCallOrder, 1))
                .withPath("/*")
                .build();
        servletModuleManager.addFilterModule(d1);

        ServletFilterModuleDescriptor d2 = new ServletFilterModuleDescriptorBuilder()
                .with(plugin)
                .withKey("filter-2")
                .with(new SoundOffFilter(filterCallOrder, 2))
                .withPath("/*")
                .build();
        servletModuleManager.addFilterModule(d2);

        HttpServletRequest mockHttpServletRequest = mock(HttpServletRequest.class);
        when(mockHttpServletRequest.getPathInfo()).thenReturn("/servlet");
        HttpServletResponse mockHttpServletResponse = mock(HttpServletResponse.class);

        Iterable<Filter> filters = servletModuleManager.getFilters(FilterLocation.BEFORE_DISPATCH, "/some/path", mockFilterConfig);
        FilterChain chain = new IteratingFilterChain(filters.iterator(), emptyChain);

        chain.doFilter(mockHttpServletRequest, mockHttpServletResponse);
        assertEquals(newList(1, 2, 2, 1), filterCallOrder);
    }

    @Test
    public void testGetFiltersWithDispatcher() throws Exception {
        ServletContext mockServletContext = mock(ServletContext.class);
        FilterConfig mockFilterConfig = mock(FilterConfig.class);
        when(mockFilterConfig.getServletContext()).thenReturn(mockServletContext);
        when(mockServletContext.getInitParameterNames()).thenReturn(new Vector().elements());
        Plugin plugin = new PluginBuilder().build();

        ServletFilterModuleDescriptor filterDescriptor = new ServletFilterModuleDescriptorBuilder()
                .with(plugin)
                .withKey("foo")
                .with(new FilterAdapter())
                .withPath("/foo")
                .with(servletModuleManager)
                .withDispatcher(REQUEST)
                .withDispatcher(FORWARD)
                .build();

        ServletFilterModuleDescriptor filterDescriptor2 = new ServletFilterModuleDescriptorBuilder()
                .with(plugin)
                .withKey("bar")
                .with(new FilterAdapter())
                .withPath("/foo")
                .with(servletModuleManager)
                .withDispatcher(REQUEST)
                .withDispatcher(INCLUDE)
                .build();

        servletModuleManager.addFilterModule(filterDescriptor);
        servletModuleManager.addFilterModule(filterDescriptor2);

        assertEquals(2, Iterables.size(servletModuleManager.getFilters(FilterLocation.BEFORE_DISPATCH, "/foo", mockFilterConfig, REQUEST)));
        assertEquals(1, Iterables.size(servletModuleManager.getFilters(FilterLocation.BEFORE_DISPATCH, "/foo", mockFilterConfig, INCLUDE)));
        assertEquals(1, Iterables.size(servletModuleManager.getFilters(FilterLocation.BEFORE_DISPATCH, "/foo", mockFilterConfig, FORWARD)));
        assertEquals(0, Iterables.size(servletModuleManager.getFilters(FilterLocation.BEFORE_DISPATCH, "/foo", mockFilterConfig, ERROR)));

        try {
            servletModuleManager.getFilters(FilterLocation.BEFORE_DISPATCH, "/foo", mockFilterConfig, null);
            fail("Shouldn't accept nulls");
        } catch (NullPointerException ex) {
            // this is good
        }
    }

    @Test
    public void testShuttingDownDoesDestroyServletsFiltersAndContexts() throws Exception {
        final String servletPath = "/servlet";
        final String pathInfo = "/pathInfo";

        final Plugin mockPlugin = mock(Plugin.class);
        final ServletModuleDescriptor mockServletModuleDescriptor = mock(ServletModuleDescriptor.class);
        when(mockServletModuleDescriptor.getPlugin()).thenReturn(mockPlugin);
        when(mockServletModuleDescriptor.getCompleteKey()).thenReturn("plugin:servlet");
        when(mockServletModuleDescriptor.getScopeKey()).thenReturn(Optional.empty());

        when(mockServletModuleDescriptor.getPaths()).thenReturn(ImmutableList.of(servletPath));
        final ServletFilterModuleDescriptor mockServletFilterModuleDescriptor = mock(ServletFilterModuleDescriptor.class);
        when(mockServletFilterModuleDescriptor.getCompleteKey()).thenReturn("plugin:filter");

        ServletContextListener mockServletContextListener = mock(ServletContextListener.class);
        ServletContextListenerModuleDescriptor mockServletContextListenerModuleDescriptor = mock(ServletContextListenerModuleDescriptor.class);
        when(mockServletContextListenerModuleDescriptor.getModule()).thenReturn(mockServletContextListener);

        final Collection<ModuleDescriptor<?>> moduleDescriptors = ImmutableList.<ModuleDescriptor<?>>of(
                mockServletModuleDescriptor, mockServletFilterModuleDescriptor, mockServletContextListenerModuleDescriptor);
        when(mockPlugin.getModuleDescriptors()).thenReturn(moduleDescriptors);

        final ServletContext mockServletContext = mock(ServletContext.class);
        when(mockServletContext.getInitParameterNames()).thenReturn(Iterators.asEnumeration(Iterators.<String>emptyIterator()));
        final ServletConfig mockServletConfig = mock(ServletConfig.class);
        when(mockServletConfig.getServletContext()).thenReturn(mockServletContext);
        final FilterConfig mockFilterConfig = mock(FilterConfig.class);
        when(mockFilterConfig.getServletContext()).thenReturn(mockServletContext);

        servletModuleManager.addServletModule(mockServletModuleDescriptor);
        servletModuleManager.addFilterModule(mockServletFilterModuleDescriptor);
        servletModuleManager.getServlet(servletPath, mockServletConfig);
        servletModuleManager.getFilters(FilterLocation.AFTER_ENCODING, pathInfo, mockFilterConfig, FilterDispatcherCondition.REQUEST);

        final PluginFrameworkShuttingDownEvent pluginFrameworkShuttingDownEvent = mock(PluginFrameworkShuttingDownEvent.class);
        // Event delivery is on implementation class, so we need to cast
        servletModuleManager.onPluginFrameworkBeforeShutdown(pluginFrameworkShuttingDownEvent);

        verify(mockServletModuleDescriptor).destroy();
        verify(mockServletFilterModuleDescriptor).destroy();
        verify(mockServletContextListener).contextDestroyed(any(ServletContextEvent.class));

        // Check that final PluginFrameworkShutdownEvent does not reinvoke destruction logic
        final PluginFrameworkShutdownEvent mockPluginFrameworkShutdownEvent = mock(PluginFrameworkShutdownEvent.class);
        // Event delivery is on implementation class, so we need to cast
        servletModuleManager.onPluginFrameworkShutdown(mockPluginFrameworkShutdownEvent);

        // What we are actually testing here is that these were called once only, since they were called for shutting down
        verify(mockServletModuleDescriptor).destroy();
        verify(mockServletFilterModuleDescriptor).destroy();
        verify(mockServletContextListener).contextDestroyed(any(ServletContextEvent.class));
    }

    @Test
    public void testFilterOrModuleRemovalDuringShutdown() throws Exception {
        final String servletPath = "/servlet";
        final String pathInfo = "/pathInfo";

        final ServletModuleDescriptor servletModuleDescriptor = mock(ServletModuleDescriptor.class);
        when(servletModuleDescriptor.getCompleteKey()).thenReturn("plugin:servlet");
        doAnswer(new Answer() {
            @Override
            public Object answer(final InvocationOnMock invocation) throws Throwable {
                servletModuleManager.removeServletModule(servletModuleDescriptor);
                return null;
            }
        }).when(servletModuleDescriptor).destroy();
        final ServletFilterModuleDescriptor servletFilterModuleDescriptor = mock(ServletFilterModuleDescriptor.class);
        when(servletFilterModuleDescriptor.getCompleteKey()).thenReturn("plugin:filter");
        doAnswer(new Answer() {
            @Override
            public Object answer(final InvocationOnMock invocation) throws Throwable {
                servletModuleManager.removeFilterModule(servletFilterModuleDescriptor);
                return null;
            }
        }).when(servletFilterModuleDescriptor).destroy();

        final ServletContext mockServletContext = mock(ServletContext.class);
        when(mockServletContext.getInitParameterNames()).thenReturn(Iterators.asEnumeration(Iterators.<String>emptyIterator()));
        final ServletConfig mockServletConfig = mock(ServletConfig.class);
        when(mockServletConfig.getServletContext()).thenReturn(mockServletContext);
        final FilterConfig mockFilterConfig = mock(FilterConfig.class);
        when(mockFilterConfig.getServletContext()).thenReturn(mockServletContext);

        servletModuleManager.addServletModule(servletModuleDescriptor);
        servletModuleManager.addFilterModule(servletFilterModuleDescriptor);

        final PluginFrameworkShuttingDownEvent pluginFrameworkShuttingDownEvent = mock(PluginFrameworkShuttingDownEvent.class);
        // Event delivery is on implementation class, so we need to cast
        servletModuleManager.onPluginFrameworkBeforeShutdown(pluginFrameworkShuttingDownEvent);

        verify(servletModuleDescriptor).destroy();
        verify(servletFilterModuleDescriptor).destroy();
    }

    @Test
    public void addServletByClassName() {
        final Plugin plugin = mock(Plugin.class);
        final ArgumentCaptor<Element> elementCaptor = ArgumentCaptor.forClass(Element.class);
        final ModuleDescriptor moduleDescriptor = mock(ModuleDescriptor.class);

        //noinspection unchecked
        when(pluginController.addDynamicModule(same(plugin), elementCaptor.capture())).thenReturn(moduleDescriptor);

        servletModuleManager.addServlet(plugin, "roger", "com.americandad.Roger");

        assertThat(elementCaptor.getValue(), isElement(rogerServletElement().addAttribute("class", "com.americandad.Roger")));
    }

    @Test
    public void addServletWithHttpServletUnexpectedModuleDescriptor() {
        expectedException.expect(PluginException.class);
        expectedException.expectMessage("com.atlassian.plugin.servlet.descriptors.ServletModuleDescriptor;");

        servletModuleManager.addServlet(mock(Plugin.class), "roger", mock(HttpServlet.class), mock(ServletContext.class));
    }

    @Test
    public void addServletWithHttpServlet() {
        final Plugin plugin = mock(Plugin.class);
        final ArgumentCaptor<Element> elementCaptor = ArgumentCaptor.forClass(Element.class);
        final ModuleDescriptor moduleDescriptor = mock(ServletModuleDescriptor.class);
        final HttpServlet httpServlet = mock(HttpServlet.class);
        final ServletContext servletContext = mock(ServletContext.class);

        //noinspection unchecked
        when(pluginController.addDynamicModule(same(plugin), elementCaptor.capture())).thenReturn(moduleDescriptor);
        when(moduleDescriptor.getCompleteKey()).thenReturn("roger.key");

        servletModuleManager.addServlet(plugin, "roger", httpServlet, servletContext);

        assertThat(elementCaptor.getValue(), isElement(rogerServletElement()));

        assertThat(servletModuleManager.getServletRefs(), hasKey("roger.key"));
        assertThat(servletModuleManager.getServletRefs().get("roger.key"), instanceOf(LazyLoadedServletReference.class));
    }

    @Test
    public void addSameServletTwice() {
        expectedException.expect(IllegalStateException.class);
        expectedException.expectMessage("roger.key");

        addServletWithHttpServlet();
        addServletWithHttpServlet();
    }

    private Element rogerServletElement() {
        final Element e = new DOMElement("servlet");
        e.addAttribute("key", "roger-servlet");
        e.addAttribute("name", "rogerServlet");
        Element url = new DOMElement("url-pattern");
        url.setText("/roger");
        e.add(url);

        return e;
    }

    @Test
    public void samePluginController() {
        servletModuleManager.onPluginFrameworkStartingEvent(new PluginFrameworkStartedEvent(pluginController, mock(PluginAccessor.class)));

        servletModuleManager.onPluginFrameworkShutdownEvent(new PluginFrameworkShutdownEvent(pluginController, mock(PluginAccessor.class)));

        assertThat(capturedLogging, not(didLogWarn()));
    }

    @Test
    public void differentPluginControllers() {
        servletModuleManager.onPluginFrameworkStartingEvent(new PluginFrameworkStartedEvent(pluginController, mock(PluginAccessor.class)));

        servletModuleManager.onPluginFrameworkShutdownEvent(new PluginFrameworkShutdownEvent(mock(PluginController.class), mock(PluginAccessor.class)));

        assertThat(capturedLogging, didLogWarn("did not match"));
    }

    @Test
    public void testNonActiveFiltersAreNotReturned() throws Exception {
        servletModuleManager = new DefaultServletModuleManager(
                mockPluginEventManager,
                mockServletMapper,
                mockFilterMapper,
                mockFilterFactory,
                scopeManager
        );

        final Set<FilterDispatcherCondition> dispatcherConditions = new HashSet<>();
        final FilterDispatcherCondition dispatcherCondition = REQUEST;
        final FilterLocation location = BEFORE_DISPATCH;
        dispatcherConditions.add(dispatcherCondition);
        final ServletFilterModuleDescriptor descriptorFree = stubFilterDescriptor(
                "free",
                Optional.empty(),
                dispatcherConditions,
                location,
                10
        );

        final ServletFilterModuleDescriptor descriptorActive = stubFilterDescriptor(
                "active",
                Optional.of("product-1"),
                dispatcherConditions,
                location,
                100
        );

        final ServletFilterModuleDescriptor descriptorInactive = stubFilterDescriptor(
                "inactive",
                Optional.of("product-2"),
                dispatcherConditions,
                location,
                1000
        );

        final Filter filterFree = stubFilter(descriptorFree);
        final Filter filterActive = stubFilter(descriptorActive);
        final Filter filterInactive = stubFilter(descriptorInactive);

        final String dummyPath = "/dummy/path";
        when(mockFilterMapper.getAll(dummyPath)).thenReturn(Arrays.asList("free", "active", "inactive"));
        final FilterConfig mockFilterConfig = mock(FilterConfig.class, Mockito.RETURNS_DEEP_STUBS);

        when(scopeManager.isScopeActive("product-1")).thenReturn(true);
        when(scopeManager.isScopeActive("product-2")).thenReturn(false);

        final Iterable<Filter> filters = servletModuleManager.getFilters(location, dummyPath, mockFilterConfig, dispatcherCondition);
        assertThat(filters, contains(filterFree, filterActive));
        assertThat(filters, not(contains(filterInactive)));
    }

    @Test
    public void testFilterSorting() throws Exception {
        servletModuleManager = new DefaultServletModuleManager(
                mockPluginEventManager,
                mockServletMapper,
                mockFilterMapper,
                mockFilterFactory,
                scopeManager
        );

        final Set<FilterDispatcherCondition> dispatcherConditions = new HashSet<>();
        final FilterDispatcherCondition dispatcherCondition = REQUEST;
        final FilterLocation location = BEFORE_DISPATCH;
        dispatcherConditions.add(dispatcherCondition);
        final ServletFilterModuleDescriptor descriptorAlpha = stubFilterDescriptor(
                "alpha",
                Optional.empty(),
                dispatcherConditions,
                location,
                10
        );
        final ServletFilterModuleDescriptor descriptorBeta = stubFilterDescriptor(
                "beta",
                Optional.empty(),
                dispatcherConditions,
                location,
                17
        );
        final ServletFilterModuleDescriptor descriptorGamma = stubFilterDescriptor(
                "gamma",
                Optional.empty(),
                dispatcherConditions,
                location,
                8
        );
        final Filter filterAlpha = stubFilter(descriptorAlpha);
        final Filter filterBeta = stubFilter(descriptorBeta);
        final Filter filterGamma = stubFilter(descriptorGamma);
        final String dummyPath = "/dummy/path";
        when(mockFilterMapper.getAll(dummyPath)).thenReturn(Arrays.asList("alpha", "beta", "gamma"));
        final FilterConfig mockFilterConfig = mock(FilterConfig.class, Mockito.RETURNS_DEEP_STUBS);

        final Iterable<Filter> filters =
                servletModuleManager.getFilters(location, dummyPath, mockFilterConfig, dispatcherCondition);

        assertThat(filters, contains(filterGamma, filterAlpha, filterBeta));
    }

    private Filter stubFilter(final ServletFilterModuleDescriptor descriptor) {
        final Filter filter = mock(Filter.class);
        final String filterMockDescription = "Filter " + descriptor.getCompleteKey();
        when(filter.toString()).thenReturn(filterMockDescription);
        when(mockFilterFactory.newFilter(descriptor)).thenReturn(filter);
        return filter;
    }

    private ServletFilterModuleDescriptor stubFilterDescriptor(
            final String descriptorCompleteKey,
            final Optional<String> scopeKey,
            final Set<FilterDispatcherCondition> dispatcherConditions,
            final FilterLocation location,
            int weight
    ) {
        final ServletFilterModuleDescriptor descriptor = mock(ServletFilterModuleDescriptor.class);
        final Plugin plugin = mock(Plugin.class);
        when(descriptor.toString()).thenReturn("Filter descriptor " + descriptorCompleteKey);
        when(descriptor.getCompleteKey()).thenReturn(descriptorCompleteKey);
        when(descriptor.getDispatcherConditions()).thenReturn(dispatcherConditions);
        when(descriptor.getLocation()).thenReturn(location);
        when(descriptor.getPlugin()).thenReturn(plugin);
        when(descriptor.getWeight()).thenReturn(weight);
        when(descriptor.getScopeKey()).thenReturn(scopeKey);


        servletModuleManager.addFilterModule(descriptor);
        return descriptor;
    }

    static class TestServletContextListener implements ServletContextListener {
        boolean initCalled = false;

        public void contextInitialized(ServletContextEvent event) {
            initCalled = true;
        }

        public void contextDestroyed(ServletContextEvent event) {
        }
    }

    static class TestHttpServlet extends HttpServlet {
        boolean serviceCalled = false;

        @Override
        public void service(ServletRequest request, ServletResponse response) {
            serviceCalled = true;
        }
    }

    static class TestHttpServletWithException extends HttpServlet {
        @Override
        public void init(ServletConfig mockServletConfig) throws ServletException {
            throw new RuntimeException("exception thrown");
        }
    }

    static class TestFilterWithException implements Filter {
        public void init(FilterConfig mockFilterConfig) throws ServletException {
            throw new RuntimeException("exception thrown");
        }

        public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain)
                throws IOException, ServletException {
        }

        public void destroy() {
        }
    }

    static class TestHttpFilter implements Filter {
        public void init(FilterConfig mockFilterConfig) throws ServletException {
        }

        public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain)
                throws IOException, ServletException {
        }

        public void destroy() {
        }
    }

    static final class WeightedValue {
        final int weight;
        final String value;

        WeightedValue(int weight, String value) {
            this.weight = weight;
            this.value = value;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) {
                return true;
            }
            if (!(o instanceof WeightedValue)) {
                return false;
            }
            WeightedValue rhs = (WeightedValue) o;
            return weight == rhs.weight && value.equals(rhs.value);
        }

        @Override
        public String toString() {
            return "[" + weight + ", " + value + "]";
        }

        static final Comparator<WeightedValue> byWeight = new Comparator<WeightedValue>() {
            public int compare(WeightedValue o1, WeightedValue o2) {
                return Integer.valueOf(o1.weight).compareTo(o2.weight);
            }
        };
    }

    static <T> List<T> newList(T... elements) {
        List<T> list = new ArrayList<T>();
        for (T e : elements) {
            list.add(e);
        }
        return list;
    }

    static <T extends Comparable<T>> Comparator<T> naturalOrder(Class<T> type) {
        return new Comparator<T>() {
            public int compare(T o1, T o2) {
                return o1.compareTo(o2);
            }
        };
    }
}
